package com.pas.socialapp.eventhandlers;

import com.pas.socialapp.models.CasteResponse;
import com.pas.socialapp.models.StateResponse;

/**
 * Created by Umesh on 24/02/16.
 */
public class CasteEvent {

    public CasteResponse casteResponse;

    public CasteEvent(CasteResponse casteResponse) {
        this.casteResponse = casteResponse;
    }

}
