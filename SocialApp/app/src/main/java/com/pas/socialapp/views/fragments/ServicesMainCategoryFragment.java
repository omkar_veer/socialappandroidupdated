package com.pas.socialapp.views.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pas.socialapp.Constants;
import com.pas.socialapp.R;
import com.pas.socialapp.eventhandlers.GenericErrorEvent;
import com.pas.socialapp.utils.Utility;
import com.pas.socialapp.views.adapters.ServicesMainCategoryListAdapter;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;

public class ServicesMainCategoryFragment extends Fragment implements ServicesMainCategoryListAdapter.ServiceClickListener {

    private View rootView;
    private EventBus eventBus = EventBus.getDefault();
    private ProgressDialog progress;

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private Context context;
    private ArrayList<String> serviceDataList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.service_main_category_fragment, container, false);
        Utility.floatingButtonVisibility(getActivity());
        initScreen();
        return rootView;
    }

    private void initScreen() {
        progress = new ProgressDialog(getActivity());
        progress.setCancelable(false);
        progress.setMessage(getString(R.string.msg_loading));


        context = getActivity();

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.serviceMainRecyclerView);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(context);
        mRecyclerView.setLayoutManager(mLayoutManager);


        serviceDataList = new ArrayList<>();
        serviceDataList.add(Constants.SERVICE_CATEGORY_1);

        serviceDataList.add(Constants.SERVICE_CATEGORY_2);

        serviceDataList.add(Constants.SERVICE_CATEGORY_3);

        serviceDataList.add(Constants.SERVICE_CATEGORY_4);

        serviceDataList.add(Constants.SERVICE_CATEGORY_5);

        mAdapter = new ServicesMainCategoryListAdapter(serviceDataList, context);
        mRecyclerView.setAdapter(mAdapter);

        ((ServicesMainCategoryListAdapter) mAdapter).setOnServiceListener(ServicesMainCategoryFragment.this);

        // progress.show();
        //  AccountManager accountManager = new AccountManager(getActivity());
        // accountManager.getJahirnamaDetails(Constants.JAHIRNAMA_TAB);
    }

    @Override
    public void onStop() {
        if (eventBus != null) {
            eventBus.unregister(this);
        }
        super.onStop();
    }

    @Override
    public void onPause() {
        if (eventBus != null) {
            eventBus.unregister(this);
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        if (eventBus != null) {
            eventBus.register(this);
        }
        super.onResume();
    }

    public void onEvent(GenericErrorEvent event) {
        progress.dismiss();
    }

    @Override
    public void getServiceCategory(String serviceType) {
        moveToScreen(new ServicesSubCategoryListFragment(), serviceType);
    }

  /*  public void onEvent(JahirnamaEvent event) {
        progress.dismiss();
        if (event.jahirnamaResponse != null) {

            if (!TextUtils.isEmpty(event.jahirnamaResponse.message) && (event.jahirnamaResponse.status.equalsIgnoreCase(getString(R.string.server_success_ok)))) {
                // Set here  Data.
                ArrayList<JahirnamaResponse.Jahirnama> jahirnamaArrayList = event.jahirnamaResponse.getJahirnama();
                if (jahirnamaArrayList != null) {
                    if (jahirnamaArrayList.size() > 0) {


                        mAdapter = new JahirnamaAdapter(jahirnamaArrayList, context);
                        mRecyclerView.setAdapter(mAdapter);

                    }
                }
            } else {
                Utility.displayToast(getActivity(), event.jahirnamaResponse.message);
            }


        } else {
            Utility.displayToast(getActivity(), getString(R.string.msg_service_call_error));
        }
    }*/

    public void moveToScreen(Fragment fragment, String serviceType) {
        FragmentTransaction fragmentManager = getFragmentManager().beginTransaction();
        Bundle args = new Bundle();
        args.putString("serviceType", serviceType);
        fragment.setArguments(args);
        fragmentManager.addToBackStack(fragment.getClass().getName());
        fragmentManager.replace(R.id.MainContainer, fragment)
                .commit();
    }


}