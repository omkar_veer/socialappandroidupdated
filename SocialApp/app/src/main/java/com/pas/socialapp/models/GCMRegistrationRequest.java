package com.pas.socialapp.models;

import com.pas.socialapp.Constants;


public class GCMRegistrationRequest extends GenericRequest {


    public String mobileno;
    public String devicetoken;
    public String deviceid;

    public GCMRegistrationRequest(String mobileno, String devicetoken,String deviceid) {
        this.mobileno = mobileno;
        this.devicetoken = devicetoken;
        this.deviceid = deviceid;
    }


}
