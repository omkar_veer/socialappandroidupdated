package com.pas.socialapp.views.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.pas.socialapp.Constants;
import com.pas.socialapp.R;
import com.pas.socialapp.managers.PrefManager;
import com.pas.socialapp.views.activities.OnboardingActivity;


public class SplashFragment extends Fragment {

    private View rootView;
    private static int SPLASH_TIME_OUT = 3000;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
      //  getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        rootView = inflater.inflate(R.layout.fragment_splash, container, false);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (getActivity() != null) {
//                    if (PrefManager.isUserLoggedIn(getActivity())) {
//                        ((OnboardingActivity) getActivity()).moveToMainActivity();
//                    } else {
                    if (PrefManager.getBooleanFromPref(getActivity(), Constants.PreferencesManagerKey.IS_LOG_IN)) {
                        ((OnboardingActivity) getActivity()).moveToMainActivity();
                    } else {
                        ((OnboardingActivity) getActivity()).moveToScreen(new LoginFragment());
                    }
//                    }
                }
            }
        }, SPLASH_TIME_OUT);

        return rootView;
    }

}