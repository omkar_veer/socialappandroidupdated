package com.pas.socialapp.eventhandlers;

import com.pas.socialapp.models.ResendOTPVerifyResponse;

public class ResendOTPverifyEvent {

    public ResendOTPVerifyResponse otpverifyResponse;
    public boolean isforResend;

    public ResendOTPverifyEvent(ResendOTPVerifyResponse otpverifyResponse, boolean isforResend) {
        this.otpverifyResponse = otpverifyResponse;
        this.isforResend = isforResend;
    }

}
