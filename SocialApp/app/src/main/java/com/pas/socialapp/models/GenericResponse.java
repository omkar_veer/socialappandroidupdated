package com.pas.socialapp.models;


import com.google.gson.annotations.SerializedName;

public class GenericResponse {

    @SerializedName("IsSuccessfull")
    public String success;

    @SerializedName("Message")
    public String message;

    @SerializedName("MessageCode")
    public String messageCode;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }
}
