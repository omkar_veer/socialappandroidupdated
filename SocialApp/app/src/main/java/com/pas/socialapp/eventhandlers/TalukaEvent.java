package com.pas.socialapp.eventhandlers;

import com.pas.socialapp.models.DistrictResponse;
import com.pas.socialapp.models.StateResponse;
import com.pas.socialapp.models.TalukaResponse;

/**
 * Created by Umesh on 24/02/16.
 */
public class TalukaEvent {

    public TalukaResponse talukaResponse;

    public TalukaEvent(TalukaResponse talukaResponse) {
        this.talukaResponse = talukaResponse;
    }

}
