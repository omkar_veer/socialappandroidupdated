package com.pas.socialapp.models;

import java.util.ArrayList;

/**
 * Created by Umesh on 08/03/16.
 */
public class WorkResponse extends GenericResponse {
    public String status;
    public String service;
    public String message;
    public ArrayList<Work> work = new ArrayList<Work>();

    /**
     * @return The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return The service
     */
    public String getService() {
        return service;
    }

    /**
     * @param service The service
     */
    public void setService(String service) {
        this.service = service;
    }

    /**
     * @return The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return The work
     */
    public ArrayList<Work> getWork() {
        return work;
    }

    /**
     * @param work The work
     */
    public void setWork(ArrayList<Work> work) {
        this.work = work;
    }

    public class Work {
        private String CID;
        private String worktype;
        private String description;
        private String image;

        /**
         * @return The CID
         */
        public String getCID() {
            return CID;
        }

        /**
         * @param CID The CID
         */
        public void setCID(String CID) {
            this.CID = CID;
        }

        /**
         * @return The worktype
         */
        public String getWorktype() {
            return worktype;
        }

        /**
         * @param worktype The worktype
         */
        public void setWorktype(String worktype) {
            this.worktype = worktype;
        }

        /**
         * @return The description
         */
        public String getDescription() {
            return description;
        }

        /**
         * @param description The description
         */
        public void setDescription(String description) {
            this.description = description;
        }

        /**
         * @return The image
         */
        public String getImage() {
            return image;
        }

        /**
         * @param image The image
         */
        public void setImage(String image) {
            this.image = image;
        }
    }
}
