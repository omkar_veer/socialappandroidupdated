package com.pas.socialapp;

import android.app.Application;

import com.pas.socialapp.managers.DatabaseManager;
import com.pas.socialapp.managers.VolleyManager;
import com.pas.socialapp.utils.VULogger;

public class SocialApp extends Application {

    public static boolean isFromResend = false;

    @Override
    public void onCreate() {
        super.onCreate();
        VolleyManager.getInstance().initVolleyRequestQueue(this);
        DatabaseManager.getInstance().initDatabase(this, Constants.LOCAL_DATABASE_NAME);
        VULogger.d("Welcome to Social App");
    }
}
