package com.pas.socialapp.managers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.pas.socialapp.models.DaoMaster;
import com.pas.socialapp.models.DaoSession;
import com.pas.socialapp.models.Notification;

public class DatabaseManager {

    private static final String ERROR_NOT_INIT = "DatabaseManager must be initialised before using";
    private static DatabaseManager instance;
    private DaoSession daoSession;

    private DatabaseManager() {
    }

    public static DatabaseManager getInstance() {
        synchronized (DatabaseManager.class) {
            if (instance == null) {
                instance = new DatabaseManager();
            }
            return instance;
        }
    }

    public DaoSession getDaoSession() {
        if (daoSession == null)
            throw new IllegalStateException(ERROR_NOT_INIT);
        return daoSession;
    }

    //This is used only by the test cases.
    public void setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
    }

    //Need to shift to builder pattern to make it fail safe.
// The initdatabase method has to be called in Application class before we perform any database operation
    public void initDatabase(final Context context, String databaseName) {
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(context, databaseName, null);
        SQLiteDatabase db = helper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(db);
        daoSession = daoMaster.newSession();
    }

    public void clearDatabase() {
        daoSession.deleteAll(Notification.class);
        daoSession.clear();
    }
}
