package com.pas.socialapp.views.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pas.socialapp.R;
import com.pas.socialapp.eventhandlers.HelpingHandEvent;
import com.pas.socialapp.eventhandlers.MatdarJagrutiEvent;
import com.pas.socialapp.managers.AccountManager;
import com.pas.socialapp.models.HelpingHandCategoryResponse;
import com.pas.socialapp.models.MatdarJagrutiResponse;
import com.pas.socialapp.utils.Utility;
import com.pas.socialapp.views.adapters.HelpingHandSubCategoryListAdapter;
import com.pas.socialapp.views.adapters.MatdarJagrutiDataListAdapter;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;

public class MatdarJagrutiFragment extends Fragment implements MatdarJagrutiDataListAdapter.OpenURLListener {

    private View rootView;
    private EventBus eventBus = EventBus.getDefault();
    private ProgressDialog progress;

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private Context context;
    String serviceType;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.matdar_jagruti_main_category_fragment, container, false);
        Utility.floatingButtonVisibility(getActivity());
        initScreen();
        return rootView;
    }

    private void initScreen() {
        progress = new ProgressDialog(getActivity());
        progress.setCancelable(false);
        progress.setMessage(getString(R.string.msg_loading));
        context = getActivity();

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.matdarJagrutiMainRecyclerView);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(context);
        mRecyclerView.setLayoutManager(mLayoutManager);
        progress.show();
        AccountManager accountManager = new AccountManager(getActivity());
        accountManager.getMatdarJagrutiDataFromServer();
    }

    @Override
    public void onStop() {
        if (eventBus != null) {
            eventBus.unregister(this);
        }
        super.onStop();
    }

    @Override
    public void onPause() {
        if (eventBus != null) {
            eventBus.unregister(this);
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        if (eventBus != null) {
            eventBus.register(this);
        }
        super.onResume();
    }

    public void onEvent(MatdarJagrutiEvent event) {
        progress.dismiss();
        if (event.matdarJagrutiResponse != null) {

            if (!TextUtils.isEmpty(event.matdarJagrutiResponse.message) && (event.matdarJagrutiResponse.status.equalsIgnoreCase(getString(R.string.server_success_ok)))) {
                // Set here  Data.
                ArrayList<MatdarJagrutiResponse.MatdarJagruti> mjArrayList = event.matdarJagrutiResponse.getMjdata();

                if (mjArrayList != null) {
                    if (mjArrayList.size() > 0) {
                        mAdapter = new MatdarJagrutiDataListAdapter(mjArrayList, context);
                        mRecyclerView.setAdapter(mAdapter);
                        ((MatdarJagrutiDataListAdapter) mAdapter).openURLServiceListener(MatdarJagrutiFragment.this);
                    }else{
                        Utility.displayToast(getActivity(),  context.getResources().getString(R.string.no_data_available));
                    }
                }
            } else {
                Utility.displayToast(getActivity(), event.matdarJagrutiResponse.message);
            }

        } else {
            Utility.displayToast(getActivity(), getString(R.string.msg_service_call_error));
        }
    }


    @Override
    public void openURLinWEbView(String url) {
        if (!url.startsWith("http://") && !url.startsWith("https://"))
            url = "http://" + url;

        Uri uri = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }
}