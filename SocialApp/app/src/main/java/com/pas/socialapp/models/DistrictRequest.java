package com.pas.socialapp.models;

/**
 * Created by ganesh on 5/1/16.
 */
public class DistrictRequest extends GenericRequest {

    public DistrictRequest(String state) {
        this.state = state;
    }

    public String state;
}
