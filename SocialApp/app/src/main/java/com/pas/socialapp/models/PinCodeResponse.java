package com.pas.socialapp.models;

import java.util.ArrayList;

/**
 * Created by Umesh  on 24/02/16.
 */
public class PinCodeResponse extends GenericRequest {

    public String status;
    public String service;
    public String message;
    public ArrayList<Pincode> pincodes = new ArrayList<Pincode>();

    /**
     * @return The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return The service
     */
    public String getService() {
        return service;
    }

    /**
     * @param service The service
     */
    public void setService(String service) {
        this.service = service;
    }

    /**
     * @return The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return The pincodes
     */
    public ArrayList<Pincode> getPincodes() {
        return pincodes;
    }

    /**
     * @param pincodes The pincodes
     */
    public void setPincodes(ArrayList<Pincode> pincodes) {
        this.pincodes = pincodes;
    }


    public class Pincode {

        private String pincode;

        /**
         * @return The pincode
         */
        public String getPincode() {
            return pincode;
        }

        /**
         * @param pincode The pincode
         */
        public void setPincode(String pincode) {
            this.pincode = pincode;
        }
    }

}

