package com.pas.socialapp.eventhandlers;

import com.pas.socialapp.models.CasteResponse;
import com.pas.socialapp.models.SubmitComplaintsResponse;

public class SubmitComplaintsEvent {

    public SubmitComplaintsResponse complaintsResponse;

    public SubmitComplaintsEvent(SubmitComplaintsResponse complaintsResponse) {
        this.complaintsResponse = complaintsResponse;
    }

}
