package com.pas.socialapp.models;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.Property;
import de.greenrobot.dao.internal.DaoConfig;

import java.util.List;

/**
 * DAO for table SHARE_CARE.
 */
public class NotificationDao extends AbstractDao<Notification, Long> {

    public static final String TABLENAME = "NOTIFICATION";

    /**
     * Properties of entity NOTIFICATION.<br/>
     * Can be used for QueryBuilder and for referencing column names.
     */
    public static class Properties {
        public final static Property _Id = new Property(0, Long.class, "_id", true, "_id");
        public final static Property Id = new Property(1, String.class, "id", false, "ID");
        public final static Property Title = new Property(2, String.class, "title", false, "TITLE");
        public final static Property Description = new Property(3, String.class, "description", false, "DESCRIPTION");
        public final static Property Url = new Property(4, String.class, "url", false, "URL");
        public final static Property Type = new Property(5, String.class, "type", false, "TYPE");
        public final static Property Time = new Property(6, String.class, "time", false, "TIME");
        public final static Property IsEnable = new Property(7, Boolean.class, "isEnable", false, "IS_ENABLE");
    }

    public NotificationDao(DaoConfig config) {
        super(config);
    }

    public NotificationDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
    }

    /**
     * Creates the underlying database table.
     */
    public static void createTable(SQLiteDatabase db, boolean ifNotExists) {
        String constraint = ifNotExists ? "IF NOT EXISTS " : "";
        db.execSQL("CREATE TABLE " + constraint + "'NOTIFICATION' (" + //
                "'_id' INTEGER PRIMARY KEY ," + //
                "'ID' TEXT ," +
                "'TITLE' TEXT," +
                "'DESCRIPTION' TEXT," +
                "'URL' TEXT," +
                "'TYPE' TEXT," +
                "'TIME' TEXT," +
                "'IS_ENABLE' BOOLEAN);");
    }

    /**
     * Drops the underlying database table.
     */
    public static void dropTable(SQLiteDatabase db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "'NOTIFICATION'";
        db.execSQL(sql);
    }

    /**
     * @inheritdoc
     */
    @Override
    protected void bindValues(SQLiteStatement stmt, Notification entity) {
        stmt.clearBindings();

        Long _id = entity.get_id();
        if (_id != null) {
            stmt.bindLong(1, _id);
        }

        String id = entity.getId();
        if (id != null) {
            stmt.bindString(2, id);
        }

        String title = entity.getTitle();
        if (title != null) {
            stmt.bindString(3, title);
        }

        String description = entity.getDescription();
        if (description != null) {
            stmt.bindString(4, description);
        }

        String url = entity.getUrl();
        if (url != null) {
            stmt.bindString(5, url);
        }

        String type = entity.getType();
        if (type != null) {
            stmt.bindString(6, type);
        }

        String time = entity.getTime();
        if (time != null) {
            stmt.bindString(7, time);
        }
        Boolean isEnable = entity.isEnable();
        if (isEnable != null) {
            stmt.bindLong(8, isEnable == true ? 1 : 0);
        }

    }

    /**
     * @inheritdoc
     */
    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0);
    }

    /**
     * @inheritdoc
     */
    @Override
    public Notification readEntity(Cursor cursor, int offset) {

        int enableFlag = cursor.isNull(offset + 7) ? null : cursor.getInt(offset + 7);

        Notification entity = new Notification(
                cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0),
                cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1),
                cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2),
                cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3),
                cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4),
                cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5),
                cursor.isNull(offset + 6) ? null : cursor.getString(offset + 6),
                enableFlag == 1 ? true : false
        );
        return entity;
    }

    /**
     * @inheritdoc
     */
    @Override
    public void readEntity(Cursor cursor, Notification entity, int offset) {
        entity.set_id(cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0));
        entity.setId(cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1));
        entity.setTitle(cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2));
        entity.setDescription(cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3));
        entity.setUrl(cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4));
        entity.setType(cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5));
        entity.setTime(cursor.isNull(offset + 6) ? null : cursor.getString(offset + 6));
        int enableFlag = cursor.isNull(offset + 7) ? null : cursor.getInt(offset + 7);
        entity.setIsEnable(enableFlag == 1 ? true : false);
    }

    /**
     * @inheritdoc
     */
    @Override
    protected Long updateKeyAfterInsert(Notification entity, long rowId) {
        entity.set_id(rowId);
        return rowId;
    }

    /**
     * @inheritdoc
     */
    @Override
    public Long getKey(Notification entity) {
        if (entity != null) {
            return entity.get_id();
        } else {
            return null;
        }
    }

    /**
     * @inheritdoc
     */
    @Override
    protected boolean isEntityUpdateable() {
        return true;
    }


    public List<Notification> getNotifications(Property propertName, boolean propertyValue) {
        try {
            return this.queryBuilder().where(propertName.eq(propertyValue)).orderAsc().list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}