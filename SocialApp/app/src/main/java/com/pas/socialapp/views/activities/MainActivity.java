package com.pas.socialapp.views.activities;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.iid.InstanceID;
import com.pas.socialapp.Constants;
import com.pas.socialapp.R;
import com.pas.socialapp.eventhandlers.GenericErrorEvent;
import com.pas.socialapp.eventhandlers.TabVisibilityEvent;
import com.pas.socialapp.managers.AccountManager;
import com.pas.socialapp.managers.PrefManager;
import com.pas.socialapp.models.TabVisibilityResponse;
import com.pas.socialapp.utils.Utility;
import com.pas.socialapp.views.fragments.*;
import com.pas.socialapp.views.custom.CustomButton;
import com.pas.socialapp.views.custom.CustomEditText;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    TextView usernameTextDrawer;
    private ProgressDialog progress;
    private EventBus eventBus = EventBus.getDefault();
    private String notificationString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        notificationString = getIntent().getStringExtra("IsFromNot");
        initView();
        ImageView fab = (ImageView) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addFragment(new ComplaintsFragment(), ComplaintsFragment.class.getSimpleName());
            }
        });
    }

    private void initView() {
        progress = new ProgressDialog(this);
        progress.setCancelable(false);
        progress.setMessage(getString(R.string.msg_loading));
        progress.show();
        AccountManager accountManager = new AccountManager(this);
        accountManager.getTabVisibilityData();
    }

    private void ServiceRequest() {
        moveToScreen(new ServicesMainCategoryFragment());
    }


    @Override
    public void onBackPressed() {
        int backStackEntryCount = getSupportFragmentManager().getBackStackEntryCount();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }else if(doubleBackToExitPressedOnce)
        {
            super.onBackPressed();
            return;
        }
        else if (backStackEntryCount == 0) {
            this.doubleBackToExitPressedOnce = true;
            Utility.displayShortLenthToast(this, getResources().getString(R.string.back_button_message_text));
            mHandler.postDelayed(mRunnable, 4000);
        } else{
            super.onBackPressed();
            return;
        }
    }


    private boolean doubleBackToExitPressedOnce;
    private Handler mHandler = new Handler();

    private final Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            doubleBackToExitPressedOnce = false;
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mHandler != null) { mHandler.removeCallbacks(mRunnable); }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
           getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_share) {
            shareAppLink();
            return true;
        }else if (id == R.id.action_rate) {
            rateApp();
          //  return true;
        }else if (id == R.id.action_developer) {
            moveToScreen(new DevFragment());
        }

        return super.onOptionsItemSelected(item);
    }

    private  void shareAppLink() {
        if (!Utility.isNetworkAvailable(MainActivity.this)) {
            Utility.displayToast(MainActivity.this, getResources().getString(R.string.internet_connectivity_message));
        }else{
            try
            { Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.app_name));
                String sAux = getResources().getString(R.string.share_app_link_message);
                sAux = sAux + "http://play.google.com/store/apps/details?id="+this.getPackageName()+ "\n\n"+getResources().getString(R.string.app_name);
                i.putExtra(Intent.EXTRA_TEXT, sAux);
                startActivity(Intent.createChooser(i, "choose one"));
            }
            catch(Exception e)
            { //e.toString();

            }
        }

    }



    private void rateApp()
    {
        if (!Utility.isNetworkAvailable(MainActivity.this)) {
            Utility.displayToast(MainActivity.this, getResources().getString(R.string.internet_connectivity_message));
        }else{
            try
            {
                Intent rateIntent = rateIntentForUrl("market://details");
                startActivity(rateIntent);
            }
            catch (ActivityNotFoundException e)
            {
                Intent rateIntent = rateIntentForUrl("http://play.google.com/store/apps/details");
                startActivity(rateIntent);
            }
        }

    }
    private Intent rateIntentForUrl(String url)
    {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(String.format("%s?id=%s", url, getPackageName())));
        int flags = Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_MULTIPLE_TASK;
        if (Build.VERSION.SDK_INT >= 21)
        {
            flags |= Intent.FLAG_ACTIVITY_NEW_DOCUMENT;
        }
        else
        {
            //noinspection deprecation
            flags |= Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET;
        }
        intent.addFlags(flags);
        return intent;
    }



    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        if (!Utility.isNetworkAvailable(MainActivity.this)) {
            Utility.displayToast(MainActivity.this, "Please turn on your internet connection.");
            return false;
        }

        int id = item.getItemId();
        if (id == R.id.navHome) {
            moveToScreen(new HomeFragment());
        }else if (id == R.id.navProfile) {
            moveToScreen(new ProfileFragment());
        } else if (id == R.id.navParty) {
            moveToScreen(new PartyProfileFragment());
        } else if (id == R.id.navWork) {
            moveToScreen(new WorkMainCategoryFragment());
        } else if (id == R.id.navJahirnama) {
            moveToScreen(new JahirnamaFragment());
        } else if (id == R.id.navServices) {
            ServiceRequest();
        } else if (id == R.id.navHelpingHand) {
            HelpingHandRequest();
        } else if (id == R.id.navMatdarJagruti) {
            moveToScreen(new MatdarJagrutiFragment());
        } else if (id == R.id.navSocialNetwork) {
            moveToScreen(new SocialNetworkFragment());
        } else if (id == R.id.navMessanger) {
            moveToScreen(new NotificationFragment());
        } else if (id == R.id.navEventOfWeek) {
            moveToScreen(new EOWFragment());
        } else if (id == R.id.navLogout) {
            displayPopUpForLogOut();

           /* FragmentManager fm = MainActivity.this.getSupportFragmentManager();
            for(int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                fm.popBackStack();
            }
            moveToScreen(new LoginFragment());*/
        } else if (id == R.id.nav_share) {
            Utility.displayToast(MainActivity.this, "Work in progress..");
        } else if (id == R.id.nav_send) {
            Utility.displayToast(MainActivity.this, "Work in progress..");
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    private Dialog dialog;

    private CustomEditText mobileEditText;
    private CustomButton okButton, cancelButton;

    /**
     * Display exit dialog.
     */
    private void displayPopUpForLogOut() {
        dialog = new Dialog(this);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        // here we set layout of progress dialog
        dialog.setContentView(R.layout.fragment_log_out_dialog);
        dialog.setCancelable(false);
        okButton = (CustomButton) dialog.findViewById(R.id.ok_button);
        cancelButton = (CustomButton) dialog.findViewById(R.id.cancel_button);
        dialog.show();
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Thread background = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            InstanceID.getInstance(getBaseContext()).deleteInstanceID();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
                background.start();

                PrefManager.setBooleanToPref(MainActivity.this, Constants.PreferencesManagerKey.IS_LOG_IN, false);
                PrefManager.setTabPref(MainActivity.this, Constants.PreferencesManagerKey.IS_TAB_VISIBLE, false);
                finish();
               /* FragmentManager fm = MainActivity.this.getSupportFragmentManager();
                for(int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                    fm.popBackStack();
                }
                moveToScreen(new LoginFragment());*/
                dialog.dismiss();
            }
        });
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }


    public void moveToScreen(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.MainContainer, fragment).commit();
    }

    private void HelpingHandRequest() {
        moveToScreen(new HelpingHandMainCategoryFragment());
    }


    public void addFragment(Fragment fragment, String tag) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.MainContainer, fragment).addToBackStack(tag).commit();

    }


    @Override
    public void onStop() {
        if (eventBus != null) {
            eventBus.unregister(this);
        }
        super.onStop();
    }

    @Override
    public void onPause() {
        if (eventBus != null) {
            eventBus.unregister(this);
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        if (eventBus != null) {
            eventBus.register(this);
        }
        super.onResume();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
       /* notificationString = getIntent().getStringExtra("IsFromNot");
        AccountManager accountManager = new AccountManager(this);
        accountManager.getTabVisibilityData();*/
    }

    public void onEvent(GenericErrorEvent event) {
        progress.dismiss();
    }

    public void onEvent(TabVisibilityEvent event) {
        progress.dismiss();
        if (event.visibilityResponse != null) {

            if (!TextUtils.isEmpty(event.visibilityResponse.message) && (event.visibilityResponse.status.equalsIgnoreCase(getString(R.string.server_success_ok)))) {
                // Set here  Data.
                ArrayList<TabVisibilityResponse.TabPreferences> tabPreferencesList = event.visibilityResponse.getTabpreferences();
                if (tabPreferencesList != null) {
                    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
                    setSupportActionBar(toolbar);
                   // toolbar.setTitleTextColor(0xFF000000);
//added

                    // Utility.displayToast(MainActivity.this, "Welcome to Nagari Suvidha.");
                    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                    ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                            this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
                    drawer.setDrawerListener(toggle);
                    toggle.syncState();

                    NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
                    navigationView.setNavigationItemSelectedListener(this);
                  //  tabPreferencesList.get(0).setProfilestatus("0");
                    if (tabPreferencesList.get(0).getProfilestatus().equalsIgnoreCase("1")) {
                        PrefManager.setTabPref(MainActivity.this, Constants.PreferencesManagerKey.IS_TAB_VISIBLE, true);
                        if (!Utility.isNetworkAvailable(MainActivity.this)) {
                            Utility.displayToast(MainActivity.this, getResources().getString(R.string.internet_connectivity_message));
                        } else {
                            if (notificationString != null && notificationString.equalsIgnoreCase("0")) {
                                navigationView.setCheckedItem(R.id.navEventOfWeek);
                                navigationView.getMenu().performIdentifierAction(R.id.navEventOfWeek, 9);
                                notificationString = null;
                            } else if (notificationString != null && notificationString.equalsIgnoreCase("1")) {
                                navigationView.setCheckedItem(R.id.navMessanger);
                                navigationView.getMenu().performIdentifierAction(R.id.navMessanger, 8);
                                notificationString = null;
                            } else {
                                /*navigationView.setCheckedItem(R.id.navProfile);
                                navigationView.getMenu().performIdentifierAction(R.id.navProfile, 0);*/
                                navigationView.setCheckedItem(R.id.navHome);
                                navigationView.getMenu().performIdentifierAction(R.id.navHome, 0);
                            }
                        }
                    } else {
                        PrefManager.setTabPref(MainActivity.this, Constants.PreferencesManagerKey.IS_TAB_VISIBLE, false);
                        if (notificationString != null && notificationString.equalsIgnoreCase("0")) {
                            navigationView.setCheckedItem(R.id.navEventOfWeek);
                            navigationView.getMenu().performIdentifierAction(R.id.navEventOfWeek, 9);
                            notificationString = null;
                        } else if (notificationString != null && notificationString.equalsIgnoreCase("1")) {
                            navigationView.setCheckedItem(R.id.navMessanger);
                            navigationView.getMenu().performIdentifierAction(R.id.navMessanger, 8);
                            notificationString = null;
                        } else {
                            Menu nav_Menu = navigationView.getMenu();
                            nav_Menu.findItem(R.id.navProfile).setVisible(false);
                            nav_Menu.findItem(R.id.navParty).setVisible(false);
                            nav_Menu.findItem(R.id.navJahirnama).setVisible(false);
                            nav_Menu.findItem(R.id.navWork).setVisible(false);
                            navigationView.setCheckedItem(R.id.navHome);
                            navigationView.getMenu().performIdentifierAction(R.id.navHome, 0);
                        }

                    }

                    //  View headerLayout = navigationView.inflateHeaderView(R.layout.nav_header_main);
        /*usernameTextDrawer  = (TextView) headerLayout.findViewById(R.id.usernameTextDrawer);

        if(PrefManager.getUsername(MainActivity.this) != null){
            usernameTextDrawer.setText("Hi, "+PrefManager.getUsername(MainActivity.this));
        }*/
                }
            } else {
                Utility.displayToast(this, event.visibilityResponse.message);
            }


        } else {
            Utility.displayToast(this, getString(R.string.msg_service_call_error));
        }
    }

    static final int ACTION_REQUEST_GALLERY = 123;
    static final int ACTION_REQUEST_CAMERA = 321;

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        switch (requestCode) {
            case ACTION_REQUEST_CAMERA:
                if (resultCode == Activity.RESULT_OK) {
                    Bitmap profileBitmapImage = (Bitmap) intent.getExtras().get("data");
                    updateImage(profileBitmapImage);
                }
                break;

            case ACTION_REQUEST_GALLERY:
                if (resultCode == Activity.RESULT_OK) {
                    try {
                        Uri imageUri = intent.getData();
                        Bitmap profileBitmapImage = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);
                        updateImage(profileBitmapImage);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
        }
    }

    private void updateImage(Bitmap profileBitmapImage) {
        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.MainContainer);
        if (currentFragment instanceof ComplaintsFragment) {
            ComplaintsFragment complaintsFragment = ((ComplaintsFragment) currentFragment);
            complaintsFragment.complaintImages.put(complaintsFragment.selectedPosition, profileBitmapImage);
            complaintsFragment.updateComplaintImageViews();
        }
    }

}
