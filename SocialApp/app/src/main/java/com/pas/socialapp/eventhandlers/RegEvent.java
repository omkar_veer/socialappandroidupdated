package com.pas.socialapp.eventhandlers;


import com.pas.socialapp.models.RegistrationResponse;

/**
 * Created by ganesh on 15/1/16.
 */
public class RegEvent {

    public RegistrationResponse registrationResponse;

    public RegEvent(RegistrationResponse registrationResponse) {
        this.registrationResponse = registrationResponse;
    }

}
