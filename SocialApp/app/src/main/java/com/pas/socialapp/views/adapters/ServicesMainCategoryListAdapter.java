package com.pas.socialapp.views.adapters;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.pas.socialapp.R;
import com.pas.socialapp.views.custom.CustomTextView;

import java.util.ArrayList;

public class ServicesMainCategoryListAdapter extends RecyclerView
        .Adapter<ServicesMainCategoryListAdapter
        .DataObjectHolder> {
    private static final String LOG_TAG = ServicesMainCategoryListAdapter.class.getSimpleName();
    private ArrayList<String> mDataset;
    private Context context;
    private ServiceClickListener serviceClickListener;

    public ServicesMainCategoryListAdapter(ArrayList<String> myDataset, Context cont) {
        mDataset = myDataset;
        this.context = cont;
    }

    @Override
    public ServicesMainCategoryListAdapter.DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.service_main_category_list_item, parent, false);

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(ServicesMainCategoryListAdapter.DataObjectHolder holder, final int position) {
        mDataset.get(position);
        if (mDataset.get(position) != null) {
            if(position==0){
                holder.requestTitle.setText(context.getString(R.string.emergency_services_text));
                holder.mainCategoryImageView.setImageResource(R.drawable.emergency_services);
            }else    if(position==1){
                holder.requestTitle.setText(context.getString(R.string.municipal_services_text));
                holder.mainCategoryImageView.setImageResource(R.drawable.mahanagarpalika_seva);
            } else   if(position==2){
                holder.requestTitle.setText(context.getString(R.string.commercial_services_text));
                holder.mainCategoryImageView.setImageResource(R.drawable.com_services);
            } else   if(position==3){
                holder.requestTitle.setText(context.getString(R.string.important_info_services_text));
                holder.mainCategoryImageView.setImageResource(R.drawable.important_info);
            }else   if(position==4){
                holder.requestTitle.setText(context.getString(R.string.medical_services_text));
                holder.mainCategoryImageView.setImageResource(R.drawable.medical_services);
            }
            //holder.requestTitle.setText(mDataset.get(position));
            holder.requestTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showServicesByCategory(mDataset.get(position));
                }

                private void showServicesByCategory(String serviceType) {
                    serviceClickListener.getServiceCategory(serviceType);
                }
            });

        }
    }

    public interface ServiceClickListener {
        public void getServiceCategory(String serviceType);
    }

    public void setOnServiceListener(ServiceClickListener serviceClickListener) {
        this.serviceClickListener = serviceClickListener;
    }


    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class DataObjectHolder extends RecyclerView.ViewHolder {
        CustomTextView requestTitle;
        ImageView mainCategoryImageView;

        public DataObjectHolder(View itemView) {
            super(itemView);
            requestTitle = (CustomTextView) itemView.findViewById(R.id.requestTitle);
            mainCategoryImageView = (ImageView) itemView.findViewById(R.id.serviceMainImageView);

        }
    }
}