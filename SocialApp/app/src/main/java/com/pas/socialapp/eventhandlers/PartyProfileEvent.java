package com.pas.socialapp.eventhandlers;

import com.pas.socialapp.models.PartyProfileResponse;

/**
 * Created by ganesh on 5/1/16.
 */
public class PartyProfileEvent {

    public PartyProfileResponse partyProfileResponse;

    public PartyProfileEvent(PartyProfileResponse partyProfileResponse) {
        this.partyProfileResponse = partyProfileResponse;
    }

}
