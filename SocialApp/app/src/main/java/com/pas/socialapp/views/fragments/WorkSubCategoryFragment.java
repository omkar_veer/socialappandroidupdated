package com.pas.socialapp.views.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pas.socialapp.Constants;
import com.pas.socialapp.R;
import com.pas.socialapp.eventhandlers.GenericErrorEvent;
import com.pas.socialapp.eventhandlers.WorkEvent;
import com.pas.socialapp.managers.AccountManager;
import com.pas.socialapp.models.WorkResponse;
import com.pas.socialapp.utils.Utility;
import com.pas.socialapp.views.adapters.WorkAdapter;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;

public class WorkSubCategoryFragment extends Fragment {

    private View rootView;
    private EventBus eventBus = EventBus.getDefault();
    private ProgressDialog progress;

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private Context context;
    String workType;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_work, container, false);
        Utility.floatingButtonVisibility(getActivity());
        workType = getArguments().getString("workType");
        initScreen();
        return rootView;
    }

    private void initScreen() {
        progress = new ProgressDialog(getActivity());
        progress.setCancelable(false);
        progress.setMessage(getString(R.string.msg_loading));


        context = getActivity();

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.profileRecyclerView);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(context);
        mRecyclerView.setLayoutManager(mLayoutManager);

        progress.show();
        AccountManager accountManager = new AccountManager(getActivity());
        accountManager.getWorkDetails(Constants.WORK_TAB);
    }

    @Override
    public void onStop() {
        if (eventBus != null) {
            eventBus.unregister(this);
        }
        super.onStop();
    }

    @Override
    public void onPause() {
        if (eventBus != null) {
            eventBus.unregister(this);
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        if (eventBus != null) {
            eventBus.register(this);
        }
        super.onResume();
    }

    public void onEvent(GenericErrorEvent event) {
        progress.dismiss();
    }
    public void onEvent(WorkEvent event) {
        progress.dismiss();
        if (event.workResponse != null) {

            if (!TextUtils.isEmpty(event.workResponse.message) && (event.workResponse.status.equalsIgnoreCase(getString(R.string.server_success_ok)))) {
                // Set here  Data.
                ArrayList<WorkResponse.Work> workData = event.workResponse.work;
                ArrayList<WorkResponse.Work> categoryWiseList = new ArrayList<WorkResponse.Work>();
                for (int i = 0; i <workData.size() ; i++) {
                    if(workType.equalsIgnoreCase(Constants.WORK_CATEGORY_1) && workData.get(i).getWorktype().equalsIgnoreCase(Constants.WORK_CATEGORY_1)){
                        categoryWiseList.add(workData.get(i));
                    }else  if(workType.equalsIgnoreCase(Constants.WORK_CATEGORY_2) && workData.get(i).getWorktype().equalsIgnoreCase(Constants.WORK_CATEGORY_2)){
                        categoryWiseList.add(workData.get(i));
                    }else  if(workType.equalsIgnoreCase(Constants.WORK_CATEGORY_3) && workData.get(i).getWorktype().equalsIgnoreCase(Constants.WORK_CATEGORY_3)){
                        categoryWiseList.add(workData.get(i));
                    }else  if(workType.equalsIgnoreCase(Constants.WORK_CATEGORY_4) && workData.get(i).getWorktype().equalsIgnoreCase(Constants.WORK_CATEGORY_4)){
                        categoryWiseList.add(workData.get(i));
                    }
                }
                if(categoryWiseList !=null) {
                    if (categoryWiseList.size() > 0) {
                        mAdapter = new WorkAdapter(categoryWiseList, context);
                        mRecyclerView.setAdapter(mAdapter);
                    }else{
                        Utility.displayToast(getActivity(), context.getResources().getString(R.string.no_data_available));
                    }
                }

            } else {
                Utility.displayToast(getActivity(), context.getResources().getString(R.string.no_data_available));
            }


        } else {
            Utility.displayToast(getActivity(), getString(R.string.msg_service_call_error));
        }
    }

}