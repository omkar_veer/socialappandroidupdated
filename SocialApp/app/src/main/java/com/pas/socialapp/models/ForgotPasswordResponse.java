package com.pas.socialapp.models;

import java.util.ArrayList;


public class ForgotPasswordResponse extends GenericRequest {

    public String status;
    public String service;
    public String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }



}
