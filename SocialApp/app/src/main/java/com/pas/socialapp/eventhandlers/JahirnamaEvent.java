package com.pas.socialapp.eventhandlers;

import com.pas.socialapp.models.JahirnamaResponse;

/**
 * Created by ganesh on 5/1/16.
 */
public class JahirnamaEvent {

    public JahirnamaResponse jahirnamaResponse;

    public JahirnamaEvent(JahirnamaResponse jahirnamaResponse) {
        this.jahirnamaResponse = jahirnamaResponse;
    }

}
