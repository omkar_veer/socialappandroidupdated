package com.pas.socialapp.views.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.pas.socialapp.R;
import com.pas.socialapp.models.Notification;

import java.util.List;

public class EOWAdapter extends RecyclerView.Adapter<EOWAdapter.ViewHolder> {
    public List<Notification> notificationList;
    static OnItemClickListener mItemClickListener;
    Context context;

    public EOWAdapter(Context context, List<Notification> notificationList) {
        this.notificationList = notificationList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.cardview_row_eventweek, null);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        Notification notification = notificationList.get(position);
        viewHolder.tvtTitle_text.setText(notification.getTitle());
        viewHolder.tvtDesc_text.setText(notification.getDescription());
        viewHolder.tvtPlace_text.setText(context.getResources().getString(R.string.lable_eventofweek_venue)+" "+ notification.getUrl());
        viewHolder.tvtDate_text.setText(context.getResources().getString(R.string.lable_eventofweek_date)+" "+notification.getType());

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return notificationList != null ? notificationList.size() : 0;
    }

    // inner class to hold a reference to each item of RecyclerView
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tvtTitle_text, tvtDesc_text,tvtPlace_text, tvtDate_text;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            tvtTitle_text = (TextView) itemLayoutView.findViewById(R.id.eov_title_text_view);
            tvtDesc_text = (TextView) itemLayoutView.findViewById(R.id.eov_desc_text_view);
            tvtPlace_text = (TextView) itemLayoutView.findViewById(R.id.eov_place_text_view);
            tvtDate_text = (TextView) itemLayoutView.findViewById(R.id.eov_date_text_view);
        }

        @Override
        public void onClick(View v) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(v, getPosition());
            }
        }
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public void updateData(List<Notification> notificationList) {
        this.notificationList = notificationList;
        notifyDataSetChanged();
    }

    public List<Notification> getNotificationList() {
        return notificationList;
    }

}
