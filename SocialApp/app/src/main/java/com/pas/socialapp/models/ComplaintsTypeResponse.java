package com.pas.socialapp.models;

import java.util.ArrayList;


public class ComplaintsTypeResponse extends GenericRequest {

    public String status;
    public String service;
    public String message;
    public ArrayList<ComplaintsType> complaintcategory = new ArrayList<ComplaintsType>();

    /**
     * @return The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return The service
     */
    public String getService() {
        return service;
    }

    /**
     * @param service The service
     */
    public void setService(String service) {
        this.service = service;
    }

    /**
     * @return The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<ComplaintsType> getComplaintcategory() {
        return complaintcategory;
    }

    public void setComplaintcategory(ArrayList<ComplaintsType> complaintcategory) {
        this.complaintcategory = complaintcategory;
    }

    public class ComplaintsType {

        public String complaintmasterid;

        public String complaintcategory;

        public String getComplaintcategoryMaster() {
            return complaintcategory;
        }

        public void setComplaintcategoryMaster(String complaintcategory) {
            this.complaintcategory = complaintcategory;
        }

        public String status;

        public String getComplaintmasterid() {
            return complaintmasterid;
        }

        public void setComplaintmasterid(String complaintmasterid) {
            this.complaintmasterid = complaintmasterid;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }

}
