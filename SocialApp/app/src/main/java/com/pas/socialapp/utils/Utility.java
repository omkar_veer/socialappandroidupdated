package com.pas.socialapp.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.pas.socialapp.R;
import com.pas.socialapp.views.activities.MainActivity;
import com.pas.socialapp.views.custom.CustomButton;
import com.pas.socialapp.views.custom.CustomEditText;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

public class Utility {
    private static Dialog internetConnectionDialog;
    public static String getDeviceId(Context ctx) {
        TelephonyManager tm = (TelephonyManager) ctx.getSystemService(Context.TELEPHONY_SERVICE);
        return tm.getDeviceId();
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static Bitmap retriveVideoFrameFromVideo(String videoPath)
            throws Throwable
    {
        Bitmap bitmap = null;
        MediaMetadataRetriever mediaMetadataRetriever = null;
        try
        {
            mediaMetadataRetriever = new MediaMetadataRetriever();
            if (Build.VERSION.SDK_INT >= 14)
                mediaMetadataRetriever.setDataSource(videoPath, new HashMap<String, String>());
            else
                mediaMetadataRetriever.setDataSource(videoPath);
            //   mediaMetadataRetriever.setDataSource(videoPath);
            bitmap = mediaMetadataRetriever.getFrameAtTime();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new Throwable(
                    "Exception in retriveVideoFrameFromVideo(String videoPath)"
                            + e.getMessage());

        }
        finally
        {
            if (mediaMetadataRetriever != null)
            {
                mediaMetadataRetriever.release();
            }
        }
        return bitmap;
    }

    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            return null;
        }
    }


    public final static boolean isValidEmail(String target) {
        if (target == null) {
            return false;
        } else {
            return Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public final static boolean isValidPhone(String target) {
        if (target == null) {
            return false;
        } else {
            return Patterns.PHONE.matcher(target).matches();
        }
    }

//    private static final String PASSWORD_PATTERN = "((?=.*[a-z])(?=.*\\d)(?=.*[A-Z])(?=.*[@#$%!]).{6,10})";

    public final static boolean isValidPassword(final String password) {
        if (password == null) {
            return false;
        } else {
//            Pattern pattern = Pattern.compile(PASSWORD_PATTERN);
//            Matcher matcher = pattern.matcher(password);
//            return matcher.matches();
            if (password.length() < 5)
                return false;
            else
                return true;
        }
    }


    public static void displayAlert(Context context, String title, String message) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, context.getString(android.R.string.ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    public static void displayToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public static void displayShortLenthToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }


    public static void hideSoftkeyboard(Context context) {
        View view = ((Activity) context).getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static void setTextWatcher(final CustomEditText customEditText) {

        TextWatcher textWatcher = new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                customEditText.setError(null);

            }

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

        };
        customEditText.addTextChangedListener(textWatcher);
    }

    public static boolean validateEditText(Context context, CustomEditText editText, String errorStr) {

        String text = editText.getText().toString().trim();
        String errorString = errorStr + " " + context.getString(R.string.required);
        if (TextUtils.isEmpty(text)) {

            editText.requestFocus();
            editText.setError(errorString);
            return false;
        }
        return true;
    }

    public static void displayInternetDialog(final Context context, final ExitListener exitListener) {

        if (internetConnectionDialog == null) {
            internetConnectionDialog = new Dialog(context);
            internetConnectionDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        }

        if (!internetConnectionDialog.isShowing()) {

            internetConnectionDialog.setContentView(R.layout.display_internet_dialog);

            internetConnectionDialog.setCancelable(false);

            // set button text for display Internet connectivity internetConnectionDialog
            CustomButton okButton = (CustomButton) internetConnectionDialog.findViewById(R.id.ok_button);
        /*    CustomButton settingButton = (CustomButton) internetConnectionDialog
                    .findViewById(R.id.setting_Button);*/

            okButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    internetConnectionDialog.dismiss();
                    internetConnectionDialog = null;
                    exitListener.exit();
                }
            });

         /*   settingButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    internetConnectionDialog.dismiss();
                    internetConnectionDialog = null;
                    Intent settingsIntent = new Intent(Settings.ACTION_SETTINGS);
                    context.startActivity(settingsIntent);

                }
            });*/

            internetConnectionDialog.show();
        }
    }

    public interface ExitListener {
        public void exit();
    }

    public static void floatingButtonVisibility(Context context){
        ImageView floatingActionButton = (ImageView) ((MainActivity) context).findViewById(R.id.fab);
        floatingActionButton.setVisibility(View.VISIBLE);
    }


    public static void shareData(Context context, String data){
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, data);
        sendIntent.setType("text/plain");
        context.startActivity(sendIntent);
    }

}
