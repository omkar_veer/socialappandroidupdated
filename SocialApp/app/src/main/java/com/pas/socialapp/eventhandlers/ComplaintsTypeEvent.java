package com.pas.socialapp.eventhandlers;

import com.pas.socialapp.models.ComplaintsTypeResponse;


public class ComplaintsTypeEvent {

    public ComplaintsTypeResponse typeResponse;

    public ComplaintsTypeEvent(ComplaintsTypeResponse typeResponse) {
        this.typeResponse = typeResponse;
    }

}
