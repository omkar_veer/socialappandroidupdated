package com.pas.socialapp.views.adapters;


import android.content.Context;
import android.content.res.ColorStateList;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.pas.socialapp.R;
import com.pas.socialapp.models.JahirnamaResponse;
import com.pas.socialapp.utils.Utility;
import com.pas.socialapp.views.custom.CustomTextView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class JahirnamaAdapter extends RecyclerView
        .Adapter<JahirnamaAdapter
        .DataObjectHolder> {
    private static final String LOG_TAG = JahirnamaAdapter.class.getSimpleName();
    private ArrayList<JahirnamaResponse.Jahirnama> mDataset;
    private Context context;

    public JahirnamaAdapter(ArrayList<JahirnamaResponse.Jahirnama> myDataset, Context cont) {
        mDataset = myDataset;
        this.context = cont;
    }


    @Override
    public JahirnamaAdapter.DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.jahirnama_list_item, parent, false);

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(JahirnamaAdapter.DataObjectHolder holder, int position) {
        mDataset.get(position);
        String data = "";
        if (mDataset.get(position).getDescription() != null) {
            holder.jahirnamaCustomTextView.setText(mDataset.get(position).getDescription());
            data += context.getString(R.string.lable_jahirnama) +  " : \n"+ mDataset.get(position).getDescription() + "\n";
        }

        if (mDataset.get(position).getImage() != null && !mDataset.get(position).getImage().isEmpty()) {
            Picasso.with(context).load(mDataset.get(position).getImage())
                    .error(R.drawable.banner00)
                    .placeholder(R.drawable.banner00)
                    .into(holder.jahirnamaImageView1);
            data += mDataset.get(position).getImage();
        } else {
            holder.jahirnamaImageView1.setVisibility(View.GONE);
        }

        holder.shareFab.setTag(data);
        holder.shareFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String shareData = (String)view.getTag();
                if (!TextUtils.isEmpty(shareData))
                    Utility.shareData(context, shareData);
                else
                    Utility.displayToast(context, context.getString(R.string.msg_service_call_error));
            }
        });


    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public static class DataObjectHolder extends RecyclerView.ViewHolder {

        CustomTextView jahirnamaCustomTextView;
        ImageView jahirnamaImageView1;
        ImageButton shareFab;

        public DataObjectHolder(View itemView) {
            super(itemView);
            jahirnamaCustomTextView = (CustomTextView) itemView.findViewById(R.id.jahirnama_description);
            jahirnamaImageView1 = (ImageView) itemView.findViewById(R.id.jahirnamaImageView);
            shareFab = (ImageButton) itemView.findViewById(R.id.jahirnamaShareFab);
        }
    }
}