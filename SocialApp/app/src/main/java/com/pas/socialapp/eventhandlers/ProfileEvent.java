package com.pas.socialapp.eventhandlers;

import com.pas.socialapp.models.ProfileResponse;

/**
 * Created by ganesh on 5/1/16.
 */
public class ProfileEvent {

    public ProfileResponse profileResponse;

    public ProfileEvent(ProfileResponse profileResponse) {
        this.profileResponse = profileResponse;
    }

}
