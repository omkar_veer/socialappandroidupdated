package com.pas.socialapp.views.fragments;

import android.app.ProgressDialog;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.pas.socialapp.Constants;
import com.pas.socialapp.R;
import com.pas.socialapp.eventhandlers.GenericErrorEvent;
import com.pas.socialapp.eventhandlers.PartyProfileEvent;
import com.pas.socialapp.managers.AccountManager;
import com.pas.socialapp.models.PartyProfileResponse;
import com.pas.socialapp.models.ProfileResponse;
import com.pas.socialapp.utils.Utility;
import com.pas.socialapp.views.custom.CustomTextView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;

public class PartyProfileFragment extends Fragment {

    private View rootView;
    private EventBus eventBus = EventBus.getDefault();
    private ProgressDialog progress;
    CustomTextView partyName, partyDtls;
    ImageView partyProfileImage;
    ImageView shareButton;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_party, container, false);
        Utility.floatingButtonVisibility(getActivity());
        initScreen();
        return rootView;
    }

    private void initScreen() {
        progress = new ProgressDialog(getActivity());
        progress.setCancelable(false);
        progress.setMessage(getString(R.string.msg_loading));
        partyName = (CustomTextView) rootView.findViewById(R.id.partyNameValue);
        partyDtls = (CustomTextView) rootView.findViewById(R.id.partyDetailsValue);
        partyProfileImage = (ImageView) rootView.findViewById(R.id.partyProfileImage);
        shareButton =  (ImageView) rootView.findViewById(R.id.partyShareFab);
        shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(data))
                    Utility.shareData(getContext(), data);
                else
                    Utility.displayToast(getContext(), getString(R.string.msg_service_call_error));
            }
        });

       /* FloatingActionButton fab = (FloatingActionButton) rootView.findViewById(R.id.partyShareFab);
        fab.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(getActivity(),R.color.colorPrimary)));
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(data))
                    Utility.shareData(getContext(), data);
                else
                    Utility.displayToast(getContext(), getString(R.string.msg_service_call_error));
            }
        });*/

        progress.show();
        AccountManager accountManager = new AccountManager(getActivity());
        accountManager.getPartyProfileDetails(Constants.PARTY_PROFILE_TAB);
    }

    @Override
    public void onStop() {
        if (eventBus != null) {
            eventBus.unregister(this);
        }
        super.onStop();
    }

    @Override
    public void onPause() {
        if (eventBus != null) {
            eventBus.unregister(this);
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        if (eventBus != null) {
            eventBus.register(this);
        }
        super.onResume();
    }

    public void onEvent(GenericErrorEvent event) {
        progress.dismiss();
    }

    String data = "";

    public void onEvent(PartyProfileEvent event) {
        progress.dismiss();
        if (event.partyProfileResponse != null) {

            if (!TextUtils.isEmpty(event.partyProfileResponse.message) && (event.partyProfileResponse.status.equalsIgnoreCase(getString(R.string.server_success_ok)))) {
                // Set here  Data.
                ArrayList<PartyProfileResponse.Partyprofile> partyprofile = event.partyProfileResponse.getPartyprofile();
                if (partyprofile.size() > 0) {
                    if (partyprofile.get(0).getPartyname() != null) {
                        partyName.setText(partyprofile.get(0).getPartyname());
                        data += getString(R.string.party_name_txt) + " : " + partyprofile.get(0).getPartyname() + "\n";
                    }
                    if (partyprofile.get(0).getPartydetails() != null) {
                        partyDtls.setText(partyprofile.get(0).getPartydetails());
                        data += getString(R.string.party_details_text) + " : " + partyprofile.get(0).getPartydetails() + "\n";
                    }

                    if(partyprofile.get(0).getPartyname()!=null && partyprofile.get(0).getPartyname().equalsIgnoreCase("Others")){
                        if (partyprofile.get(0).getOtherpartyimage() != null && !partyprofile.get(0).getOtherpartyimage().isEmpty()) {
                            Picasso.with(getActivity()).load(partyprofile.get(0).getOtherpartyimage())
                                    .error(R.drawable.banner00)
                                    .placeholder(R.drawable.banner00)
                                    .fit().centerInside()
                                    .into(partyProfileImage);

                            data += partyprofile.get(0).getOtherpartyimage();
                        } else {
                            partyProfileImage.setVisibility(View.GONE);
                        }
                    }
                    else{
                        if (partyprofile.get(0).getPartyimage() != null && !partyprofile.get(0).getPartyimage().isEmpty()) {
                            Picasso.with(getActivity()).load(partyprofile.get(0).getPartyimage())
                                    .error(R.drawable.banner00)
                                    .placeholder(R.drawable.banner00)
                                    .fit().centerInside()
                                    .into(partyProfileImage);

                            data += partyprofile.get(0).getPartyimage();
                        } else {
                            partyProfileImage.setVisibility(View.GONE);
                        }
                    }

                }

            } else {
                Utility.displayToast(getActivity(), event.partyProfileResponse.message);
            }


        } else {
            Utility.displayToast(getActivity(), getString(R.string.msg_service_call_error));
        }
    }


}