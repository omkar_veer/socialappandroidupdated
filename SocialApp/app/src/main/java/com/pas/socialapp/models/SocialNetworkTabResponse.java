package com.pas.socialapp.models;

import java.util.ArrayList;

public class SocialNetworkTabResponse extends GenericRequest {

    public String status;
    public String service;
    public String message;
    public ArrayList<SocialNetwork> socialurl = new ArrayList<SocialNetwork>();

    public ArrayList<SocialNetwork> getSocialurl() {
        return socialurl;
    }

    public void setSocialurl(ArrayList<SocialNetwork> socialurl) {
        this.socialurl = socialurl;
    }
    /**
     * @return The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return The service
     */
    public String getService() {
        return service;
    }

    /**
     * @param service The service
     */
    public void setService(String service) {
        this.service = service;
    }

    /**
     * @return The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

public class SocialNetwork{
    private String socialid;
    private String name;
    private String url;
    private String socialimage;
    private String candidateid;
    private String created;
    private String updated;
    private String status;

    public String getSocialimage() {
        return socialimage;
    }

    public void setSocialimage(String socialimage) {
        this.socialimage = socialimage;
    }

    public String getSocialid() {
        return socialid;
    }

    public void setSocialid(String socialid) {
        this.socialid = socialid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCandidateid() {
        return candidateid;
    }

    public void setCandidateid(String candidateid) {
        this.candidateid = candidateid;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }
}
}