package com.pas.socialapp.views.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TableRow;

import com.google.android.gms.iid.InstanceID;
import com.pas.socialapp.Constants;
import com.pas.socialapp.R;
import com.pas.socialapp.eventhandlers.GenericErrorEvent;
import com.pas.socialapp.managers.PrefManager;
import com.pas.socialapp.utils.Utility;
import com.pas.socialapp.views.activities.MainActivity;
import com.pas.socialapp.views.custom.CustomButton;
import com.pas.socialapp.views.custom.CustomTextView;

import java.util.Timer;
import java.util.TimerTask;

import de.greenrobot.event.EventBus;

import static com.pas.socialapp.R.id.serviceurl;

public class DevFragment extends Fragment {

    private View rootView;

    private CustomTextView mobileTextView,mobileOneTextView,mobileTwoTextView,webTextView,webOneTextView,emailTextView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_developer, container, false);
        ImageView floatingActionButton = (ImageView) ((MainActivity) getActivity()).findViewById(R.id.fab);
        floatingActionButton.setVisibility(View.GONE);
        initScreen();
        return rootView;
    }

    private void initScreen() {
        mobileTextView = (CustomTextView) rootView.findViewById(R.id.mobileTextView);
        Linkify.addLinks(mobileTextView, Linkify.PHONE_NUMBERS);
        mobileOneTextView = (CustomTextView) rootView.findViewById(R.id.mobileOneTextView);
        Linkify.addLinks(mobileOneTextView, Linkify.PHONE_NUMBERS);
        mobileTwoTextView = (CustomTextView) rootView.findViewById(R.id.mobileTwoTextView);
        Linkify.addLinks(mobileTwoTextView, Linkify.PHONE_NUMBERS);
        webTextView = (CustomTextView) rootView.findViewById(R.id.webTextView);
        Linkify.addLinks(webTextView, Linkify.WEB_URLS);
        webOneTextView = (CustomTextView) rootView.findViewById(R.id.webOneTextView);
        Linkify.addLinks(webOneTextView, Linkify.WEB_URLS);
        emailTextView = (CustomTextView) rootView.findViewById(R.id.emailTextView);
    }
}