package com.pas.socialapp.views.adapters;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import com.pas.socialapp.R;
import com.pas.socialapp.models.Notification;
import com.pas.socialapp.utils.Utility;
import com.squareup.picasso.Picasso;

import java.util.List;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {
    public List<Notification> notificationList;
    static OnItemClickListener mItemClickListener;
    Context context;
    BitmapDrawable bitmapDrawable, videoDrawable;

    public NotificationAdapter(Context context, List<Notification> notificationList) {
        this.notificationList = notificationList;
        this.context = context;
        Bitmap icon = BitmapFactory.decodeResource(context.getResources(),
                R.drawable.bg_audio);
        bitmapDrawable = new BitmapDrawable(icon);

        Bitmap videoIcon = BitmapFactory.decodeResource(context.getResources(),
                R.drawable.bg_video);
        videoDrawable = new BitmapDrawable(videoIcon);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.cardview_row_notifications, null);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int position) {
        Notification notification = notificationList.get(position);


        viewHolder.tvtinfo_text.setText(notification.getTitle());
        viewHolder.tvtDate_text.setText(notification.getTime());
        viewHolder.tvtDesc_text.setText(notification.getDescription());
      //  viewHolder.notificationShareFab.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(context,R.color.colorPrimary)));

        viewHolder.notificationShareFab.setTag(notification.getTitle() + "\n"
                + notification.getDescription() + "\n" + notification.getUrl());

        if (notification.getType() != null && notification.getType().equalsIgnoreCase("Audio")) {
            viewHolder.mPlayerContainer.setVisibility(View.VISIBLE);
            viewHolder.videoView.setVisibility(View.GONE);
            viewHolder.notificationImageView.setVisibility(View.GONE);
            viewHolder.audioPlayer.setBackgroundDrawable(bitmapDrawable);
            try {
                MediaController mediaController = new MediaController(context);
                mediaController.setAnchorView(viewHolder.audioPlayer);
                Uri video = Uri.parse(notification.getUrl());
                viewHolder.audioPlayer.setMediaController(mediaController);
                viewHolder.audioPlayer.setVideoURI(video);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (notification.getType() != null && notification.getType().equalsIgnoreCase("Video")) {
            viewHolder.videoView.setVisibility(View.VISIBLE);
            viewHolder.mPlayerContainer.setVisibility(View.GONE);
            viewHolder.notificationImageView.setVisibility(View.GONE);
//            viewHolder.videoView.setBackgroundDrawable(videoDrawable);
            try {
                MediaController mediaController = new MediaController(context);
                mediaController.setAnchorView(viewHolder.videoView);
                Uri video = Uri.parse(notification.getUrl());
                viewHolder.videoView.setMediaController(mediaController);
                viewHolder.videoView.setVideoURI(video);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (notification.getType() != null && notification.getType().equalsIgnoreCase("Image")) {
            viewHolder.notificationImageView.setVisibility(View.VISIBLE);
            viewHolder.mPlayerContainer.setVisibility(View.GONE);
            viewHolder.videoView.setVisibility(View.GONE);
            Picasso.with(context).load(notification.getUrl())
                    .error(R.drawable.banner00)
                    .placeholder(R.drawable.banner00)
                    .into(viewHolder.notificationImageView);
        } else {
            viewHolder.notificationShareFab.setTag(notification.getTitle() + "\n"
                    + notification.getDescription());
            viewHolder.mPlayerContainer.setVisibility(View.GONE);
            viewHolder.videoView.setVisibility(View.GONE);
            viewHolder.notificationImageView.setVisibility(View.GONE);
        }

        viewHolder.notificationShareFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String data = (String) view.getTag();
                if (!TextUtils.isEmpty(data))
                    Utility.shareData(context, data);
                else
                    Utility.displayToast(context, context.getString(R.string.msg_service_call_error));
            }
        });

        viewHolder.videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                viewHolder.videoView.setBackgroundDrawable(null);
            }
        });
        /*viewHolder.videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                viewHolder.videoView.setBackgroundDrawable(videoDrawable);

            }
        });*/


    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return notificationList != null ? notificationList.size() : 0;
    }

    // inner class to hold a reference to each item of RecyclerView
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tvtinfo_text, tvtDate_text, tvtDesc_text;
        private ViewGroup mPlayerContainer;
        //        private FullscreenVideoLayout videoLayout;
        private ImageView notificationImageView;
        private VideoView videoView;
        VideoView audioPlayer;
        ImageButton notificationShareFab;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            tvtDesc_text = (TextView) itemLayoutView.findViewById(R.id.desc_text_view);
            tvtinfo_text = (TextView) itemLayoutView.findViewById(R.id.info_text_view);
            tvtDate_text = (TextView) itemLayoutView.findViewById(R.id.date_text_view);
            mPlayerContainer = (ViewGroup) itemLayoutView.findViewById(R.id.player_layout);
            audioPlayer = (VideoView) itemLayoutView.findViewById(R.id.audioview);
            videoView = (VideoView) itemLayoutView.findViewById(R.id.videoview);
            notificationImageView = (ImageView) itemLayoutView.findViewById(R.id.notificationImageView);
            notificationShareFab = (ImageButton) itemLayoutView.findViewById(R.id.notificationShareFab);
        }

        @Override
        public void onClick(View v) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(v, getPosition());
            }
        }
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public void updateData(List<Notification> notificationList) {
        this.notificationList = notificationList;
        notifyDataSetChanged();
    }

    public List<Notification> getNotificationList() {
        return notificationList;
    }

}
