package com.pas.socialapp.models;

/**
 * Created by ganesh on 5/1/16.
 */
public class TalukaRequest extends GenericRequest {

    public TalukaRequest(String district) {
        this.district = district;
    }

    public String district;
}
