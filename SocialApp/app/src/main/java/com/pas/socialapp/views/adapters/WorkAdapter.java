package com.pas.socialapp.views.adapters;


import android.content.Context;
import android.content.res.ColorStateList;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pas.socialapp.R;
import com.pas.socialapp.models.WorkResponse;
import com.pas.socialapp.utils.Utility;
import com.pas.socialapp.views.custom.CustomTextView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class WorkAdapter extends RecyclerView
        .Adapter<WorkAdapter
        .DataObjectHolder> {
    private static final String LOG_TAG = WorkAdapter.class.getSimpleName();
    private ArrayList<WorkResponse.Work> mDataset;
    private Context context;


    public WorkAdapter(ArrayList<WorkResponse.Work> myDataset, Context cont) {
        mDataset = myDataset;
        this.context = cont;
    }


    @Override
    public WorkAdapter.DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.work_list_item, parent, false);

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(WorkAdapter.DataObjectHolder holder, int position) {
        mDataset.get(position);
        String data = "";
        if (mDataset.get(position).getWorktype() != null && !mDataset.get(position).getWorktype().isEmpty()) {
            /*if(mDataset.get(position).getWorktype().equalsIgnoreCase("Development Work")){
                holder.worktype.setText(context.getString(R.string.development_work_text));
            }else if (mDataset.get(position).getWorktype().equalsIgnoreCase("Social Work")){
                holder.worktype.setText(context.getString(R.string.social_work_text));
            }else if(mDataset.get(position).getWorktype().equalsIgnoreCase("Sport Work")){
                holder.worktype.setText(context.getString(R.string.sport_work_text));
            }else if (mDataset.get(position).getWorktype().equalsIgnoreCase("Religious Work")){
                holder.worktype.setText(context.getString(R.string.religious_work_text));
            }*/
            //holder.worktype.setText(mDataset.get(position).getWorktype());
        } else {
            holder.worktype.setText("");
            holder.workTypelayout.setVisibility(View.GONE);

        }
        if (mDataset.get(position).getDescription() != null && !mDataset.get(position).getDescription().isEmpty()) {
            holder.workdesc.setText(mDataset.get(position).getDescription());
            data += context.getString(R.string.lable_work_menu) +  " : \n"+ mDataset.get(position).getDescription() + "\n";
        } else {
            holder.workdesc.setText("");
            holder.workDescLayout.setVisibility(View.GONE);
        }

        if (mDataset.get(position).getImage() != null && !mDataset.get(position).getImage().isEmpty()) {
            Picasso.with(context).load(mDataset.get(position).getImage())
                    .error(R.drawable.banner00)
                    .placeholder(R.drawable.banner00)
                    .into(holder.workImageView1);
            data += mDataset.get(position).getImage();
        } else {
            holder.workImageView1.setVisibility(View.GONE);
        }


        holder.shareFab.setTag(data);
        holder.shareFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String shareData = (String)view.getTag();
                if (!TextUtils.isEmpty(shareData))
                    Utility.shareData(context, shareData);
                else
                    Utility.displayToast(context, context.getString(R.string.msg_service_call_error));
            }
        });


    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public static class DataObjectHolder extends RecyclerView.ViewHolder {

        private TextView worktype;
        private TextView workdesc;
        private ImageView workImageView1;
        private LinearLayout workTypelayout, workDescLayout;
        private ImageButton shareFab;

        public DataObjectHolder(View itemView) {
            super(itemView);
            worktype = (CustomTextView) itemView.findViewById(R.id.worktypeValue);
            workdesc = (CustomTextView) itemView.findViewById(R.id.workdescriptionValue);
            workImageView1 = (ImageView) itemView.findViewById(R.id.workImageView1);
            workTypelayout = (LinearLayout) itemView.findViewById(R.id.worktypelayout);
            workDescLayout = (LinearLayout) itemView.findViewById(R.id.worktypedesclayout);
            shareFab  = (ImageButton) itemView.findViewById(R.id.workShareFab);

        }
    }
}