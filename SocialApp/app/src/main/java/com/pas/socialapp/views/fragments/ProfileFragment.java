package com.pas.socialapp.views.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.pas.socialapp.Constants;
import com.pas.socialapp.R;
import com.pas.socialapp.eventhandlers.GCMRegistrationEvent;
import com.pas.socialapp.eventhandlers.GenericErrorEvent;
import com.pas.socialapp.eventhandlers.ProfileEvent;
import com.pas.socialapp.managers.AccountManager;
import com.pas.socialapp.managers.PrefManager;
import com.pas.socialapp.models.ProfileResponse;
import com.pas.socialapp.utils.Utility;
import com.pas.socialapp.views.activities.OnboardingActivity;
import com.pas.socialapp.views.adapters.ProfileAdapter;

import java.io.IOException;
import java.util.ArrayList;

import de.greenrobot.event.EventBus;

public class ProfileFragment extends Fragment {

    private View rootView;
    private EventBus eventBus = EventBus.getDefault();
    private ProgressDialog progress;
    // private ProfileAdapter adapter;

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private Context context;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_profile, container, false);
        Utility.floatingButtonVisibility(getActivity());
        gcmRegistrationInBackground();
        initScreen();
        return rootView;
    }

    private void initScreen() {
        progress = new ProgressDialog(getActivity());
        progress.setCancelable(false);
        progress.setMessage(getString(R.string.msg_loading));

        context = getActivity();

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.profileRecyclerView);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(context);
        mRecyclerView.setLayoutManager(mLayoutManager);

        FloatingActionButton fab = (FloatingActionButton) rootView.findViewById(R.id.profileShareFab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(data))
                    Utility.shareData(getContext(), data);
                else
                    Utility.displayToast(getContext(), getString(R.string.msg_service_call_error));
            }
        });

        progress.show();
        AccountManager accountManager = new AccountManager(getActivity());
        accountManager.getProfileDetails(Constants.PROFILE_TAB);


    }

    private void gcmRegistrationInBackground(){
        if (TextUtils.isEmpty(PrefManager.getGCMRegId(getActivity()))) {
            Log.d("GCM REg Id is: -->>",PrefManager.getGCMRegId(getActivity()));
            doGcmRegistration();
        } else {

        }
    }


    private boolean isGooglePlayServicesAvailable() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, getActivity(),
                        Constants.PLAY_SERVICES_RESOLUTION_REQUEST).show();
//                if (progress != null) {
//                    progress.dismiss();
//                }
            } else {
                Toast.makeText(getActivity(),
                        "This device doesn't support Play services, App will not receive notifications.",
                        Toast.LENGTH_LONG).show();
            }
            return false;
        }
        return true;
    }


    private void doGcmRegistration() {
        //      if (TextUtils.isEmpty(PrefManager.getGCMRegId(getActivity()))) {
        if (Utility.isNetworkAvailable(getActivity())) {
            if (isGooglePlayServicesAvailable()) {
                new GcmRegTask().execute();
            }
        } else {
            Toast.makeText(getActivity(), "Please turn on your internet to receive notifications.", Toast.LENGTH_LONG).show();
        }
        // }
    }

    private class GcmRegTask extends AsyncTask<Void, String, String> {

        /**
         * The Gcm obj.
         */
        GoogleCloudMessaging gcmObj;
        /**
         * The Reg id.
         */
        String regId;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... params) {
            if (gcmObj == null) {
                gcmObj = GoogleCloudMessaging.getInstance(getActivity());
            }
            try {
                regId = gcmObj.register(Constants.GCM_PROJECT_ID);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return regId;
        }

        @Override
        protected void onPostExecute(String result) {
            if (!TextUtils.isEmpty(regId)) {
                Log.d("GCM_Key", "" + regId);
                PrefManager.setGCMRegId(getActivity(), regId);
                AccountManager accountManager = new AccountManager(getActivity());
                accountManager.sendGCMRegIdToServer(PrefManager.getUserPhone(getActivity()), regId, getDeviceId(getActivity()));
                //  Toast.makeText(getActivity(), "App registered for notifications", Toast.LENGTH_LONG).show();
            } else {
                //  Toast.makeText(getActivity(), "App will not receive push notification, please try again.", Toast.LENGTH_LONG).show();

            }
            super.onPostExecute(result);
        }
    }


    public void onEvent(GCMRegistrationEvent event) {
        progress.dismiss();
        if (event.registrationResponse != null) {

            if (!TextUtils.isEmpty(event.registrationResponse.message) && (event.registrationResponse.status.equalsIgnoreCase(getString(R.string.server_success_ok)))) {
              //  Utility.displayToast(getActivity(), event.registrationResponse.message);
            } else {
                //Utility.displayToast(getActivity(), event.registrationResponse.message);
            }
        } else {
            Utility.displayToast(getActivity(), getString(R.string.msg_service_call_error));
        }
    }

    public static String getDeviceId(Context context) {

        final String macAddr, androidId;
        WifiManager wifiMan = (WifiManager) context
                .getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInf = wifiMan.getConnectionInfo();
        macAddr = wifiInf.getMacAddress();
        androidId = ""
                + android.provider.Settings.Secure.getString(
                context.getContentResolver(),
                android.provider.Settings.Secure.ANDROID_ID);
        String UUID = androidId.hashCode() + "" + macAddr.hashCode();
        // Maybe save this: deviceUuid.toString()); to the preferences.
        return UUID;
    }

    @Override
    public void onStop() {
        if (eventBus != null) {
            eventBus.unregister(this);
        }
        super.onStop();
    }

    @Override
    public void onPause() {
        if (eventBus != null) {
            eventBus.unregister(this);
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        if (eventBus != null) {
            eventBus.register(this);
        }
        super.onResume();
    }

    public void onEvent(GenericErrorEvent event) {
        progress.dismiss();
    }

    public void onEvent(ProfileEvent event) {
        progress.dismiss();
        if (event.profileResponse != null) {

            if (!TextUtils.isEmpty(event.profileResponse.message) && (event.profileResponse.status.equalsIgnoreCase(getString(R.string.server_success_ok)))) {
                // Set here profile Data.
                ArrayList<ProfileResponse.Profile> profileData = event.profileResponse.getProfile();
                if (profileData != null) {
                    if (profileData.size() > 0) {
                        mAdapter = new ProfileAdapter(profileData, context);
                        mRecyclerView.setAdapter(mAdapter);
                        getShareData(profileData);
                    }
                }

            } else {
                Utility.displayToast(getActivity(), event.profileResponse.message);
            }


        } else {
            Utility.displayToast(getActivity(), getString(R.string.msg_service_call_error));
        }
    }

    String data = "";

    public void getShareData(ArrayList<ProfileResponse.Profile> profileData) {
        data = "";
        for (ProfileResponse.Profile profile : profileData) {
            data += getString(R.string.name_text) + " : " + profile.getFirstname() + " " + profile.getMiddlename() + " " + profile.getLastname() + "\n";
            data += getString(R.string.party_name_text) + " : " + profile.getPartyname() + "\n";
            if (!TextUtils.isEmpty(profile.getEmail())) {
                data += getString(R.string.lable_email) + " : " + profile.getEmail() + "\n";
            }
            if (!TextUtils.isEmpty(profile.getWard())) {
                data += getString(R.string.lable_ward) + " : " + profile.getWard() + "\n";
            }
            if (!TextUtils.isEmpty(profile.getArea())) {
                data += getString(R.string.lable_area) + " : " + profile.getArea() + "\n";
            }
        }
    }

}