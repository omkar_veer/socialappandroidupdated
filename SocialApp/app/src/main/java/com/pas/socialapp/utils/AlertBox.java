package com.pas.socialapp.utils;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;

public class AlertBox {

	public static void showAlertDialog(final Context ctx, final String title, final String message) {
		
		((Activity)ctx).runOnUiThread(new Runnable() {
			public void run() {

				AlertDialog alertDialog = new AlertDialog.Builder(ctx).create();
				alertDialog.setTitle(title);
				alertDialog.setMessage(message);
				alertDialog.setCanceledOnTouchOutside(true);
				alertDialog.show();

			}
			});
	}
	
	public static void showAlertDialog(final Context ctx, final String title, final String message, final OnCancelListener cancelListener) {
		
		((Activity)ctx).runOnUiThread(new Runnable() {
			public void run() {

				AlertDialog alertDialog = new AlertDialog.Builder(ctx).create();
				alertDialog.setTitle(title);
				alertDialog.setMessage(message);		
				alertDialog.setCanceledOnTouchOutside(true);
				alertDialog.setOnCancelListener(cancelListener);
				alertDialog.show();

			}
			});
	}
	
	public static void showAlertDialogOK(final Context ctx, final String title, final String message, final String btnTxt) {
		
		((Activity)ctx).runOnUiThread(new Runnable() {
			public void run() {

				new AlertDialog.Builder(ctx)
				.setTitle(title)
				.setMessage(message)
				.setNeutralButton(btnTxt,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.dismiss();
							}
				}).show();

			}
			});
	}
}
