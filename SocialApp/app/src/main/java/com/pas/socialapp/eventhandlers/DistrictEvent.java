package com.pas.socialapp.eventhandlers;

import com.pas.socialapp.models.DistrictResponse;
import com.pas.socialapp.models.StateResponse;

/**
 * Created by Umesh on 24/02/16.
 */
public class DistrictEvent {

    public DistrictResponse districtResponse;

    public DistrictEvent(DistrictResponse districtresponse) {
        this.districtResponse = districtresponse;
    }

}
