package com.pas.socialapp.views.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pas.socialapp.R;
import com.pas.socialapp.eventhandlers.ServiceCategoryEvent;
import com.pas.socialapp.managers.AccountManager;
import com.pas.socialapp.models.ServiceCategoryResponse;
import com.pas.socialapp.utils.Utility;
import com.pas.socialapp.views.adapters.ServicesSubCategoryListAdapter;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;

public class ServicesSubCategoryListFragment extends Fragment implements ServicesSubCategoryListAdapter.OpenURLListener {

    private View rootView;
    private EventBus eventBus = EventBus.getDefault();
    private ProgressDialog progress;

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private Context context;
    String serviceType;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        serviceType = getArguments().getString("serviceType");
        rootView = inflater.inflate(R.layout.service_sub_category_fragment, container, false);
        Utility.floatingButtonVisibility(getActivity());
        initScreen();
        return rootView;
    }

    private void initScreen() {
        progress = new ProgressDialog(getActivity());
        progress.setCancelable(false);
        progress.setMessage(getString(R.string.msg_loading));


        context = getActivity();

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.serviceSubRecyclerView);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(context);
        mRecyclerView.setLayoutManager(mLayoutManager);


        progress.show();

        AccountManager accountManager = new AccountManager(getActivity());
        accountManager.getServicesByCategory(serviceType);
    }

    @Override
    public void onStop() {
        if (eventBus != null) {
            eventBus.unregister(this);
        }
        super.onStop();
    }

    @Override
    public void onPause() {
        if (eventBus != null) {
            eventBus.unregister(this);
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        if (eventBus != null) {
            eventBus.register(this);
        }
        super.onResume();
    }

    public void onEvent(ServiceCategoryEvent event) {
        progress.dismiss();
        if (event.serviceCategoryResponse != null) {

            if (!TextUtils.isEmpty(event.serviceCategoryResponse.message) && (event.serviceCategoryResponse.status.equalsIgnoreCase(getString(R.string.server_success_ok)))) {
                // Set here  Data.
                ArrayList<ServiceCategoryResponse.Servicetype> servicetypeArrayList = event.serviceCategoryResponse.getServicetype();

                if (servicetypeArrayList != null) {
                    if (servicetypeArrayList.size() > 0) {
                        mAdapter = new ServicesSubCategoryListAdapter(servicetypeArrayList, context);
                        mRecyclerView.setAdapter(mAdapter);
                        ((ServicesSubCategoryListAdapter) mAdapter).openURLServiceListener(ServicesSubCategoryListFragment.this);
                    }else{
                        Utility.displayToast(getActivity(),  context.getResources().getString(R.string.no_data_available));
                    }
                }
            } else {
                Utility.displayToast(getActivity(), event.serviceCategoryResponse.message);
            }

        } else {
            Utility.displayToast(getActivity(), getString(R.string.msg_service_call_error));
        }
    }


    @Override
    public void openURLinWEbView(String url) {
        if (!url.startsWith("http://") && !url.startsWith("https://"))
            url = "http://" + url;

        Uri uri = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);


       /* Fragment urlWebView = new URLWebView();
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Bundle bundle = new Bundle();
        bundle.putString(Constants.WEB_URL, url);
        urlWebView.setArguments(bundle);
        fragmentTransaction.add(R.id.MainContainer, urlWebView);
        fragmentTransaction.addToBackStack(ServiceTypeCategoryListFragment.class.getSimpleName());
        fragmentTransaction.commit();*/
    }
}