package com.pas.socialapp;



public class Constants {

    //public final static String CANDIDATE_ID = "74";   // Change ID here wrt to Candidate name & Id

    public final static String ROLE = "citizen";

    public final static String LOCAL_DATABASE_NAME = "NagariSuvidha.db";

    public final static String PROFILE_TAB = "profile";

    public final static String WORK_TAB = "work";

    public final static String PARTY_PROFILE_TAB = "partyprofile";

    public final static String JAHIRNAMA_TAB = "jahirnama";

    public static final String SERVER_BASE_URL = "http://www.nagarisuvidha.com/sapp/backoffice/webservice/";

    //  public static final String SERVER_BASE_URL = "http://www.anithedesigner.in/sapp/backoffice/webservice/";

    public static final String REGISTRATION_URL = SERVER_BASE_URL + "citizenregister.php";

    public static final String LOGIN_URL = SERVER_BASE_URL + "citizenlogin.php";

    public static final String VERIFY_OTP_URL = SERVER_BASE_URL + "verifycitizen.php";

    public static final String RESEND_OTP_URL = SERVER_BASE_URL + "resendotp.php";

    public static final String STATES_URL = SERVER_BASE_URL + "getallstates.php";

    public static final String CASTE_URL = SERVER_BASE_URL + "getcastemaster.php";

    public static final String RELIGION_URL = SERVER_BASE_URL + "getreligionmaster.php";

    public static final String DISTRICT_URL = SERVER_BASE_URL + "getalldistricts.php";

    public static final String TALUKA_URL = SERVER_BASE_URL + "getalltalukas.php";

    public static final String PINCODE_URL = SERVER_BASE_URL + "getallpincodes.php";

    public static final String CANDIDATE_DATA = SERVER_BASE_URL + "getcandidatedata.php";

    public static final String WORK_JAHIRNAMA_DATA = SERVER_BASE_URL + "getalljahirnamaorwork.php";

    public static final String GET_SERVICE_CATEGORYS = SERVER_BASE_URL + "getservicetype.php";

    public static final String GET_MATDAR_JAGRUTI_DATA_URL = SERVER_BASE_URL + "getmjdata.php";

    public static final String GET_HELPING_HAND_SERVICE_CATEGORYS = SERVER_BASE_URL + "gethhdata.php";

    public static final String SEND_GCM_REG_ID_TO_SERVER = SERVER_BASE_URL + "registerdevice.php";

    public static final String GET_SOCIAL_NETWORK_DATA_URL = SERVER_BASE_URL + "socialurl_mobile.php";

    public static final String GET_TAB_VISIBILITY_DATA_URL = SERVER_BASE_URL + "gettabpreferences.php";

    public static final String GET_COMPLAINTS_MASTER_DATA_URL = SERVER_BASE_URL + "getcomplaintcategorymaster.php";

    public static final String ADD_COMPLAINT_URL = "http://www.nagarisuvidha.com/sapp/backoffice/webservice/addcomplaint.php";

    public static final String FORGOT_PASSWORD_URL ="http://www.nagarisuvidha.com/sapp/backoffice/webservice/forgotpassword.php";

    public static final String SERVICE_CATEGORY_1 = "Emergency Services";
    public static final String SERVICE_CATEGORY_2 = "Municipal Services";
    public static final String SERVICE_CATEGORY_3 = "Commercial Services";
    public static final String SERVICE_CATEGORY_5 = "Medical Assistance Services";
    public static final String SERVICE_CATEGORY_4 = "Important Information";

    public static final String HH_CATEGORY_1 = "New Businessmen";
    public static final String HH_CATEGORY_2 = "Skill Development";
    public static final String HH_CATEGORY_3 = "Government Loan Schemes";
    public static final String HH_CATEGORY_4 = "PMC Schemes";
    public static final String HH_CATEGORY_5 = "Utility Bills";

    public static final String WORK_CATEGORY_1 = "Social Work";
    public static final String WORK_CATEGORY_2= "Development Work";
    public static final String WORK_CATEGORY_3 = "Religious Work";
    public static final String WORK_CATEGORY_4 = "Sports Work";

    public static final String LOG_TAG = "SocialApp";

    public static final boolean ISLOGGABLE = true;

    public static final String TABLET_7_INCH = "TABLET_7_INCH";

    public static final String TABLET_10_INCH = "TABLET_10_INCH";

    public static final String PHONE = "PHONE";

    public final static String GCM_PROJECT_ID = "630590327389";

    public final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    public final static String REQUEST_FROM = "APP";

    public final static String WEB_URL = "web_url";

    public static class PreferencesManagerKey {
        public static final String CTID = "CTID";
        public static final String IS_LOG_IN = "0";
        public static final String IS_TAB_VISIBLE = "0";
    }

    public final static String NOTIFICATION_DESC_KEY = "description";

    public final static String NOTIFICATION_TITLE_KEY = "title";

    public final static String NOTIFICATION_URL_KEY = "path";

    public final static String NOTIFICATION_TYPE_KEY = "media_type";

    public final static String NOTIFICATION_VENUE_KEY = "eventvenue";

    public final static String NOTIFICATION_DATE_KEY = "eventdate";

}
