package com.pas.socialapp.models;

import java.util.ArrayList;

/**
 * Created by Umesh  on 24/02/16.
 */
public class HelpingHandCategoryResponse extends GenericRequest {

    public String status;
    public String service;
    public String message;
    public ArrayList<Hhdatum> hhdata = new ArrayList<Hhdatum>();

    /**
     * @return The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return The service
     */
    public String getService() {
        return service;
    }

    /**
     * @param service The service
     */
    public void setService(String service) {
        this.service = service;
    }

    /**
     * @return The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return The hhdata
     */
    public ArrayList<Hhdatum> getHhdata() {
        return hhdata;
    }

    /**
     * @param hhdata The hhdata
     */
    public void setHhdata(ArrayList<Hhdatum> hhdata) {
        this.hhdata = hhdata;
    }


    public class Hhdatum {

        private String hhdataid;
        private String mainhhcategory;
        private String hhcategoryname;
        private String dataurl;
        private String pdffile;
        private String candidateid;
        private String created;
        private String updated;
        private String status;
        private String hhstatus;
        private String hhurl;

        /**
         * @return The hhdataid
         */
        public String getHhdataid() {
            return hhdataid;
        }

        /**
         * @param hhdataid The hhdataid
         */
        public void setHhdataid(String hhdataid) {
            this.hhdataid = hhdataid;
        }

        /**
         * @return The mainhhcategory
         */
        public String getMainhhcategory() {
            return mainhhcategory;
        }

        /**
         * @param mainhhcategory The mainhhcategory
         */
        public void setMainhhcategory(String mainhhcategory) {
            this.mainhhcategory = mainhhcategory;
        }

        /**
         * @return The hhcategoryname
         */
        public String getHhcategoryname() {
            return hhcategoryname;
        }

        /**
         * @param hhcategoryname The hhcategoryname
         */
        public void setHhcategoryname(String hhcategoryname) {
            this.hhcategoryname = hhcategoryname;
        }

        /**
         * @return The dataurl
         */
        public String getDataurl() {
            return dataurl;
        }

        /**
         * @param dataurl The dataurl
         */
        public void setDataurl(String dataurl) {
            this.dataurl = dataurl;
        }

        /**
         * @return The pdffile
         */
        public String getPdffile() {
            return pdffile;
        }

        /**
         * @param pdffile The pdffile
         */
        public void setPdffile(String pdffile) {
            this.pdffile = pdffile;
        }

        /**
         * @return The candidateid
         */
        public String getCandidateid() {
            return candidateid;
        }

        /**
         * @param candidateid The candidateid
         */
        public void setCandidateid(String candidateid) {
            this.candidateid = candidateid;
        }

        /**
         * @return The created
         */
        public String getCreated() {
            return created;
        }

        /**
         * @param created The created
         */
        public void setCreated(String created) {
            this.created = created;
        }

        /**
         * @return The updated
         */
        public String getUpdated() {
            return updated;
        }

        /**
         * @param updated The updated
         */
        public void setUpdated(String updated) {
            this.updated = updated;
        }

        /**
         * @return The status
         */
        public String getStatus() {
            return status;
        }

        /**
         * @param status The status
         */
        public void setStatus(String status) {
            this.status = status;
        }

        /**
         * @return The hhstatus
         */
        public String getHhstatus() {
            return hhstatus;
        }

        /**
         * @param hhstatus The hhstatus
         */
        public void setHhstatus(String hhstatus) {
            this.hhstatus = hhstatus;
        }

        /**
         * @return The hhurl
         */
        public String getHhurl() {
            return hhurl;
        }

        /**
         * @param hhurl The hhurl
         */
        public void setHhurl(String hhurl) {
            this.hhurl = hhurl;
        }
    }
}