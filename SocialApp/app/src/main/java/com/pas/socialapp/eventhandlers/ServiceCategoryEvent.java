package com.pas.socialapp.eventhandlers;

import com.pas.socialapp.models.ServiceCategoryResponse;

/**
 * Created by Umesh on 24/02/16.
 */
public class ServiceCategoryEvent {

    public ServiceCategoryResponse serviceCategoryResponse;

    public ServiceCategoryEvent(ServiceCategoryResponse serviceCategoryResponse) {
        this.serviceCategoryResponse = serviceCategoryResponse;
    }

}
