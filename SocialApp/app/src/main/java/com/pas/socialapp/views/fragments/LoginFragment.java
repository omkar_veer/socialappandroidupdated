package com.pas.socialapp.views.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.pas.socialapp.Constants;
import com.pas.socialapp.R;
import com.pas.socialapp.eventhandlers.ForgotPasswordEvent;
import com.pas.socialapp.eventhandlers.GCMRegistrationEvent;
import com.pas.socialapp.eventhandlers.LoginEvent;
import com.pas.socialapp.eventhandlers.OTPverifyEvent;
import com.pas.socialapp.eventhandlers.ResendOTPverifyEvent;
import com.pas.socialapp.managers.AccountManager;
import com.pas.socialapp.managers.PrefManager;
import com.pas.socialapp.utils.Utility;
import com.pas.socialapp.views.activities.OnboardingActivity;
import com.pas.socialapp.views.custom.CustomButton;
import com.pas.socialapp.views.custom.CustomEditText;
import com.pas.socialapp.views.custom.CustomTextView;

import java.io.IOException;

import de.greenrobot.event.EventBus;

public class LoginFragment extends Fragment implements View.OnClickListener {

    private View rootView;
    private CustomEditText usernameEditText, passwordEditText;
    private EventBus eventBus = EventBus.getDefault();
    private String userName;
    private String password;
    private ProgressDialog progress;
    private CustomTextView forgotPasswordTV;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_login, container, false);
        initScreen();
        return rootView;
    }

    private void initScreen() {
        progress = new ProgressDialog(getActivity());
        progress.setCancelable(false);
        progress.setMessage(getString(R.string.msg_loading));
        usernameEditText = (CustomEditText) rootView.findViewById(R.id.username_et);
        passwordEditText = (CustomEditText) rootView.findViewById(R.id.password_et);
        Utility.setTextWatcher(usernameEditText);
        Utility.setTextWatcher(passwordEditText);

        forgotPasswordTV = (CustomTextView) rootView.findViewById(R.id.forgotPasswordTV);
        forgotPasswordTV.setOnClickListener(this);

        passwordEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((actionId == EditorInfo.IME_ACTION_DONE)) {
                    validate();
                }
                return false;
            }
        });

        rootView.findViewById(R.id.login_button).setOnClickListener(this);
        rootView.findViewById(R.id.signup_button).setOnClickListener(this);
        rootView.findViewById(R.id.forgotPasswordTV).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.login_button:
                validate();
                break;

            case R.id.signup_button:
                ((OnboardingActivity) getActivity()).moveToScreenWithBackstack(new SignUpFragment());
                break;

            case R.id.forgotPasswordTV:
                // progress.show();
                // Utility.displayToast(getActivity(),"functionality under development.");
                displayPopUpForForgotPassword();
                break;

            default:
                break;

        }
    }


    private void validate() {
        userName = usernameEditText.getText().toString().trim().replace(" ", "");
        password = passwordEditText.getText().toString().trim().replace(" ", "");

        if (TextUtils.isEmpty(userName)) {
            usernameEditText.setError(getString(R.string.msg_login_invalid_email));
            return;
        }

        if (!Utility.isValidEmail(userName)) {
            usernameEditText.setError(getString(R.string.msg_login_invalid_email));
            return;
        }

        if (TextUtils.isEmpty(password)) {
            passwordEditText.setError(getString(R.string.msg_login_invalid_password));
            return;
        }
        if (Utility.isNetworkAvailable(getActivity())) {
            Utility.hideSoftkeyboard(getActivity());
            doLogin(userName, password);
        } else {
            Utility.displayToast(getActivity(), getResources().getString(R.string.internet_connectivity_message));
        }


    }

    private void doLogin(final String username, final String password) {
        progress.show();
        AccountManager accountManager = new AccountManager(getActivity());
        accountManager.loginTask(username, password);
    }

    @Override
    public void onStop() {
        if (eventBus != null) {
            eventBus.unregister(this);
        }
        super.onStop();
    }

    @Override
    public void onPause() {
        if (eventBus != null) {
            eventBus.unregister(this);
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        if (eventBus != null) {
            eventBus.register(this);
        }
        super.onResume();
    }

    private void showOTPDialog(final String mobileNo,final String citizenId) {

        final Dialog emailVerificationCodeDialog = new Dialog(getActivity());
        emailVerificationCodeDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);


        if (!emailVerificationCodeDialog.isShowing()) {

            emailVerificationCodeDialog.setContentView(R.layout.dialog_email_verification_code);
            // emailVerificationCodeDialog.setCancelable(false);
            emailVerificationCodeDialog.show();

            final CustomButton verifyBtn = (CustomButton) emailVerificationCodeDialog.findViewById(R.id.sms_otp_verify);
            final CustomButton resendBtn = (CustomButton) emailVerificationCodeDialog.findViewById(R.id.resend_otp_button);

            final CustomEditText verificationCode = (CustomEditText) emailVerificationCodeDialog
                    .findViewById(R.id.otp_verification_code_edit_text);
            Utility.setTextWatcher(verificationCode);


            verificationCode.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if ((actionId == EditorInfo.IME_ACTION_DONE)) {

                        verifyBtn.performClick();
                    }
                    return false;
                }
            });

            verifyBtn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    String errorStr = getActivity().getString(R.string.verification_code_error);

                    if (Utility.validateEditText(getActivity(), verificationCode, errorStr)) {
                        if (emailVerificationCodeDialog != null) {
                            if (Utility.isNetworkAvailable(getActivity())) {
                                progress.show();
                                AccountManager accountManager = new AccountManager(getActivity());
                                accountManager.verifyOTPTask(mobileNo, verificationCode.getText().toString(),citizenId);

                                emailVerificationCodeDialog.setCancelable(true);
                                emailVerificationCodeDialog.dismiss();
                            } else {
                                Utility.displayToast(getActivity(), getResources().getString(R.string.internet_connectivity_message));
                            }

                        }
                    }
                }

            });


            resendBtn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (emailVerificationCodeDialog != null) {
                        if (Utility.isNetworkAvailable(getActivity())) {
                            progress.show();
                            Utility.displayToast(getActivity(), getString(R.string.resend_otp));
                            AccountManager accountManager = new AccountManager(getActivity());
                            // code commmited as create new method for adding hardcore response while user is resending otp
                            //accountManager.resendOTPTask(mobileNo, PrefManager.getStringFromPref(getActivity(), citizenId));
                            accountManager.resendOTP(mobileNo, PrefManager.getStringFromPref(getActivity(), citizenId));
                        } else {
                            Utility.displayToast(getActivity(), getResources().getString(R.string.internet_connectivity_message));
                        }
                    }
                }
            });
        }

    }

    public void onEvent(OTPverifyEvent event) {
        progress.dismiss();
        if (event.otpverifyResponse != null) {
            if ((!TextUtils.isEmpty(event.otpverifyResponse.message)) && (event.otpverifyResponse.status.equalsIgnoreCase(getString(R.string.server_success_ok)))) {
                if (event.isforResend) {
                    Utility.displayToast(getActivity(), event.otpverifyResponse.message);
                } else {
                    Utility.displayToast(getActivity(), event.otpverifyResponse.message);
                    //((OnboardingActivity) getActivity()).removeCurrentScreen();
                }

            } else {
                Utility.displayToast(getActivity(), event.otpverifyResponse.message);
            }
        } else {
            Utility.displayToast(getActivity(), getString(R.string.msg_service_call_error));
        }
    }


    public void onEvent(ResendOTPverifyEvent event) {
        progress.dismiss();
        if (event.otpverifyResponse != null) {
            if ((!TextUtils.isEmpty(event.otpverifyResponse.message)) && (event.otpverifyResponse.status.equalsIgnoreCase(getString(R.string.server_success_ok)))) {
                if (event.isforResend) {
                    Utility.displayToast(getActivity(), event.otpverifyResponse.message);
                } else {
                    Utility.displayToast(getActivity(), event.otpverifyResponse.message);
                    //((OnboardingActivity) getActivity()).removeCurrentScreen();
                }

            } else {
                Utility.displayToast(getActivity(), event.otpverifyResponse.message);
            }
        } else {
            Utility.displayToast(getActivity(), getString(R.string.msg_service_call_error));
        }
    }



    public void onEvent(LoginEvent event) {
        progress.dismiss();
        if (event.loginResponse != null) {

            if (!TextUtils.isEmpty(event.loginResponse.message) && (event.loginResponse.status.equalsIgnoreCase(getString(R.string.server_success_ok))) && (event.loginResponse.CID.equalsIgnoreCase(getString(R.string.candidate_id)))) {
                PrefManager.setBooleanToPref(getActivity(), Constants.PreferencesManagerKey.IS_LOG_IN, true);
                PrefManager.setUserDetails(getActivity(), userName, password, event.loginResponse.CTID, event.loginResponse.phone);
                Utility.displayToast(getActivity(), getString(R.string.msg_login_success));

                if (TextUtils.isEmpty(PrefManager.getGCMRegId(getActivity()))) {
                    Log.d("GCM REg Id is: -->>",PrefManager.getGCMRegId(getActivity()));
                    doGcmRegistration();
                } else {
                    ((OnboardingActivity) getActivity()).moveToMainActivity();
                }

            }else if(event.loginResponse.status.equalsIgnoreCase("0") && (event.loginResponse.CID.equalsIgnoreCase(getString(R.string.candidate_id)))){
                showOTPDialog(event.loginResponse.getPhone(),event.loginResponse.CTID);
            }
            else {
                Utility.displayToast(getActivity(), getString(R.string.msg_service_call_error));
                //Utility.displayToast(getActivity(), event.loginResponse.message);
            }


        } else {
            Utility.displayToast(getActivity(), getString(R.string.msg_service_call_error));
        }

    }

    public void onEvent(ForgotPasswordEvent event) {
        progress.dismiss();
        if (event.forgotPasswordResponse != null) {

            if (!TextUtils.isEmpty(event.forgotPasswordResponse.message) && (event.forgotPasswordResponse.status.equalsIgnoreCase(getString(R.string.server_success_ok)))) {
                Utility.displayToast(getActivity(), getActivity().getString(R.string.forgot_password_success_label));
            } else {
                Utility.displayToast(getActivity(), event.forgotPasswordResponse.message);
            }
        } else {
            Utility.displayToast(getActivity(), getString(R.string.msg_service_call_error));
        }
    }

    public void onEvent(GCMRegistrationEvent event) {
        progress.dismiss();
        if (event.registrationResponse != null) {

            if (!TextUtils.isEmpty(event.registrationResponse.message) && (event.registrationResponse.status.equalsIgnoreCase(getString(R.string.server_success_ok)))) {
                ((OnboardingActivity) getActivity()).moveToMainActivity();
            } else {
                Utility.displayToast(getActivity(), event.registrationResponse.message);
            }
        } else {
            Utility.displayToast(getActivity(), getString(R.string.msg_service_call_error));
        }
    }


    private void doGcmRegistration() {
        //      if (TextUtils.isEmpty(PrefManager.getGCMRegId(getActivity()))) {
        if (Utility.isNetworkAvailable(getActivity())) {
            if (isGooglePlayServicesAvailable()) {
                new GcmRegTask().execute();
            }
        } else {
            Toast.makeText(getActivity(), "Please turn on your internet to receive notifications.", Toast.LENGTH_LONG).show();
        }
        // }
    }

    public static String getDeviceId(Context context) {

        final String macAddr, androidId;
        WifiManager wifiMan = (WifiManager) context
                .getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInf = wifiMan.getConnectionInfo();
        macAddr = wifiInf.getMacAddress();
        androidId = ""
                + android.provider.Settings.Secure.getString(
                context.getContentResolver(),
                android.provider.Settings.Secure.ANDROID_ID);
        String UUID = androidId.hashCode() + "" + macAddr.hashCode();
        // Maybe save this: deviceUuid.toString()); to the preferences.
        return UUID;
    }

    private class GcmRegTask extends AsyncTask<Void, String, String> {

        /**
         * The Gcm obj.
         */
        GoogleCloudMessaging gcmObj;
        /**
         * The Reg id.
         */
        String regId;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... params) {
            if (gcmObj == null) {
                gcmObj = GoogleCloudMessaging.getInstance(getActivity());
            }
            try {
                regId = gcmObj.register(Constants.GCM_PROJECT_ID);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return regId;
        }

        @Override
        protected void onPostExecute(String result) {
            if (!TextUtils.isEmpty(regId)) {
                Log.d("GCM_Key", "" + regId);
                PrefManager.setGCMRegId(getActivity(), regId);
                AccountManager accountManager = new AccountManager(getActivity());
                accountManager.sendGCMRegIdToServer(PrefManager.getUserPhone(getActivity()), regId, getDeviceId(getActivity()));
                //  Toast.makeText(getActivity(), "App registered for notifications", Toast.LENGTH_LONG).show();
            } else {
                //  Toast.makeText(getActivity(), "App will not receive push notification, please try again.", Toast.LENGTH_LONG).show();
                ((OnboardingActivity) getActivity()).moveToMainActivity();
            }
            super.onPostExecute(result);
        }
    }

    private Dialog dialog;

    private CustomEditText mobileEditText;
    private CustomButton submitButton;

    /**
     * Display exit dialog.
     */
    private void displayPopUpForForgotPassword() {
        dialog = new Dialog(getActivity());
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        // here we set layout of progress dialog
        dialog.setContentView(R.layout.fragment_dialog);
        dialog.setCancelable(true);
        mobileEditText = (CustomEditText) dialog.findViewById(R.id.mobile_no_editText);
        submitButton = (CustomButton) dialog.findViewById(R.id.submit_button);
        dialog.show();
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mobileNo = mobileEditText.getText().toString();
                if (!TextUtils.isEmpty(mobileNo) && mobileNo.length() == 10) {
                    AccountManager accountManager = new AccountManager(getActivity());
                    accountManager.forgotPassword(mobileNo, "citizen");
                    dialog.dismiss();
                } else {
                    Utility.displayToast(getActivity(), "Please enter valid mobile number");
                }
            }
        });


    }


    private boolean isGooglePlayServicesAvailable() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, getActivity(),
                        Constants.PLAY_SERVICES_RESOLUTION_REQUEST).show();
//                if (progress != null) {
//                    progress.dismiss();
//                }
            } else {
                Toast.makeText(getActivity(),
                        "This device doesn't support Play services, App will not receive notifications.",
                        Toast.LENGTH_LONG).show();
            }
            return false;
        }
        return true;
    }


}