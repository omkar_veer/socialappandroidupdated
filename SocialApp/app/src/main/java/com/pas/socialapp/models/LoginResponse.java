package com.pas.socialapp.models;

/**
 * Created by ganesh on 5/1/16.
 */
public class LoginResponse extends GenericResponse {


    public String message;

    public String status;

    public String service;

    public String CTID;

    public String CID;

    public String phone;


    public String getCID() {
        return CID;
    }

    public void setCID(String CID) {
        this.CID = CID;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getCTID() {
        return CTID;
    }

    public void setCTID(String CTID) {
        this.CTID = CTID;
    }

    @Override
    public String toString() {
        return "ClassPojo [message = " + message + ", status = " + status + ", service = " + service + ", CTID = " + CTID + "]";
    }

}
