package com.pas.socialapp.eventhandlers;

import com.pas.socialapp.models.LoginResponse;
import com.pas.socialapp.models.OTPVerifyResponse;

/**
 * Created by ganesh on 5/1/16.
 */
public class OTPverifyEvent {

    public OTPVerifyResponse otpverifyResponse;
    public boolean isforResend;

    public OTPverifyEvent(OTPVerifyResponse otpverifyResponse, boolean isforResend) {
        this.otpverifyResponse = otpverifyResponse;
        this.isforResend = isforResend;
    }

}
