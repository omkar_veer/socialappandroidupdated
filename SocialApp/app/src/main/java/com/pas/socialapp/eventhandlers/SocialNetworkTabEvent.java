package com.pas.socialapp.eventhandlers;

import com.pas.socialapp.models.SocialNetworkTabResponse;

public class SocialNetworkTabEvent {

    public SocialNetworkTabResponse socialNetworkTabResponse;

    public SocialNetworkTabEvent(SocialNetworkTabResponse socialNetworkTabResponse) {
        this.socialNetworkTabResponse = socialNetworkTabResponse;
    }

}
