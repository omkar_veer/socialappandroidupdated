package com.pas.socialapp.views.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pas.socialapp.Constants;
import com.pas.socialapp.R;
import com.pas.socialapp.eventhandlers.GenericErrorEvent;
import com.pas.socialapp.utils.Utility;
import com.pas.socialapp.views.adapters.HelpingHandMainCategoryListAdapter;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;

public class HelpingHandMainCategoryFragment extends Fragment implements HelpingHandMainCategoryListAdapter.ServiceClickListener {

    private View rootView;
    private EventBus eventBus = EventBus.getDefault();
    private ProgressDialog progress;

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private Context context;
    private ArrayList<String> hh_serviceDataList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.hh_main_category_fragment, container, false);
        Utility.floatingButtonVisibility(getActivity());
        initScreen();
        return rootView;
    }

    private void initScreen() {
        progress = new ProgressDialog(getActivity());
        progress.setCancelable(false);
        progress.setMessage(getString(R.string.msg_loading));


        context = getActivity();

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.hhMainRecyclerView);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(context);
        mRecyclerView.setLayoutManager(mLayoutManager);


        hh_serviceDataList = new ArrayList<>();
        hh_serviceDataList.add(Constants.HH_CATEGORY_1);
        hh_serviceDataList.add(Constants.HH_CATEGORY_2);
        hh_serviceDataList.add(Constants.HH_CATEGORY_3);
        hh_serviceDataList.add(Constants.HH_CATEGORY_4);
        hh_serviceDataList.add(Constants.HH_CATEGORY_5);

        mAdapter = new HelpingHandMainCategoryListAdapter(hh_serviceDataList, context);
        mRecyclerView.setAdapter(mAdapter);

        ((HelpingHandMainCategoryListAdapter) mAdapter).setOnServiceListener(HelpingHandMainCategoryFragment.this);

        // progress.show();
        //  AccountManager accountManager = new AccountManager(getActivity());
        // accountManager.getJahirnamaDetails(Constants.JAHIRNAMA_TAB);
    }

    @Override
    public void onStop() {
        if (eventBus != null) {
            eventBus.unregister(this);
        }
        super.onStop();
    }

    @Override
    public void onPause() {
        if (eventBus != null) {
            eventBus.unregister(this);
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        if (eventBus != null) {
            eventBus.register(this);
        }
        super.onResume();
    }

    public void onEvent(GenericErrorEvent event) {
        progress.dismiss();
    }

    @Override
    public void getServiceCategory(String serviceType) {
        moveToScreen(new HelpingHandSubCategoryListFragment(), serviceType);
    }

    public void moveToScreen(Fragment fragment, String serviceType) {
        FragmentTransaction fragmentManager = getFragmentManager().beginTransaction();
        Bundle args = new Bundle();
        args.putString("hh_serviceType", serviceType);
        fragment.setArguments(args);
        fragmentManager.addToBackStack(fragment.getClass().getName());
        fragmentManager.replace(R.id.MainContainer, fragment)
                .commit();
    }


}