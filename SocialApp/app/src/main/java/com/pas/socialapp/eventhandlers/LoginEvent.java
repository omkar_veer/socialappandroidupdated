package com.pas.socialapp.eventhandlers;

import com.pas.socialapp.models.LoginResponse;

/**
 * Created by ganesh on 5/1/16.
 */
public class LoginEvent {

    public LoginResponse loginResponse;

    public LoginEvent(LoginResponse loginResponse) {

        this.loginResponse = loginResponse;
    }

}
