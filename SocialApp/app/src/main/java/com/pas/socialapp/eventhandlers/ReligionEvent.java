package com.pas.socialapp.eventhandlers;

import com.pas.socialapp.models.ReligionResponse;

/**
 * Created by Umesh on 24/02/16.
 */
public class ReligionEvent {

    public ReligionResponse religionResponse;

    public ReligionEvent(ReligionResponse religionResponse) {
        this.religionResponse = religionResponse;
    }

}
