package com.pas.socialapp.views.adapters;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.UnderlineSpan;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.pas.socialapp.R;
import com.pas.socialapp.models.MatdarJagrutiResponse;
import com.pas.socialapp.models.SocialNetworkTabResponse;
import com.pas.socialapp.utils.Utility;
import com.pas.socialapp.views.custom.CustomTextView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class SocialNetworkDataListAdapter extends RecyclerView
        .Adapter<SocialNetworkDataListAdapter
        .DataObjectHolder> {
    private static final String LOG_TAG = SocialNetworkDataListAdapter.class.getSimpleName();
    private ArrayList<SocialNetworkTabResponse.SocialNetwork> mDataset;
    private Context context;

    private OpenURLListener openURLListener;

    public SocialNetworkDataListAdapter(ArrayList<SocialNetworkTabResponse.SocialNetwork> myDataset, Context cont) {
        mDataset = myDataset;
        this.context = cont;
    }


    @Override
    public SocialNetworkDataListAdapter.DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.social_network_data_list_items, parent, false);

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(SocialNetworkDataListAdapter.DataObjectHolder holder, final int position) {

        String data = "";

        if (!TextUtils.isEmpty(mDataset.get(position).getName())) {
            //holder.socialNetworkName.setText(mDataset.get(position).getName());
            data += context.getString(R.string.lable_social_network) +  " : \n"+ mDataset.get(position).getUrl() + "\n";
            SpannableString content = new SpannableString(mDataset.get(position).getName());
            content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
            holder.socialNetworkName.setText(content);
            Picasso.with(context).load(mDataset.get(position).getSocialimage()).error(R.drawable.app_icon).placeholder(R.drawable.app_icon).into(holder.socialImageUrl);
            holder.socialNetworkName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String browserURL= mDataset.get(position).getUrl();
                    if(!browserURL.contains("http")){
                        browserURL= "http://"+browserURL;
                    }
                    Uri uri = Uri.parse(browserURL); // missing 'http://' will cause crashed
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    context.startActivity(intent);
                }
            });
            holder.shareFab.setTag(data);
            //    holder.shareFab.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(context,R.color.colorPrimary)));
            holder.shareFab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String shareData = (String)view.getTag();
                    if (!TextUtils.isEmpty(shareData))
                        Utility.shareData(context, shareData);
                    else
                        Utility.displayToast(context, context.getString(R.string.msg_service_call_error));
                }
            });
        }

        /*if (!TextUtils.isEmpty(mDataset.get(position).getUrl())) {
            holder.socialNetworkURL.setText(mDataset.get(position).getUrl());
            Linkify.addLinks(holder.socialNetworkURL, Linkify.WEB_URLS);
        }*/
    }


    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class DataObjectHolder extends RecyclerView.ViewHolder {
        private CustomTextView socialNetworkName;
        private ImageView socialImageUrl;
        private CustomTextView socialNetworkURL;
        ImageView shareFab;



        public DataObjectHolder(View itemView) {
            super(itemView);
            socialImageUrl = (ImageView) itemView.findViewById(R.id.socialImageView);
            socialNetworkName = (CustomTextView) itemView.findViewById(R.id.socailNetworkName);
            socialNetworkURL = (CustomTextView) itemView.findViewById(R.id.socailNetworkUrl);
            shareFab = (ImageView) itemView.findViewById(R.id.socialNetworkShareFab);

        }
    }

    public interface OpenURLListener {
        public void openURLinWEbView(String url);
    }

    public void openURLServiceListener(OpenURLListener openURLListener) {
        this.openURLListener = openURLListener;
    }
}