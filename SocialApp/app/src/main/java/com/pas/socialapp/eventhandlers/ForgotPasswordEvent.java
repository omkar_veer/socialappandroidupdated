package com.pas.socialapp.eventhandlers;

import com.pas.socialapp.models.ForgotPasswordResponse;

public class ForgotPasswordEvent {

    public ForgotPasswordResponse forgotPasswordResponse;

    public ForgotPasswordEvent(ForgotPasswordResponse forgotPasswordResponse) {
        this.forgotPasswordResponse = forgotPasswordResponse;
    }

}
