package com.pas.socialapp.views.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.pas.socialapp.R;
import com.pas.socialapp.managers.DatabaseManager;
import com.pas.socialapp.models.Notification;
import com.pas.socialapp.models.NotificationDao;
import com.pas.socialapp.views.adapters.EOWAdapter;
import com.pas.socialapp.views.adapters.NotificationAdapter;
import de.greenrobot.event.EventBus;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ganesh.Bahirat on 5/14/2015.
 */
public class EOWFragment extends Fragment {

    private View rootView;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private EventBus eventBus = EventBus.getDefault();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_eventofweek, container, false);
        initScreen();
        return rootView;
    }

    private void initScreen() {

        setRecyclerView();

        List<Notification> notificationList = DatabaseManager.getInstance().getDaoSession()
                .getNotificationDao().getNotifications(NotificationDao.Properties.IsEnable, false);
        if (notificationList != null && !notificationList.isEmpty())
            ((EOWAdapter) mAdapter).updateData(notificationList);
        else
            Toast.makeText(getActivity(),  getActivity().getResources().getString(R.string.no_data_available), Toast.LENGTH_LONG).show();

       /* Notification notification = new Notification();
        notification.setDescription("Meeting for janhit Meeting for janhit Meeting for janhit Meeting for janhitMeeting for janhit");
        notification.setTitle("Emergency Meeting");
        notification.setUrl("www.google.com");
        notification.setType("15-08-2016");

        Notification notification1 = new Notification();
        notification1.setDescription("Meeting for janhit Meeting for janhit Meeting for janhit Meeting for janhitMeeting for janhit");
        notification1.setTitle("Emergency Meeting");
        notification1.setUrl("www.google.com");
        notification1.setType("15-08-2016");

           notificationList.add(notification);
        notificationList.add(notification1);*/

        ((EOWAdapter) mAdapter).updateData(notificationList);
    }


    private void setRecyclerView() {
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.eventofweek_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new EOWAdapter(getContext(), new ArrayList<Notification>());
        mRecyclerView.setAdapter(mAdapter);
        ((EOWAdapter) mAdapter).setOnItemClickListener(new EOWAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
            }
        });
    }

    public void onEventMainThread(String refresh) {
        initScreen();
    }


    @Override
    public void onStop() {
        if (eventBus != null) {
            eventBus.unregister(this);
        }
        super.onStop();
    }

    @Override
    public void onPause() {
        if (eventBus != null) {
            eventBus.unregister(this);
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        if (eventBus != null) {
            eventBus.register(this);
        }
        super.onResume();
    }

}