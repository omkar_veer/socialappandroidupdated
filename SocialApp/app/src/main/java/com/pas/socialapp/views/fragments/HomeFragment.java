package com.pas.socialapp.views.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.google.android.gms.iid.InstanceID;
import com.pas.socialapp.Constants;
import com.pas.socialapp.R;
import com.pas.socialapp.eventhandlers.GenericErrorEvent;
import com.pas.socialapp.managers.PrefManager;
import com.pas.socialapp.utils.Utility;
import com.pas.socialapp.views.activities.MainActivity;
import com.pas.socialapp.views.custom.CustomButton;

import java.util.Timer;
import java.util.TimerTask;

import de.greenrobot.event.EventBus;

public class HomeFragment extends Fragment implements View.OnClickListener {

    private View rootView;
    private EventBus eventBus = EventBus.getDefault();
    private ProgressDialog progress;
    private RelativeLayout relativeLayout;





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_home, container, false);
        ImageView floatingActionButton = (ImageView) ((MainActivity) getActivity()).findViewById(R.id.fab);
        floatingActionButton.setVisibility(View.GONE);
        initScreen();
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }



    @Override
    public void onStop() {
        if (eventBus != null) {
            eventBus.unregister(this);
        }
        //  sliderImageView.clearAnimation();
        super.onStop();
    }

    @Override
    public void onPause() {
        if (eventBus != null) {
            eventBus.unregister(this);
        }
        // sliderImageView.clearAnimation();
        super.onPause();
    }

    @Override
    public void onResume() {
        if (eventBus != null) {
            eventBus.register(this);
        }
        super.onResume();
    }

    @Override
    public void onDestroy() {
        // sliderImageView.clearAnimation();
        super.onDestroy();
    }

    public void onEvent(GenericErrorEvent event) {
        progress.dismiss();
    }


    private void initScreen() {
        if(PrefManager.getTabPref(getActivity(),Constants.PreferencesManagerKey.IS_TAB_VISIBLE)){
            rootView.findViewById(R.id.profileImageView).setOnClickListener(this);
            rootView.findViewById(R.id.partyProfileImageView).setOnClickListener(this);
            rootView.findViewById(R.id.workImageView).setOnClickListener(this);
            rootView.findViewById(R.id.jahirnamaImageView).setOnClickListener(this);
        }else{
            rootView.findViewById(R.id.topRL).setVisibility(View.GONE);
            rootView.findViewById(R.id.jahirnamaImageView).setOnClickListener(this);
        }
        rootView.findViewById(R.id.sevaImageView).setOnClickListener(this);
        rootView.findViewById(R.id.helpingHandImageView).setOnClickListener(this);
        rootView.findViewById(R.id.matdarImageView).setOnClickListener(this);
        rootView.findViewById(R.id.socialNetworkImageView).setOnClickListener(this);
        rootView.findViewById(R.id.messangerImageView).setOnClickListener(this);
        rootView.findViewById(R.id.eventImageView).setOnClickListener(this);
        rootView.findViewById(R.id.logoutImageView).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Utility.hideSoftkeyboard(getActivity());

        switch (view.getId()) {
            case R.id.profileImageView:
                moveToScreen(new ProfileFragment());
                break;

            case R.id.partyProfileImageView:
                moveToScreen(new PartyProfileFragment());
                break;

            case R.id.workImageView:
                moveToScreen(new WorkMainCategoryFragment());
                break;

            case R.id.jahirnamaImageView:
                if(PrefManager.getTabPref(getActivity(),Constants.PreferencesManagerKey.IS_TAB_VISIBLE)){
                    moveToScreen(new JahirnamaFragment());
                }else{
                    Utility.displayToast(getContext(), getString(R.string.disable_feature_msg));
                }
                break;

            case R.id.sevaImageView:
                moveToScreen(new ServicesMainCategoryFragment());
                break;

            case R.id.helpingHandImageView:
                moveToScreen(new HelpingHandMainCategoryFragment());
                break;

            case R.id.matdarImageView:
                moveToScreen(new MatdarJagrutiFragment());
                break;

            case R.id.socialNetworkImageView:
                moveToScreen(new SocialNetworkFragment());
                break;
            case R.id.messangerImageView:
                moveToScreen(new NotificationFragment());
                break;

            case R.id.eventImageView:
                moveToScreen(new EOWFragment());
                break;

            case R.id.logoutImageView:
                displayPopUpForLogOut();
                break;

            default:
                break;

        }
    }

    public void moveToScreen(Fragment fragment) {
        FragmentTransaction fragmentManager = getFragmentManager().beginTransaction();
        fragmentManager.addToBackStack(fragment.getClass().getName());
        fragmentManager.replace(R.id.MainContainer, fragment).commit();
    }

    private Dialog dialog;

    private CustomButton okButton, cancelButton;

    private void displayPopUpForLogOut() {
        dialog = new Dialog(getActivity());
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        // here we set layout of progress dialog
        dialog.setContentView(R.layout.fragment_log_out_dialog);
        dialog.setCancelable(false);
        okButton = (CustomButton) dialog.findViewById(R.id.ok_button);
        cancelButton = (CustomButton) dialog.findViewById(R.id.cancel_button);
        dialog.show();
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Thread background = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            InstanceID.getInstance(getActivity()).deleteInstanceID();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
                background.start();

                PrefManager.setBooleanToPref(getActivity(), Constants.PreferencesManagerKey.IS_LOG_IN, false);
                PrefManager.setTabPref(getActivity(), Constants.PreferencesManagerKey.IS_TAB_VISIBLE, false);
                getActivity().finish();
               /* FragmentManager fm = MainActivity.this.getSupportFragmentManager();
                for(int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                    fm.popBackStack();
                }
                moveToScreen(new LoginFragment());*/
                dialog.dismiss();
            }
        });
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }

}