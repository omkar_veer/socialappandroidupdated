package com.pas.socialapp.managers;

import android.content.Context;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.pas.socialapp.Constants;
import com.pas.socialapp.eventhandlers.NoNetworkEvent;
import com.pas.socialapp.utils.Utility;

import de.greenrobot.event.EventBus;

public final class VolleyManager {
    private static VolleyManager mInstance;
    private RequestQueue mRequestQueue;
    private Context context;

    private VolleyManager() {
    }

    public static synchronized VolleyManager getInstance() {
        if (mInstance == null) {
            mInstance = new VolleyManager();
        }
        return mInstance;
    }

    public void initVolleyRequestQueue(final Context context) {
        mRequestQueue = Volley.newRequestQueue(context);
        this.context = context;
    }


    public <T> void addToRequestQueue(Request<T> req, String tag) {
        if (Utility.isNetworkAvailable(context)) {
            // set the default tag if tag is empty
            req.setTag(TextUtils.isEmpty(tag) ? Constants.LOG_TAG : tag);
            mRequestQueue.add(req);
        } else {
            EventBus.getDefault().post(new NoNetworkEvent());
        }
    }


    public <T> void addToRequestQueue(Request<T> req) {
        if (Utility.isNetworkAvailable(context)) {
            req.setTag(Constants.LOG_TAG);
            mRequestQueue.add(req);
        } else {
            EventBus.getDefault().post(new NoNetworkEvent());
        }
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }
}
