package com.pas.socialapp.models;


import com.google.gson.annotations.SerializedName;

public class GenericRequest{

    @SerializedName("Key")
    public String key;

    @SerializedName("TimeStamp")
    public String date;

}
