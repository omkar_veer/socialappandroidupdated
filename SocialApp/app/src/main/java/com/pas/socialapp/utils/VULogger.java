package com.pas.socialapp.utils;

import android.util.Log;
import static com.pas.socialapp.Constants.ISLOGGABLE;
import static com.pas.socialapp.Constants.LOG_TAG;

public class VULogger {

	private static final int MINUSONE = -1;

	public static int i(String msg) {
		int retVal = MINUSONE;

		if (ISLOGGABLE) {
			retVal = Log.i(LOG_TAG, msg);
		}
		return retVal;
	}

	public static int e(String msg) {
		int retVal = MINUSONE;

		if (ISLOGGABLE) {
			retVal = Log.e(LOG_TAG, msg);
		}
		return retVal;
	}

	public static int d(String msg) {
		int retVal = MINUSONE;

		if (ISLOGGABLE) {
			retVal = Log.d(LOG_TAG, msg);
		}
		return retVal;
	}

	// two args
	public static int i(String tag, String msg) {
		int retVal = MINUSONE;

		if (ISLOGGABLE) {
			retVal = Log.i(tag, msg);
		}
		return retVal;
	}

	public static int e(String tag, String msg) {
		int retVal = MINUSONE;

		if (ISLOGGABLE) {
			retVal = Log.e(tag, msg);
		}
		return retVal;
	}

	public static int d(String tag, String msg) {
		int retVal = MINUSONE;

		if (ISLOGGABLE) {
			retVal = Log.d(tag, msg);
		}
		return retVal;
	}

	public static int e(Exception e) {
		int retVal = MINUSONE;

		retVal = e(Log.getStackTraceString(e));
		return retVal;
	}
}
