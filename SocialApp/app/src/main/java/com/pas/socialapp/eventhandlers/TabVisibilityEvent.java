package com.pas.socialapp.eventhandlers;

import com.pas.socialapp.models.TabVisibilityResponse;

public class TabVisibilityEvent {

    public TabVisibilityResponse visibilityResponse;

    public TabVisibilityEvent(TabVisibilityResponse visibilityResponse) {
        this.visibilityResponse = visibilityResponse;
    }

}
