package com.pas.socialapp.models;

import java.util.ArrayList;

/**
 * Created by Umesh  on 24/02/16.
 */
public class TabVisibilityResponse extends GenericRequest {

    public String status;
    public String service;
    public String message;
    public ArrayList<TabPreferences> tabpreferences = new ArrayList<TabPreferences>();

    /**
     * @return The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return The service
     */
    public String getService() {
        return service;
    }

    /**
     * @param service The service
     */
    public void setService(String service) {
        this.service = service;
    }

    /**
     * @return The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<TabPreferences> getTabpreferences() {
        return tabpreferences;
    }

    public void setTabpreferences(ArrayList<TabPreferences> tabpreferences) {
        this.tabpreferences = tabpreferences;
    }

    public class TabPreferences {

        private String CID;

        private String profilestatus;

        private String partystatus;

        private String jahirnamastatus;

        private String workstatus;

        public String getCID() {
            return CID;
        }

        public void setCID(String CID) {
            this.CID = CID;
        }

        public String getProfilestatus() {
            return profilestatus;
        }

        public void setProfilestatus(String profilestatus) {
            this.profilestatus = profilestatus;
        }

        public String getPartystatus() {
            return partystatus;
        }

        public void setPartystatus(String partystatus) {
            this.partystatus = partystatus;
        }

        public String getJahirnamastatus() {
            return jahirnamastatus;
        }

        public void setJahirnamastatus(String jahirnamastatus) {
            this.jahirnamastatus = jahirnamastatus;
        }

        public String getWorkstatus() {
            return workstatus;
        }

        public void setWorkstatus(String workstatus) {
            this.workstatus = workstatus;
        }
    }
}
