package com.pas.socialapp.views.adapters;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pas.socialapp.R;
import com.pas.socialapp.views.custom.CustomTextView;

import java.util.ArrayList;

public class WorkMainCategoryListAdapter extends RecyclerView
        .Adapter<WorkMainCategoryListAdapter
        .DataObjectHolder> {
    private static final String LOG_TAG = WorkMainCategoryListAdapter.class.getSimpleName();
    private ArrayList<String> mDataset;
    private Context context;
    private WorkClickListener serviceClickListener;

    public WorkMainCategoryListAdapter(ArrayList<String> myDataset, Context cont) {
        mDataset = myDataset;
        this.context = cont;
    }

    @Override
    public WorkMainCategoryListAdapter.DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.work_main_category_list_item, parent, false);

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(WorkMainCategoryListAdapter.DataObjectHolder holder, final int position) {
        mDataset.get(position);
        if (mDataset.get(position) != null) {
            if(position==0){
                holder.requestTitle.setText(context.getString(R.string.social_work_text));
            }else    if(position==1){
                holder.requestTitle.setText(context.getString(R.string.development_work_text));
            } else   if(position==2){
                holder.requestTitle.setText(context.getString(R.string.religious_work_text));
            } else   if(position==3){
                holder.requestTitle.setText(context.getString(R.string.sport_work_text));
            }
            //holder.requestTitle.setText(mDataset.get(position));
            holder.requestTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showWorkByCategory(mDataset.get(position));
                }

                private void showWorkByCategory(String serviceType) {
                    serviceClickListener.getWorkCategory(serviceType);
                }
            });

        }
    }

    public interface WorkClickListener {
        public void getWorkCategory(String serviceType);
    }

    public void setOnWorkListener(WorkClickListener serviceClickListener) {
        this.serviceClickListener = serviceClickListener;
    }


    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class DataObjectHolder extends RecyclerView.ViewHolder {
        CustomTextView requestTitle;

        public DataObjectHolder(View itemView) {
            super(itemView);
            requestTitle = (CustomTextView) itemView.findViewById(R.id.requestTitle);

        }
    }
}