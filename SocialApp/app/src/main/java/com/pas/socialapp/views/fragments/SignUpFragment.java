package com.pas.socialapp.views.fragments;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.pas.socialapp.Constants;
import com.pas.socialapp.R;
import com.pas.socialapp.eventhandlers.CasteEvent;
import com.pas.socialapp.eventhandlers.DistrictEvent;
import com.pas.socialapp.eventhandlers.GenericErrorEvent;
import com.pas.socialapp.eventhandlers.OTPverifyEvent;
import com.pas.socialapp.eventhandlers.PinCodeEvent;
import com.pas.socialapp.eventhandlers.RegEvent;
import com.pas.socialapp.eventhandlers.ReligionEvent;
import com.pas.socialapp.eventhandlers.ResendOTPverifyEvent;
import com.pas.socialapp.eventhandlers.StatesEvent;
import com.pas.socialapp.eventhandlers.TalukaEvent;
import com.pas.socialapp.managers.AccountManager;
import com.pas.socialapp.managers.PrefManager;
import com.pas.socialapp.models.CasteResponse;
import com.pas.socialapp.models.DistrictResponse;
import com.pas.socialapp.models.PinCodeResponse;
import com.pas.socialapp.models.ReligionResponse;
import com.pas.socialapp.models.StateResponse;
import com.pas.socialapp.models.TalukaResponse;
import com.pas.socialapp.utils.Utility;
import com.pas.socialapp.views.activities.MainActivity;
import com.pas.socialapp.views.activities.OnboardingActivity;
import com.pas.socialapp.views.custom.CustomButton;
import com.pas.socialapp.views.custom.CustomEditText;
import com.pas.socialapp.views.custom.CustomRadioButton;
import com.pas.socialapp.views.custom.CustomTextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import de.greenrobot.event.EventBus;

public class SignUpFragment extends Fragment implements View.OnClickListener, DatePickerDialog.OnDateSetListener, AdapterView.OnItemSelectedListener, Utility.ExitListener {

    private View rootView;
    private CustomEditText firstnameEditText, middlenameEditText, lastnameEditText, emailEditText, passwordEditText, repasswordEditText, phoneEditText,
            landlineEditText, otheresEducationEditText, flatnoCustomEditText, houseCustomEditText, wingCustomEditText, societyCustomEditText,
            surveynoCustomEditText, lane1CustomEditText, lane2CustomEditText, areaCustomEditText, mothernameEditText, alternatePhoneEditText, secondaryPhoneEditText;
    private CustomTextView dateOfBirth, anniversaryCustomTextView;
    private EventBus eventBus = EventBus.getDefault();
    private ProgressDialog progress;
    private boolean isDateDialogShown = false;
    private boolean isAnniversaryDialogShown = false;
    private RadioGroup genderRadioGroup;
    private CustomRadioButton genderRadioButton;
    private Context context;
    private String bloodgroup, profession,education, mobileNo, state, district, taluka, pincode, casteStr, religionStr;
    Spinner spinner_bloodgrp, spinner_education, spinner_state, spinner_district, spinner_taluka, spinner_pincode, spinner_caste, spinner_religion,spinner_profession;
    ArrayList<String> state_values;
    ArrayList<String> dist_values;
    ArrayList<String> taluka_values;
    ArrayList<String> pincode_values;
    private CheckBox isChairmanCheckBox, isVoterCheckBox;
    private String isChairman = "0";
    private String isVoter = "0";
    ArrayList<String> caste_values;
    ArrayList<String> religion_values;
    ArrayAdapter stateAdapter, districtAdapter, talukaAdapter, pincodeAdapter, casteAdapter, religionAdapter;
    boolean isDOBSelected, isAnniversarySelected = false;
    boolean isOthersSelected = false;
    private LinearLayout stepOneLL,stepTwoLL;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        context = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_signup, container, false);
        initScreen();
        return rootView;
    }

    private void initScreen() {
        progress = new ProgressDialog(getActivity());
        progress.setCancelable(false);
        progress.setMessage(getString(R.string.msg_loading));
        firstnameEditText = (CustomEditText) rootView.findViewById(R.id.reg_username_first_et);
        middlenameEditText = (CustomEditText) rootView.findViewById(R.id.reg_username_middle_et);
        lastnameEditText = (CustomEditText) rootView.findViewById(R.id.reg_username_last_et);
        emailEditText = (CustomEditText) rootView.findViewById(R.id.reg_email_et);
        passwordEditText = (CustomEditText) rootView.findViewById(R.id.reg_password_et);
        repasswordEditText = (CustomEditText) rootView.findViewById(R.id.reg_repassword_et);

        phoneEditText = (CustomEditText) rootView.findViewById(R.id.reg_phone_et);
        alternatePhoneEditText = (CustomEditText) rootView.findViewById(R.id.reg_phone_alternate_et);
        secondaryPhoneEditText = (CustomEditText) rootView.findViewById(R.id.reg_phone_secondary_et);


        landlineEditText = (CustomEditText) rootView.findViewById(R.id.reg_landline_et);
        otheresEducationEditText = (CustomEditText) rootView.findViewById(R.id.reg_education_others_et);

        flatnoCustomEditText = (CustomEditText) rootView.findViewById(R.id.reg_flat_no_et);
        houseCustomEditText = (CustomEditText) rootView.findViewById(R.id.reg_house_name_et);
        wingCustomEditText = (CustomEditText) rootView.findViewById(R.id.reg_wing_et);
        societyCustomEditText = (CustomEditText) rootView.findViewById(R.id.reg_society_et);
        surveynoCustomEditText = (CustomEditText) rootView.findViewById(R.id.reg_survey_no_et);
        lane1CustomEditText = (CustomEditText) rootView.findViewById(R.id.reg_lane1_et);
        lane2CustomEditText = (CustomEditText) rootView.findViewById(R.id.reg_lane2_et);
        areaCustomEditText = (CustomEditText) rootView.findViewById(R.id.reg_area_et);
        mothernameEditText = (CustomEditText) rootView.findViewById(R.id.reg_user_mothername_et);

        dateOfBirth = (CustomTextView) rootView.findViewById(R.id.registration_text_view_date_of_birth);
        dateOfBirth.setOnClickListener(this);

        anniversaryCustomTextView = (CustomTextView) rootView.findViewById(R.id.registration_text_view_anniversary);
        anniversaryCustomTextView.setOnClickListener(this);

        genderRadioGroup = (RadioGroup) rootView.findViewById(R.id.registration_radio_group_gender);

        spinner_bloodgrp = (Spinner) rootView.findViewById(R.id.reg_bloodgrp_spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(context,
                R.array.bloodgrp_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_bloodgrp.setPrompt("Blood Group");
        spinner_bloodgrp.setAdapter(adapter);
        spinner_bloodgrp.setOnItemSelectedListener(this);

        spinner_profession = (Spinner) rootView.findViewById(R.id.reg_profession_et);


        spinner_caste = (Spinner) rootView.findViewById(R.id.reg_caste_spinner);
        spinner_religion = (Spinner) rootView.findViewById(R.id.reg_religion_spinner);


        spinner_education = (Spinner) rootView.findViewById(R.id.reg_education_spinner);
        ArrayAdapter<CharSequence> adapter_education = ArrayAdapter.createFromResource(context,
                R.array.education_array, android.R.layout.simple_spinner_item);
        adapter_education.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_education.setPrompt("Education");
        spinner_education.setAdapter(adapter_education);
        spinner_education.setOnItemSelectedListener(this);

        ArrayAdapter<CharSequence> adapter_profession = ArrayAdapter.createFromResource(context,
                R.array.profession_array, android.R.layout.simple_spinner_item);
        adapter_profession.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_profession.setPrompt("Profession");
        spinner_profession.setAdapter(adapter_profession);
        spinner_profession.setOnItemSelectedListener(this);

        isChairmanCheckBox = (CheckBox) rootView.findViewById(R.id.isChairmanCheckbox);
        isChairmanCheckBox.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (isChairmanCheckBox.isChecked()) {
                    isChairman = "1";
                } else {
                    isChairman = "0";
                }
            }
        });


        isVoterCheckBox = (CheckBox) rootView.findViewById(R.id.isVoter);

        isVoterCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isVoterCheckBox.isChecked()) {
                    isVoter = "1";
                } else {
                    isVoter = "0";
                }
            }
        });

        spinner_state = (Spinner) rootView.findViewById(R.id.spinner_state);
        spinner_district = (Spinner) rootView.findViewById(R.id.spinner_district);
        spinner_taluka = (Spinner) rootView.findViewById(R.id.spinner_taluka);
        spinner_pincode = (Spinner) rootView.findViewById(R.id.spinner_pincode);

        // TODO : Check network connection here

        if (Utility.isNetworkAvailable(getActivity())) {
            getAllStatesFromService();

        } else {
            Utility.displayToast(getActivity(), getResources().getString(R.string.internet_connectivity_message));
            Utility.displayInternetDialog(getActivity(), this);
        }



        stepOneLL = (LinearLayout) rootView.findViewById(R.id.stepOneLL);
        stepTwoLL = (LinearLayout) rootView.findViewById(R.id.stepTwoLL);

        rootView.findViewById(R.id.reg_signup_button).setOnClickListener(this);
        rootView.findViewById(R.id.reg_signin_button).setOnClickListener(this);
        rootView.findViewById(R.id.stepOneButton).setOnClickListener(this);
        rootView.findViewById(R.id.stepTwoButton).setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        Utility.hideSoftkeyboard(getActivity());

        switch (view.getId()) {
            case R.id.reg_signup_button:
                if(stepOneLL.getVisibility()!=View.VISIBLE)
                stepOneLL.setVisibility(View.VISIBLE);
                if(stepTwoLL.getVisibility()!=View.VISIBLE)
                    stepTwoLL.setVisibility(View.VISIBLE);
                validate();
                break;

            case R.id.reg_signin_button:
                ((OnboardingActivity) getActivity()).removeCurrentScreen();
                break;

            case R.id.registration_text_view_date_of_birth:

                if (!isDateDialogShown) {
                    isDateDialogShown = true;
                    showDatePickerDialog();
                }
                break;

            case R.id.registration_text_view_anniversary:

                if (!isAnniversaryDialogShown) {
                    isAnniversaryDialogShown = true;
                    showAnniversaryDatePickerDialog();
                }
                break;

            case R.id.stepOneButton:
                if (stepOneLL.getVisibility() == View.VISIBLE) {
                    stepOneLL.setVisibility(View.GONE);
                } else {
                    stepOneLL.setVisibility(View.VISIBLE);
                }
                break;

            case R.id.stepTwoButton:
                if (stepTwoLL.getVisibility() == View.VISIBLE) {
                    stepTwoLL.setVisibility(View.GONE);
                } else {
                    stepTwoLL.setVisibility(View.VISIBLE);
                }
                break;

            default:
                break;

        }
    }

    private void showAnniversaryDatePickerDialog() {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        // Create a new instance of DatePickerDialog and return it
        DatePickerDialog dialog = new DatePickerDialog(context,
                SignUpFragment.this, year, month, day);
        dialog.getDatePicker().setMaxDate(new Date().getTime());
        dialog.show();
        isAnniversarySelected = true;
        isAnniversaryDialogShown = false;
    }


    private void showDatePickerDialog() {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        // Create a new instance of DatePickerDialog and return it
        DatePickerDialog dialog = new DatePickerDialog(context,
                SignUpFragment.this, year, month, day);
        dialog.getDatePicker().setMaxDate(new Date().getTime());
        dialog.show();
        isDOBSelected = true;
        isDateDialogShown = false;
    }


    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {


        if (isDOBSelected) {
            dateOfBirth.setText(
                    new StringBuilder().append(year).append("-")
                            .append(monthOfYear + 1).append("-")
                            .append(dayOfMonth).append(" ")

                           /* .append(dayOfMonth).append("-")
                            // Month is 0 based so add 1
                            .append(monthOfYear + 1).append("-")
                            .append(year).append(" ")*/

            );
            dateOfBirth.setTextColor(getResources().getColor(android.R.color.black));
            isDOBSelected = false;
        }

        if (isAnniversarySelected) {

            anniversaryCustomTextView.setText(
                    new StringBuilder()
                            .append(year).append("-")
                            .append(monthOfYear + 1).append("-")
                            .append(dayOfMonth).append(" "));
            anniversaryCustomTextView.setTextColor(getResources().getColor(android.R.color.black));

            isAnniversarySelected = false;
        }


    }

    private String getGender() {

        if (genderRadioGroup.getCheckedRadioButtonId() != -1) {

            // get selected radio button from radioGroup
            int selectedId = genderRadioGroup.getCheckedRadioButtonId();

            // find the radio button by returned id
            genderRadioButton = (CustomRadioButton) rootView.findViewById(selectedId);

            if (genderRadioButton.getText().toString().equals(getString(R.string.registration_radio_male))) {
                return getString(R.string.registration_radio_selected_male);
            } else {
                return getString(R.string.registration_radio_selected_female);
            }
        } else {
            return getString(R.string.registration_radio_selected_nothing);
        }
    }

    private void validate() {
        String userName = firstnameEditText.getText().toString().trim().replace(" ", "");
        String middleName = middlenameEditText.getText().toString().trim().replace(" ", "");
        String lastName = lastnameEditText.getText().toString().trim().replace(" ", "");

        String email = emailEditText.getText().toString().trim().replace(" ", "");
        String password = passwordEditText.getText().toString().trim().replace(" ", "");
        String repassword = repasswordEditText.getText().toString().trim().replace(" ", "");

        mobileNo = phoneEditText.getText().toString().trim().replace(" ", "");
        String landline = landlineEditText.getText().toString().trim().replace(" ", "");


        String alternatePhone = alternatePhoneEditText.getText().toString().trim().replace(" ", "");
        String secondaryPhone = secondaryPhoneEditText.getText().toString().trim().replace(" ", "");


        String dob = dateOfBirth.getText().toString().trim();
        dob = dob.equals(getString(R.string.registration_text_date_of_birth)) ? "" : dob;


        String anniversaryDate = anniversaryCustomTextView.getText().toString().trim();
        anniversaryDate = anniversaryDate.equals(getString(R.string.registration_text_anniversary)) ? "" : anniversaryDate;


        String gender = getGender().trim();
        String mothername = mothernameEditText.getText().toString().trim();

        String flatno = flatnoCustomEditText.getText().toString().trim().replace(" ", "");
        String house = houseCustomEditText.getText().toString().trim().replace(" ", "");
        String wing = wingCustomEditText.getText().toString().trim().replace(" ", "");
        String society = societyCustomEditText.getText().toString().trim().replace(" ", "");
        String survey = surveynoCustomEditText.getText().toString().trim().replace(" ", "");
        String lane1 = lane1CustomEditText.getText().toString().trim().replace(" ", "");
        String lane2 = lane2CustomEditText.getText().toString().trim().replace(" ", "");
        String area = areaCustomEditText.getText().toString().trim().replace(" ", "");

        if (TextUtils.isEmpty(userName)) {
            firstnameEditText.requestFocus();
            firstnameEditText.setError(getString(R.string.msg_login_invalid_username));
            return;
        }
        if (TextUtils.isEmpty(middleName)) {
            middlenameEditText.requestFocus();
            middlenameEditText.setError(getString(R.string.msg_login_invalid_username_middle));
            return;
        }
        if (TextUtils.isEmpty(lastName)) {
            lastnameEditText.requestFocus();
            lastnameEditText.setError(getString(R.string.msg_login_invalid_username_last));
            return;
        }

        if (TextUtils.isEmpty(mothername)) {
            mothernameEditText.requestFocus();
            mothernameEditText.setError(getString(R.string.msg_login_invalid_mothersname));
            return;
        }


        if (!Utility.isValidEmail(email)) {
            emailEditText.requestFocus();
            emailEditText.setError(getString(R.string.msg_login_invalid_email));
            return;
        }

        if (!Utility.isValidPassword(password)) {
            passwordEditText.requestFocus();
            passwordEditText.setError(getString(R.string.msg_login_invalid_password));
            return;
        }

        if (TextUtils.isEmpty(repassword)) {
            repasswordEditText.requestFocus();
            repasswordEditText.setError(getString(R.string.msg_login_invalid_password));
            return;
        }

        if (!repassword.equals(password)) {
            repasswordEditText.requestFocus();
            repasswordEditText.setError(getString(R.string.msg_login_password_not_matched));
            return;
        }


        if (TextUtils.isEmpty(mobileNo)) {
            phoneEditText.requestFocus();
            phoneEditText.setError(getString(R.string.msg_reg_invalid_phone));
            return;
        }

        if (!isValidMobile(mobileNo)) {
            phoneEditText.requestFocus();
            phoneEditText.setError(getString(R.string.msg_reg_invalid_phone));
            return;
        }


        if (TextUtils.isEmpty(dob)) {
            Utility.displayToast(context, getString(R.string.msg_reg_invalid_DOB));
            return;
        }
        /*if (TextUtils.isEmpty(anniversaryDate)) {
            Utility.displayToast(context, getString(R.string.msg_reg_invalid_Anniversary_date));
            return;
        }*/


        if (isOthersSelected) {
            education = otheresEducationEditText.getText().toString().trim().replace(" ", "");
            if (TextUtils.isEmpty(education)) {
                otheresEducationEditText.requestFocus();
                otheresEducationEditText.setError(getString(R.string.msg_reg_specify_education));
                return;
            }
        }

      /*  if (TextUtils.isEmpty(casteStr)) {
            Utility.displayToast(context, getString(R.string.caste_reg_invalid_state));
            return;
        }*/
        if (TextUtils.isEmpty(religionStr)) {
            Utility.displayToast(context, getString(R.string.reg_invalid_religion));
            return;
        }
        if (TextUtils.isEmpty(lane1)) {
            lane1CustomEditText.requestFocus();
            lane1CustomEditText.setError(getString(R.string.msg_reg_invalid_address1));
            return;
        }

        if (TextUtils.isEmpty(area)){
            areaCustomEditText.requestFocus();
            areaCustomEditText.setError(getString(R.string.msg_reg_invalid_area));
            return;
        }

        if (TextUtils.isEmpty(state)) {
            Utility.displayToast(context, getString(R.string.msg_reg_invalid_state));
            return;
        }

        if (TextUtils.isEmpty(district)) {
            Utility.displayToast(context, getString(R.string.msg_reg_invalid_district));
            return;
        }

        if (TextUtils.isEmpty(taluka)) {
            Utility.displayToast(context, getString(R.string.msg_reg_invalid_taluka));
            return;
        }

        if (TextUtils.isEmpty(pincode)) {
            Utility.displayToast(context, getString(R.string.msg_reg_invalid_pincode));
            return;
        }

        if (Utility.isNetworkAvailable(getActivity())) {
            doRegistration(userName, middleName, lastName, email, password, mobileNo, landline, dob, gender, bloodgroup, education, profession, isChairman,
                    flatno, house, wing, society, survey, lane1, lane2, area, state, district, taluka, pincode, casteStr, mothername, isVoter, anniversaryDate, alternatePhone, secondaryPhone, religionStr);
        } else {
            Utility.displayToast(getActivity(), getResources().getString(R.string.internet_connectivity_message));
        }
    }

    private void doRegistration(final String username, final String middlename, final String lastname, final String email, final String password, final String phone, final String landline, final String dob, final String gender, final String bloodgroup, final String education, final String profession, final String isChairman,
                                String flatno, String house, String wing, String society, String survey, String lane1, String lane2, String area, String state, String district, String taluka, String pincode, String casteStr, String mothername, String isVoter, String anniversaryDate, String alternatePhone, String secondaryPhone, String religionStr) {
        progress.show();
        AccountManager accountManager = new AccountManager(getActivity());
        accountManager.userRegistrationTask(username, middlename, lastname, email, password, phone, landline, dob, gender, bloodgroup, education, profession, isChairman, flatno, house, wing, society, survey, lane1, lane2, area, state, district, taluka, pincode, casteStr, mothername, isVoter, anniversaryDate, alternatePhone, secondaryPhone, religionStr);

    }

    public boolean isValidMobile(String phone) {
        boolean check;
        if (phone.length() == 10) {
            check = true;
        } else {
            check = false;
        }
        return check;
    }


    @Override
    public void onStop() {
        if (eventBus != null) {
            eventBus.unregister(this);
        }
        super.onStop();
    }


    @Override
    public void onPause() {
        if (eventBus != null) {
            eventBus.unregister(this);
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        if (eventBus != null) {
            eventBus.register(this);
        }
        super.onResume();
    }

    public void onEvent(RegEvent event) {
        progress.dismiss();
        if (event.registrationResponse != null) {
            if ((!TextUtils.isEmpty(event.registrationResponse.message)) && (event.registrationResponse.status.equalsIgnoreCase(getString(R.string.server_success_ok)))) {
                Utility.displayToast(getActivity(), event.registrationResponse.message);
                PrefManager.setStringToPref(getActivity(), Constants.PreferencesManagerKey.CTID, event.registrationResponse.CTID);
                showOTPDialog();   // show OTP dialog
                //  ((OnboardingActivity) getActivity()).removeCurrentScreen();
            } else {
                if (event.registrationResponse.otp != null) {
                    if (event.registrationResponse.otp.equalsIgnoreCase("pending")) {
                        Utility.displayToast(getActivity(), event.registrationResponse.message);
                        showOTPDialog();
                    } else {
                        Utility.displayToast(getActivity(), event.registrationResponse.message);
                    }
                } else {
                    Utility.displayToast(getActivity(), event.registrationResponse.message);
                }
            }
        } else {
            Utility.displayToast(getActivity(), getString(R.string.msg_service_call_error));
        }
    }

    public void onEvent(GenericErrorEvent event) {
        progress.dismiss();
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        switch (parent.getId()) {
            case R.id.reg_bloodgrp_spinner:
                bloodgroup = parent.getItemAtPosition(position).toString();
                break;

            case R.id.reg_profession_et:
                profession = parent.getItemAtPosition(position).toString();
                break;

            case R.id.reg_education_spinner:
                education = parent.getItemAtPosition(position).toString();
                String[] eduArray;
                eduArray = getResources().getStringArray(R.array.education_array);
                String others = eduArray[eduArray.length - 1];
                if (education.equalsIgnoreCase(others)) {
                    isOthersSelected = true;
                    otheresEducationEditText.setVisibility(View.VISIBLE);
                } else {
                    isOthersSelected = false;
                    otheresEducationEditText.setVisibility(View.GONE);
                }

                break;

            case R.id.reg_caste_spinner:
                casteStr = parent.getItemAtPosition(position).toString();
                break;
            case R.id.reg_religion_spinner:
                religionStr = parent.getItemAtPosition(position).toString();
                break;
            case R.id.spinner_state:
                state = parent.getItemAtPosition(position).toString();
                dist_values = null;
                taluka_values = null;
                pincode_values = null;

                spinner_district.setVisibility(View.VISIBLE);
                spinner_taluka.setVisibility(View.GONE);
                spinner_pincode.setVisibility(View.GONE);
                if (Utility.isNetworkAvailable(getActivity())) {
                    getAllDistrictsFromService(state);
                } else {
                    Utility.displayToast(getActivity(), getResources().getString(R.string.internet_connectivity_message));
                }

                break;
            case R.id.spinner_district:
                district = parent.getItemAtPosition(position).toString();

                taluka_values = null;
                pincode_values = null;
                spinner_taluka.setVisibility(View.VISIBLE);
                spinner_pincode.setVisibility(View.GONE);
                if (Utility.isNetworkAvailable(getActivity())) {
                    getAllTalukaFromService(district);
                } else {
                    Utility.displayToast(getActivity(), getResources().getString(R.string.internet_connectivity_message));
                }

                break;
            case R.id.spinner_taluka:
                taluka = parent.getItemAtPosition(position).toString();
                pincode_values = null;
                spinner_pincode.setVisibility(View.VISIBLE);
                if (Utility.isNetworkAvailable(getActivity())) {
                    getAllPincodesFromService(taluka);
                } else {
                    Utility.displayToast(getActivity(), getResources().getString(R.string.internet_connectivity_message));
                }

                break;
            case R.id.spinner_pincode:
                pincode = parent.getItemAtPosition(position).toString();
                break;
            default:
                break;

        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    private void showOTPDialog() {

        final Dialog emailVerificationCodeDialog = new Dialog(context);
        emailVerificationCodeDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);


        if (!emailVerificationCodeDialog.isShowing()) {

            emailVerificationCodeDialog.setContentView(R.layout.dialog_email_verification_code);
            // emailVerificationCodeDialog.setCancelable(false);
            emailVerificationCodeDialog.show();

            final CustomButton verifyBtn = (CustomButton) emailVerificationCodeDialog.findViewById(R.id.sms_otp_verify);
            final CustomButton resendBtn = (CustomButton) emailVerificationCodeDialog.findViewById(R.id.resend_otp_button);

            final CustomEditText verificationCode = (CustomEditText) emailVerificationCodeDialog
                    .findViewById(R.id.otp_verification_code_edit_text);
            Utility.setTextWatcher(verificationCode);


            verificationCode.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if ((actionId == EditorInfo.IME_ACTION_DONE)) {

                        verifyBtn.performClick();
                    }
                    return false;
                }
            });

            verifyBtn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    String errorStr = context.getString(R.string.verification_code_error);

                    if (Utility.validateEditText(context, verificationCode, errorStr)) {
                        if (emailVerificationCodeDialog != null) {
                            if (Utility.isNetworkAvailable(getActivity())) {
                                progress.show();
                                AccountManager accountManager = new AccountManager(getActivity());
                                accountManager.verifyOTPTask(mobileNo, verificationCode.getText().toString(), PrefManager.getStringFromPref(getActivity(), Constants.PreferencesManagerKey.CTID));

                                emailVerificationCodeDialog.setCancelable(true);
                                emailVerificationCodeDialog.dismiss();
                            } else {
                                Utility.displayToast(getActivity(), "Please turn on your internet connection.");
                            }

                        }
                    }
                }

            });


            resendBtn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (emailVerificationCodeDialog != null) {
                        if (Utility.isNetworkAvailable(getActivity())) {
                            progress.show();
                            Utility.displayToast(getActivity(), getString(R.string.resend_otp));
                            AccountManager accountManager = new AccountManager(getActivity());
                            // code commmited as create new method for adding hardcore response while user is resending otp
                          //  accountManager.resendOTPTask(mobileNo, PrefManager.getStringFromPref(getActivity(), Constants.PreferencesManagerKey.CTID));
                            accountManager.resendOTP(mobileNo, PrefManager.getStringFromPref(getActivity(), Constants.PreferencesManagerKey.CTID));
                        } else {
                            Utility.displayToast(getActivity(), getResources().getString(R.string.internet_connectivity_message));
                        }
                    }
                }
            });
        }

    }

    public void onEvent(OTPverifyEvent event) {
        progress.dismiss();
        if (event.otpverifyResponse != null) {
            if ((!TextUtils.isEmpty(event.otpverifyResponse.message)) && (event.otpverifyResponse.status.equalsIgnoreCase(getString(R.string.server_success_ok)))) {
                if (event.isforResend) {
                    Utility.displayToast(getActivity(), event.otpverifyResponse.message);
                } else {
                    Utility.displayToast(getActivity(), event.otpverifyResponse.message);
                    ((OnboardingActivity) getActivity()).removeCurrentScreen();
                }

            } else {
                Utility.displayToast(getActivity(), event.otpverifyResponse.message);
            }
        } else {
            Utility.displayToast(getActivity(), getString(R.string.msg_service_call_error));
        }
    }

    public void onEvent(ResendOTPverifyEvent event) {
        progress.dismiss();
        if (event.otpverifyResponse != null) {
            if ((!TextUtils.isEmpty(event.otpverifyResponse.message)) && (event.otpverifyResponse.status.equalsIgnoreCase(getString(R.string.server_success_ok)))) {
                if (event.isforResend) {
                    Utility.displayToast(getActivity(), event.otpverifyResponse.message);
                } else {
                    Utility.displayToast(getActivity(), event.otpverifyResponse.message);
                    //((OnboardingActivity) getActivity()).removeCurrentScreen();
                }

            } else {
                Utility.displayToast(getActivity(), event.otpverifyResponse.message);
            }
        } else {
            Utility.displayToast(getActivity(), getString(R.string.msg_service_call_error));
        }
    }


    private void getAllStatesFromService() {
        progress.show();
        AccountManager accountManager = new AccountManager(getActivity());
        accountManager.getAllStates();

    }

    private void getAllCasteFromService() {
        progress.show();
        AccountManager accountManager = new AccountManager(getActivity());
        accountManager.getCaste();

    }

    private void getAllReligionFromService() {
        progress.show();
        AccountManager accountManager = new AccountManager(getActivity());
        accountManager.getReligion();

    }


    public void onEvent(ReligionEvent event) {
        progress.dismiss();
        // Get Religion after caste

        if (event.religionResponse != null) {
            if ((!TextUtils.isEmpty(event.religionResponse.message)) && (event.religionResponse.status.equalsIgnoreCase(getString(R.string.server_success_ok)))) {

                ArrayList<ReligionResponse.Religion> religion_data = event.religionResponse.getReligion();
                if (religion_data != null) {
                    religion_values = new ArrayList<>();
                    religion_values.add("Please Select Religion*");
                    for (int i = 0; i < religion_data.size(); i++) {
                        if (religion_data.get(i).getReligion() != null) {
                            religion_values.add(religion_data.get(i).getReligion());
                        }
                    }
                    religionAdapter = new ArrayAdapter(context, android.R.layout.simple_spinner_item, religion_values);
                    religionAdapter.setDropDownViewResource(
                            android.R.layout.simple_spinner_dropdown_item);
                    spinner_religion.setAdapter(religionAdapter);
                    spinner_religion.setSelection(Adapter.NO_SELECTION, false);
                    spinner_religion.setOnItemSelectedListener(this);

                }
            } else {
                Utility.displayToast(getActivity(), event.religionResponse.message);
            }
        } else {
            Utility.displayToast(getActivity(), getString(R.string.msg_service_call_error));
        }
    }

    public void onEvent(CasteEvent event) {
        progress.dismiss();
        // Get Religion after caste
        getAllReligionFromService();
        if (event.casteResponse != null) {
            if ((!TextUtils.isEmpty(event.casteResponse.message)) && (event.casteResponse.status.equalsIgnoreCase(getString(R.string.server_success_ok)))) {

                ArrayList<CasteResponse.Caste> caste_data = event.casteResponse.getCaste();
                if (caste_data != null) {
                    caste_values = new ArrayList<>();
                    caste_values.add("Please Select caste");
                    for (int i = 0; i < caste_data.size(); i++) {
                        if (caste_data.get(i).getCastename() != null) {
                            caste_values.add(caste_data.get(i).getCastename());
                        }
                    }
                    casteAdapter = new ArrayAdapter(context, android.R.layout.simple_spinner_item, caste_values);
                    casteAdapter.setDropDownViewResource(
                            android.R.layout.simple_spinner_dropdown_item);
                    spinner_caste.setAdapter(casteAdapter);
                    spinner_caste.setSelection(Adapter.NO_SELECTION, false);
                    spinner_caste.setOnItemSelectedListener(this);

                }
            } else {
                Utility.displayToast(getActivity(), event.casteResponse.message);
            }
        } else {
            Utility.displayToast(getActivity(), getString(R.string.msg_service_call_error));
        }
    }

    public void onEvent(StatesEvent event) {
        progress.dismiss();
        if (event.stateResponse != null) {
            if ((!TextUtils.isEmpty(event.stateResponse.message)) && (event.stateResponse.status.equalsIgnoreCase(getString(R.string.server_success_ok)))) {

                ArrayList<StateResponse.State> states_data = event.stateResponse.getStates();
                if (states_data != null) {
                    getAllCasteFromService();
                    state_values = new ArrayList<>();
                    state_values.add("Please Select State*");
                    for (int i = 0; i < states_data.size(); i++) {
                        if (states_data.get(i).getState() != null) {
                            state_values.add(states_data.get(i).getState());
                        }
                    }
                    stateAdapter = new ArrayAdapter(context, android.R.layout.simple_spinner_item, state_values);
                    stateAdapter.setDropDownViewResource(
                            android.R.layout.simple_spinner_dropdown_item);
                    spinner_state.setAdapter(stateAdapter);
                    spinner_state.setSelection(Adapter.NO_SELECTION, false);
                    spinner_state.setOnItemSelectedListener(this);

                }
            } else {
                Utility.displayToast(getActivity(), event.stateResponse.message);
            }
        } else {
            Utility.displayToast(getActivity(), getString(R.string.msg_service_call_error));
        }
    }


    private void getAllDistrictsFromService(String state) {
        progress.show();
        AccountManager accountManager = new AccountManager(getActivity());
        accountManager.getAllDistricts(state);

    }

    public void onEvent(DistrictEvent event) {
        progress.dismiss();
        if (event.districtResponse != null) {
            if ((!TextUtils.isEmpty(event.districtResponse.message)) && (event.districtResponse.status.equalsIgnoreCase(getString(R.string.server_success_ok)))) {

                ArrayList<DistrictResponse.District> district_data = event.districtResponse.getDistricts();
                if (district_data != null) {
                    dist_values = new ArrayList<>();
                    dist_values.add("Please Select District*");
                    for (int i = 0; i < district_data.size(); i++) {
                        if (district_data.get(i).getDistrict() != null) {
                            dist_values.add(district_data.get(i).getDistrict());
                        }
                    }
                    districtAdapter = new ArrayAdapter(context, android.R.layout.simple_spinner_item, dist_values);
                    districtAdapter.setDropDownViewResource(
                            android.R.layout.simple_spinner_dropdown_item);
                    spinner_district.setAdapter(districtAdapter);
                    spinner_district.setSelection(Adapter.NO_SELECTION, false);
                    spinner_district.setOnItemSelectedListener(this);
                }
            } else {
                Utility.displayToast(getActivity(), event.districtResponse.message);
            }
        } else {
            Utility.displayToast(getActivity(), getString(R.string.msg_service_call_error));
        }
    }


    private void getAllTalukaFromService(String district) {
        progress.show();
        AccountManager accountManager = new AccountManager(getActivity());
        accountManager.getAllTalukas(district);
    }

    public void onEvent(TalukaEvent event) {
        progress.dismiss();
        if (event.talukaResponse != null) {
            if ((!TextUtils.isEmpty(event.talukaResponse.message)) && (event.talukaResponse.status.equalsIgnoreCase(getString(R.string.server_success_ok)))) {

                ArrayList<TalukaResponse.Taluka> taluka_data = event.talukaResponse.getTalukas();
                if (taluka_data != null) {
                    taluka_values = new ArrayList<>();
                    taluka_values.add("Please Select Taluka*");
                    for (int i = 0; i < taluka_data.size(); i++) {
                        if (taluka_data.get(i).getTaluka() != null) {
                            taluka_values.add(taluka_data.get(i).getTaluka());
                        }
                    }
                    talukaAdapter = new ArrayAdapter(context, android.R.layout.simple_spinner_item, taluka_values);
                    talukaAdapter.setDropDownViewResource(
                            android.R.layout.simple_spinner_dropdown_item);
                    spinner_taluka.setAdapter(talukaAdapter);
                    spinner_taluka.setSelection(Adapter.NO_SELECTION, false);
                    spinner_taluka.setOnItemSelectedListener(this);

                }
            } else {
                Utility.displayToast(getActivity(), event.talukaResponse.message);
            }
        } else {
            Utility.displayToast(getActivity(), getString(R.string.msg_service_call_error));
        }
    }

    private void getAllPincodesFromService(String taluka) {
        progress.show();
        AccountManager accountManager = new AccountManager(getActivity());
        accountManager.getAllPinCodes(taluka);
    }

    public void onEvent(PinCodeEvent event) {
        progress.dismiss();
        if (event.pinCodeResponse != null) {
            if ((!TextUtils.isEmpty(event.pinCodeResponse.message)) && (event.pinCodeResponse.status.equalsIgnoreCase(getString(R.string.server_success_ok)))) {

                ArrayList<PinCodeResponse.Pincode> pincodes_data = event.pinCodeResponse.getPincodes();
                if (pincodes_data != null) {
                    pincode_values = new ArrayList<>();
                    pincode_values.add("Please Select Pincode*");
                    for (int i = 0; i < pincodes_data.size(); i++) {
                        if (pincodes_data.get(i).getPincode() != null) {
                            pincode_values.add(pincodes_data.get(i).getPincode());
                        }
                    }
                    pincodeAdapter = new ArrayAdapter(context, android.R.layout.simple_spinner_item, pincode_values);
                    pincodeAdapter.setDropDownViewResource(
                            android.R.layout.simple_spinner_dropdown_item);
                    spinner_pincode.setAdapter(pincodeAdapter);
                    spinner_pincode.setSelection(Adapter.NO_SELECTION, false);
                    spinner_pincode.setOnItemSelectedListener(this);

                }
            } else {
                Utility.displayToast(getActivity(), event.pinCodeResponse.message);
            }
        } else {
            Utility.displayToast(getActivity(), getString(R.string.msg_service_call_error));
        }
    }


    @Override
    public void exit() {
        ((OnboardingActivity) getActivity()).removeCurrentScreen();
    }
}