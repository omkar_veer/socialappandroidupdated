package com.pas.socialapp.eventhandlers;

import com.pas.socialapp.models.DistrictResponse;
import com.pas.socialapp.models.GCMRegistrationResponse;


public class GCMRegistrationEvent {

    public GCMRegistrationResponse registrationResponse;

    public GCMRegistrationEvent(GCMRegistrationResponse registrationResponse) {
        this.registrationResponse = registrationResponse;
    }

}
