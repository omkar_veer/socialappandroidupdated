package com.pas.socialapp.views.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.pas.socialapp.R;
import com.pas.socialapp.managers.DatabaseManager;
import com.pas.socialapp.models.Notification;
import com.pas.socialapp.models.NotificationDao;
import com.pas.socialapp.views.adapters.NotificationAdapter;

import de.greenrobot.event.EventBus;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ganesh.Bahirat on 5/14/2015.
 */
public class NotificationFragment extends Fragment {

    private View rootView;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private EventBus eventBus = EventBus.getDefault();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_notification, container, false);
        initScreen();
        return rootView;
    }

    private void initScreen() {
        setRecyclerView();

        List<Notification> notificationList = DatabaseManager.getInstance().getDaoSession()
                .getNotificationDao().getNotifications(NotificationDao.Properties.IsEnable, true);
        if (notificationList != null && !notificationList.isEmpty())
            ((NotificationAdapter) mAdapter).updateData(notificationList);
        else
            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.no_data_available), Toast.LENGTH_LONG).show();
/*

        Notification textNotification = new Notification();
        textNotification.setTitle("I am text title...");
        textNotification.setDescription("I am text description...");
        textNotification.setUrl("");
        textNotification.setType("Text");
        textNotification.setTime("24/06/2016 5:55 PM");
        textNotification.setIsEnable(true);

        Notification audioNotification = new Notification();
        audioNotification.setTitle("I am Audio title...");
        audioNotification.setDescription("I am Audio description...");
        audioNotification.setUrl("http://gaana99.com/fileDownload/Songs/0/28703.mp3");
        audioNotification.setType("Audio");
        audioNotification.setTime("24/06/2016 5:55 PM");
        audioNotification.setIsEnable(true);

        Notification videoNotification = new Notification();
        videoNotification.setTitle("I am Video title...");
        videoNotification.setDescription("I am Video description...");
        videoNotification.setUrl("http://www.quirksmode.org/html5/videos/big_buck_bunny.mp4");
        videoNotification.setType("Video");
        videoNotification.setTime("24/06/2016 5:55 PM");
        videoNotification.setIsEnable(true);

        Notification ImageNotification = new Notification();
        ImageNotification.setTitle("I am Image title...");
        ImageNotification.setDescription("I am Image description...");
        ImageNotification.setUrl("http://www.nagarisuvidha.com/sapp/backoffice/webservice/uploads/announcement/image/utility%20bills.jpg");
        ImageNotification.setType("Image");
        ImageNotification.setTime("24/06/2016 5:55 PM");
        ImageNotification.setIsEnable(true);

       // List<Notification> notificationList = new ArrayList<>();
        notificationList.add(textNotification);
        notificationList.add(audioNotification);
        notificationList.add(ImageNotification);
        notificationList.add(videoNotification);
*/

        ((NotificationAdapter) mAdapter).updateData(notificationList);
    }


    private void setRecyclerView() {
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.notification_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new NotificationAdapter(getContext(), new ArrayList<Notification>());
        mRecyclerView.setAdapter(mAdapter);
        ((NotificationAdapter) mAdapter).setOnItemClickListener(new NotificationAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
            }
        });
    }

    public void onEventMainThread(String refresh) {
        initScreen();
    }


    @Override
    public void onStop() {
        if (eventBus != null) {
            eventBus.unregister(this);
        }
        super.onStop();
    }

    @Override
    public void onPause() {
        if (eventBus != null) {
            eventBus.unregister(this);
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        if (eventBus != null) {
            eventBus.register(this);
        }
        super.onResume();
    }

}