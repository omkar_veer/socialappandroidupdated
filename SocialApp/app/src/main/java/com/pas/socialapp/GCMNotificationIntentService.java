package com.pas.socialapp;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.pas.socialapp.managers.DatabaseManager;
import com.pas.socialapp.managers.PrefManager;
import com.pas.socialapp.views.activities.MainActivity;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * The type Gcm notification intent service.
 */
public class GCMNotificationIntentService extends IntentService {
    /**
     * The constant notifyID.
     */
    public static final int notifyID = 9001;

    /**
     * Instantiates a new Gcm notification intent service.
     */
    public GCMNotificationIntentService() {
        super("GcmIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);

        Log.v("GYM_GCM", "In onHandleIntent");
        String messageType = gcm.getMessageType(intent);

//        array('title' => 'eventtitle', 'description' => 'eventdescription', 'eventvenue' =>
// 'eventevenue', 'eventdate' => 'eventdate');

        if (!extras.isEmpty()) {
            if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
                sendNotification("Send error: " + extras.toString(), "", "", "");
                Log.d("GYM_GCM", "MESSAGE_TYPE_SEND_ERROR");
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
                sendNotification("Deleted messages on server: "
                        + extras.toString(), "", "", "");
                Log.d("GYM_GCM", "MESSAGE_TYPE_DELETED");
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {

                if (PrefManager.isUserLoggedIn(this)) {
                    if (!TextUtils.isEmpty(extras.getString(Constants.NOTIFICATION_TITLE_KEY))) {
                        Log.v("GYM_GCM", "Title > " + extras.getString(Constants.NOTIFICATION_TITLE_KEY));
                        Log.v("GYM_GCM", "Type > " + extras.getString(Constants.NOTIFICATION_TYPE_KEY));
                        String title = extras.getString(Constants.NOTIFICATION_TITLE_KEY);
                        String description = extras.getString(Constants.NOTIFICATION_DESC_KEY);
                        String url = extras.getString(Constants.NOTIFICATION_URL_KEY);
                        String type = extras.getString(Constants.NOTIFICATION_TYPE_KEY);
                        String eventvenue = extras.getString(Constants.NOTIFICATION_VENUE_KEY);
                        String date = extras.getString(Constants.NOTIFICATION_DATE_KEY);

                        if (TextUtils.isEmpty(type)) {
                            String desc = description + "\nVenue : " + eventvenue + "\nDate : " + date;
                            sendNotification(title, desc, url, type);
                            addNotificationToDb(title, description, eventvenue, date, false);
                        } else {
                            sendNotification(title, description, url, type);
                            addNotificationToDb(title, description, url, type, true);
                        }

                       // sendNotification(title, description, url, type);

                    } else {
                        Log.v("GYM_GCM", "Empty Title");
                    }
                } else {
                    Log.v("GYM_GCM", "User is not logged in");
                }
            }
        }
        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }

    private void addNotificationToDb(String title, String desc, String url, String type, boolean isForMessagnger) {
        com.pas.socialapp.models.Notification notification = new com.pas.socialapp.models.Notification();
        notification.setTitle(title);
        notification.setDescription(desc);
        notification.setUrl(url);
        notification.setType(type);
        notification.setTime(getCurrentTime());
        notification.setIsEnable(isForMessagnger);
        DatabaseManager.getInstance().getDaoSession().getNotificationDao().insertOrReplace(notification);
    }

    public String getCurrentTime() {
        SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy HH:mm a");
        Date now = new Date();
        String strDate = sdfDate.format(now);
        return strDate;
    }

    private void sendNotification(String title, String desc, String url, String type) {
        Intent resultIntent = new Intent(this, MainActivity.class);
        if(TextUtils.isEmpty(type)){
            resultIntent.putExtra("IsFromNot","0");
        }else{
            resultIntent.putExtra("IsFromNot","1");
        }
        resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 0, resultIntent, PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder mNotifyBuilder;
        NotificationManager mNotificationManager;

        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        mNotifyBuilder = new NotificationCompat.Builder(this).setContentTitle(getString(R.string.app_name)).setContentText(title).setSmallIcon(R.drawable.app_icon);
        // Set pending intent
        mNotifyBuilder.setContentIntent(resultPendingIntent);

        // Set Vibrate, Sound and Light
        int defaults = 0;
        defaults = defaults | Notification.DEFAULT_LIGHTS;
        defaults = defaults | Notification.DEFAULT_VIBRATE;
        defaults = defaults | Notification.DEFAULT_SOUND;

        mNotifyBuilder.setDefaults(defaults);
        mNotifyBuilder.setContentText(desc);
        mNotifyBuilder.setAutoCancel(true);
        mNotifyBuilder.setSmallIcon( getNotificationIcon() );
        mNotificationManager.notify(notifyID, mNotifyBuilder.build());
    }

    private int getNotificationIcon() {
        boolean useWhiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.drawable.ic_launcher : R.drawable.ic_launcher;
    }
}
