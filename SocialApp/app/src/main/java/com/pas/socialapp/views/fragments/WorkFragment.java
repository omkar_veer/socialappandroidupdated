package com.pas.socialapp.views.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pas.socialapp.Constants;
import com.pas.socialapp.R;
import com.pas.socialapp.eventhandlers.GenericErrorEvent;
import com.pas.socialapp.eventhandlers.WorkEvent;
import com.pas.socialapp.managers.AccountManager;
import com.pas.socialapp.models.WorkResponse;
import com.pas.socialapp.utils.Utility;
import com.pas.socialapp.views.adapters.WorkAdapter;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;

public class WorkFragment extends Fragment {

    private View rootView;
    private EventBus eventBus = EventBus.getDefault();
    private ProgressDialog progress;

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private Context context;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_work, container, false);
        Utility.floatingButtonVisibility(getActivity());

        initScreen();
        return rootView;
    }

    private void initScreen() {
        progress = new ProgressDialog(getActivity());
        progress.setCancelable(false);
        progress.setMessage(getString(R.string.msg_loading));

        context = getActivity();

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.profileRecyclerView);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(context);
        mRecyclerView.setLayoutManager(mLayoutManager);

        progress.show();
        AccountManager accountManager = new AccountManager(getActivity());
        accountManager.getWorkDetails(Constants.WORK_TAB);
    }

    @Override
    public void onStop() {
        if (eventBus != null) {
            eventBus.unregister(this);
        }
        super.onStop();
    }

    @Override
    public void onPause() {
        if (eventBus != null) {
            eventBus.unregister(this);
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        if (eventBus != null) {
            eventBus.register(this);
        }
        super.onResume();
    }

    public void onEvent(GenericErrorEvent event) {
        progress.dismiss();
    }
    public void onEvent(WorkEvent event) {
        progress.dismiss();
        if (event.workResponse != null) {

            if (!TextUtils.isEmpty(event.workResponse.message) && (event.workResponse.status.equalsIgnoreCase(getString(R.string.server_success_ok)))) {
                // Set here  Data.
                ArrayList<WorkResponse.Work> workData = event.workResponse.work;
                if(workData !=null) {
                    if (workData.size() > 0) {
                        mAdapter = new WorkAdapter(workData, context);
                        mRecyclerView.setAdapter(mAdapter);
                    }
                }

            } else {
                Utility.displayToast(getActivity(), event.workResponse.message);
            }


        } else {
            Utility.displayToast(getActivity(), getString(R.string.msg_service_call_error));
        }
    }

}