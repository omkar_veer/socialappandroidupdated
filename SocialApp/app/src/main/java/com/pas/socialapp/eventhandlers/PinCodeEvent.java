package com.pas.socialapp.eventhandlers;

import com.pas.socialapp.models.PinCodeResponse;

/**
 * Created by Umesh on 24/02/16.
 */
public class PinCodeEvent {

    public PinCodeResponse pinCodeResponse;

    public PinCodeEvent(PinCodeResponse pinCodeResponse) {
        this.pinCodeResponse = pinCodeResponse;
    }

}
