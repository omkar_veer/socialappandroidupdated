package com.pas.socialapp.models;

public class Notification {

    private Long _id;
    private String id;
    private String description;
    private String title;
    private String url;
    private String type;
    private String time;
    private boolean isEnable;

    public Notification() {
    }

    public Notification(Long _id, String id, String title, String description, String url, String type, String time, boolean isEnable) {
        this._id = _id;
        this.id = id;
        this.title = title;
        this.description = description;
        this.url = url;
        this.type = type;
        this.isEnable = isEnable;
        this.time = time;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long get_id() {
        return _id;
    }

    public void set_id(Long _id) {
        this._id = _id;
    }


    public boolean isEnable() {
        return isEnable;
    }

    public void setIsEnable(boolean isEnable) {
        this.isEnable = isEnable;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
