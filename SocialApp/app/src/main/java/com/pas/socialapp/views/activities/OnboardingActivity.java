package com.pas.socialapp.views.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

import com.pas.socialapp.R;
import com.pas.socialapp.views.fragments.EmailVerifyFragment;
import com.pas.socialapp.views.fragments.LoginFragment;
import com.pas.socialapp.views.fragments.SignUpFragment;
import com.pas.socialapp.views.fragments.SplashFragment;


/**
 * The type Onboarding Activity.
 */
public class OnboardingActivity extends FragmentActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_onboarding);
//        FacebookSdk.sdkInitialize(this.getApplicationContext());
        moveToScreen(new SplashFragment());
    }


    /**
     * Move to screen.
     *
     * @param fragment the fragment
     */
    public void moveToScreen(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.onboardingContainer, fragment)
                .commit();
    }

    public void moveToScreenWithBackstack(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.onboardingContainer, fragment)
                .addToBackStack(fragment.getClass().getName())
                .commit();
    }


    @Override
    public void onBackPressed() {
        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.onboardingContainer);
        if (currentFragment instanceof SignUpFragment)
            removeCurrentScreen();
        else if (currentFragment instanceof EmailVerifyFragment) {
            clearBackstack();
            moveToScreen(new LoginFragment());
        } else
            super.onBackPressed();
    }

    public void clearBackstack() {
        FragmentManager fm = getSupportFragmentManager();
        for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }
    }

    public void removeCurrentScreen() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        }
    }


    /**
     * Move to main activity.
     */
    public void moveToMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }


}
