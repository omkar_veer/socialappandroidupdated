package com.pas.socialapp.views.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.RadioButton;

import com.pas.socialapp.R;


/**
 * Created by Umesh on 10/02/2015.
 */

public class CustomRadioButton extends RadioButton {

    /**
     * Instantiates a new custom text view.
     *
     * @param context the context
     */
    public CustomRadioButton(Context context) {
        super(context);
    }

    /**
     * Instantiates a new custom text view.
     *
     * @param context the context
     * @param attrs   the attrs
     */
    public CustomRadioButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomFont(context, attrs);
    }

    /**
     * Instantiates a new custom text view.
     *
     * @param context  the context
     * @param attrs    the attrs
     * @param defStyle the def style
     */
    public CustomRadioButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setCustomFont(context, attrs);
    }

    /**
     * Sets the custom font.
     *
     * @param context the context
     * @param attrs   the attrs
     */
    private void setCustomFont(Context context, AttributeSet attrs) {

        TypedArray typedArray = context.obtainStyledAttributes(attrs,
                R.styleable.CustomView);
        String customFont = typedArray
                .getString(R.styleable.CustomView_customFont);
        setCustomFont(context, customFont);
        typedArray.recycle();
    }

    /**
     * Sets the custom font.
     *
     * @param context the context
     * @param asset   the asset
     * @return true, if successful
     */
    public boolean setCustomFont(Context context, String asset) {
        Typeface typeface = null;
        typeface = Typefaces.get(context, asset);
        setTypeface(typeface);
        return true;
    }

}
