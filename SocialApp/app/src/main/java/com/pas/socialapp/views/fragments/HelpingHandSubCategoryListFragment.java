package com.pas.socialapp.views.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pas.socialapp.R;
import com.pas.socialapp.eventhandlers.HelpingHandEvent;
import com.pas.socialapp.eventhandlers.ServiceCategoryEvent;
import com.pas.socialapp.managers.AccountManager;
import com.pas.socialapp.models.HelpingHandCategoryResponse;
import com.pas.socialapp.models.ServiceCategoryResponse;
import com.pas.socialapp.utils.Utility;
import com.pas.socialapp.views.adapters.HelpingHandSubCategoryListAdapter;
import com.pas.socialapp.views.adapters.ServicesSubCategoryListAdapter;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;

public class HelpingHandSubCategoryListFragment extends Fragment implements HelpingHandSubCategoryListAdapter.OpenURLListener {

    private View rootView;
    private EventBus eventBus = EventBus.getDefault();
    private ProgressDialog progress;

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private Context context;
    String serviceType;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        serviceType = getArguments().getString("hh_serviceType");
        rootView = inflater.inflate(R.layout.hh_sub_category_fragment, container, false);
        Utility.floatingButtonVisibility(getActivity());
        initScreen();
        return rootView;
    }

    private void initScreen() {
        progress = new ProgressDialog(getActivity());
        progress.setCancelable(false);
        progress.setMessage(getString(R.string.msg_loading));
        context = getActivity();

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.hhSubCategoryRecyclerView);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(context);
        mRecyclerView.setLayoutManager(mLayoutManager);
        progress.show();
        AccountManager accountManager = new AccountManager(getActivity());
        accountManager.getHelpingHnadByCategory(serviceType);
    }

    @Override
    public void onStop() {
        if (eventBus != null) {
            eventBus.unregister(this);
        }
        super.onStop();
    }

    @Override
    public void onPause() {
        if (eventBus != null) {
            eventBus.unregister(this);
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        if (eventBus != null) {
            eventBus.register(this);
        }
        super.onResume();
    }

    public void onEvent(HelpingHandEvent event) {
        progress.dismiss();
        if (event.helpingHandCategoryResponse != null) {

            if (!TextUtils.isEmpty(event.helpingHandCategoryResponse.message) && (event.helpingHandCategoryResponse.status.equalsIgnoreCase(getString(R.string.server_success_ok)))) {
                // Set here  Data.
                ArrayList<HelpingHandCategoryResponse.Hhdatum> hhdatumArrayList = event.helpingHandCategoryResponse.getHhdata();

                if (hhdatumArrayList != null) {
                    if (hhdatumArrayList.size() > 0) {
                        mAdapter = new HelpingHandSubCategoryListAdapter(hhdatumArrayList, context);
                        mRecyclerView.setAdapter(mAdapter);
                        ((HelpingHandSubCategoryListAdapter) mAdapter).openURLServiceListener(HelpingHandSubCategoryListFragment.this);
                    }else{
                        Utility.displayToast(getActivity(),  context.getResources().getString(R.string.no_data_available));
                    }
                }
            } else {
                Utility.displayToast(getActivity(), event.helpingHandCategoryResponse.message);
            }

        } else {
            Utility.displayToast(getActivity(), getString(R.string.msg_service_call_error));
        }
    }


    @Override
    public void openURLinWEbView(String url) {
        if (!url.startsWith("http://") && !url.startsWith("https://"))
            url = "http://" + url;

        Uri uri = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }
}