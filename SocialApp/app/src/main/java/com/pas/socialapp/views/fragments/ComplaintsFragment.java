package com.pas.socialapp.views.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;

import com.pas.socialapp.Constants;
import com.pas.socialapp.R;
import com.pas.socialapp.eventhandlers.ComplaintsTypeEvent;
import com.pas.socialapp.eventhandlers.GenericErrorEvent;
import com.pas.socialapp.eventhandlers.SubmitComplaintsEvent;
import com.pas.socialapp.managers.AccountManager;
import com.pas.socialapp.managers.PrefManager;
import com.pas.socialapp.models.ComplaintsTypeResponse;
import com.pas.socialapp.utils.Utility;
import com.pas.socialapp.views.activities.MainActivity;
import com.pas.socialapp.views.custom.CustomButton;
import com.pas.socialapp.views.custom.CustomEditText;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.greenrobot.event.EventBus;


public class ComplaintsFragment extends Fragment implements AdapterView.OnItemSelectedListener {

    private View rootView;
    private ProgressDialog progress;
    private EventBus eventBus = EventBus.getDefault();
    private ArrayAdapter complaintsTypeAdapter;
    private Context context;
    private ArrayList<String> complaints_type;
    private Spinner complaints_type_spinner;
    private String complaintsType;
    private CustomButton sendComplaintButton;
    private CustomEditText titleEditText, descEditText;
    public HashMap<Integer, Bitmap> complaintImages = new HashMap<>();
    public int selectedPosition = 0;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_complaints, container, false);
        ImageView floatingActionButton = (ImageView) ((MainActivity) getActivity()).findViewById(R.id.fab);
        floatingActionButton.setVisibility(View.GONE);
        initView();
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        context = activity;
    }

    private void initView() {
        sendComplaintButton = (CustomButton) rootView.findViewById(R.id.send_complaint_button);
        progress = new ProgressDialog(getActivity());
        progress.setCancelable(false);
        progress.setMessage(getString(R.string.msg_loading));
        complaints_type_spinner = (Spinner) rootView.findViewById(R.id.complaints_type_spinner);
        getComplaintsTypeFromServer();

        rootView.findViewById(R.id.comp_image1).setOnClickListener(onClickListener);
        rootView.findViewById(R.id.comp_image2).setOnClickListener(onClickListener);
        rootView.findViewById(R.id.comp_image3).setOnClickListener(onClickListener);

        titleEditText = (CustomEditText) rootView.findViewById(R.id.complaints_title);
        descEditText = (CustomEditText) rootView.findViewById(R.id.complaints_desc);


        sendComplaintButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validate();
            }
        });
    }

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String tagVal = (String) v.getTag();
            selectedPosition = Integer.parseInt(tagVal);
            displayChooseImageAlert();
        }
    };


    static final int ACTION_REQUEST_GALLERY = 123;
    static final int ACTION_REQUEST_CAMERA = 321;

    private void displayChooseImageAlert() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Choose Image Source");
        builder.setItems(new CharSequence[]{"Gallery", "Camera", "Remove"},
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                 /* For Image capture from Gallary*/
                                getActivity().startActivityForResult(new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI),
                                        ACTION_REQUEST_GALLERY);
                                break;

                            case 1:
                                    /* For Image capture from camera */
                                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                getActivity().startActivityForResult(intent, ACTION_REQUEST_CAMERA);
                                break;

                            case 2:
                                complaintImages.remove(selectedPosition);
                                updateComplaintImageViews();
                                break;

                            default:
                                break;
                        }
                    }
                });

        builder.show();

    }

    public void updateComplaintImageViews() {

        ImageView image1 = (ImageView) rootView.findViewById(R.id.comp_image1);
        ImageView image2 = (ImageView) rootView.findViewById(R.id.comp_image2);
        ImageView image3 = (ImageView) rootView.findViewById(R.id.comp_image3);

        if (complaintImages != null) {
            Bitmap value1 = complaintImages.get(1);
            if (value1 != null)
                image1.setImageBitmap(value1);
            else
                image1.setImageResource(R.drawable.icn_add);

            Bitmap value2 = complaintImages.get(2);
            if (value2 != null)
                image2.setImageBitmap(value2);
            else
                image2.setImageResource(R.drawable.icn_add);

            Bitmap value3 = complaintImages.get(3);
            if (value3 != null)
                image3.setImageBitmap(value3);
            else
                image3.setImageResource(R.drawable.icn_add);
        } else {
            image1.setImageResource(R.drawable.icn_add);
            image2.setImageResource(R.drawable.icn_add);
            image3.setImageResource(R.drawable.icn_add);
        }

    }


    private String getBase64(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(byteArray, Base64.DEFAULT);
    }


    private void validate() {
        String complainttitle, complaintdescription;
        String complaintcategory = complaints_type_spinner.getSelectedItem().toString();
        String complaintimage1 = "";
        String complaintimage2 = "";
        String complaintimage3 = "";
        String CTID = PrefManager.getCTID(getActivity());
        String CID = getActivity().getResources().getString(R.string.candidate_id);
        // complainttitle = titleEditText.getText().toString().trim().replace(" ", "");
        //complaintdescription = descEditText.getText().toString().trim().replace(" ", "");

        complainttitle = titleEditText.getText().toString();
        complaintdescription = descEditText.getText().toString();

        if (TextUtils.isEmpty(complainttitle)) {
            titleEditText.setError(getString(R.string.msg_invalid_entry));
            return;
        }

        if (TextUtils.isEmpty(complaintdescription)) {
            descEditText.setError(getString(R.string.msg_invalid_entry));
            return;
        }

        progress.show();
        for (Map.Entry<Integer, Bitmap> entry : complaintImages.entrySet()) {
            int key = entry.getKey();
            Bitmap value = entry.getValue();
            switch (key) {
                case 1:
                    //complaintimage1 = "data:image/png;base64,"+ getBase64(value);
                    complaintimage1 = "data:image/png;base64,"+ getBase64(value);
                    break;

                case 2:
                    complaintimage2 = getBase64(value);
                    break;

                case 3:
                    complaintimage3 = getBase64(value);
                    break;
            }
        }

        if (Utility.isNetworkAvailable(getActivity())) {
            Utility.hideSoftkeyboard(getActivity());
            AccountManager accountManager = new AccountManager(getActivity());
            accountManager.submitComplaint(complaintcategory, complaintdescription, complainttitle,
                    complaintimage1, complaintimage2, complaintimage3, CTID, CID);
        } else {
            progress.dismiss();
            Utility.displayToast(getActivity(), getResources().getString(R.string.internet_connectivity_message));
        }


    }

    private void getComplaintsTypeFromServer() {
        progress.show();
        AccountManager accountManager = new AccountManager(getActivity());
        accountManager.getComplaintsTypeFromServer();

    }

    public void onEvent(ComplaintsTypeEvent event) {
        progress.dismiss();
        if (event.typeResponse != null) {
            if ((!TextUtils.isEmpty(event.typeResponse.message)) && (event.typeResponse.status.equalsIgnoreCase(getString(R.string.server_success_ok)))) {

                ArrayList<ComplaintsTypeResponse.ComplaintsType> complaintsTypesList = event.typeResponse.getComplaintcategory();
                if (complaintsTypesList != null) {
                    complaints_type = new ArrayList<>();
                    //  complaints_type.add("Please Select complaint type");
                    for (int i = 0; i < complaintsTypesList.size(); i++) {
                        if (complaintsTypesList.get(i).getComplaintcategoryMaster() != null) {
                            complaints_type.add(complaintsTypesList.get(i).getComplaintcategoryMaster());
                        }
                    }
                    complaintsTypeAdapter = new ArrayAdapter(context, android.R.layout.simple_spinner_item, complaints_type);
                    complaintsTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    complaints_type_spinner.setAdapter(complaintsTypeAdapter);
                    complaints_type_spinner.setSelection(Adapter.NO_SELECTION, false);
                    complaints_type_spinner.setOnItemSelectedListener(this);

                }
            } else {
                Utility.displayToast(getActivity(), event.typeResponse.message);
            }
        } else {
            Utility.displayToast(getActivity(), getString(R.string.msg_service_call_error));
        }
    }

    public void onEvent(SubmitComplaintsEvent event) {
        progress.dismiss();
        if (event.complaintsResponse != null) {
            if ((!TextUtils.isEmpty(event.complaintsResponse.message)) && (event.complaintsResponse.status.equalsIgnoreCase(getString(R.string.server_success_ok)))) {
                Utility.displayToast(getActivity(), event.complaintsResponse.message);
                getFragmentManager().popBackStack();
            } else {
                Utility.displayToast(getActivity(), event.complaintsResponse.message);
            }
        } else {
            Utility.displayToast(getActivity(), getString(R.string.msg_service_call_error));
        }
    }

    public void onEvent(GenericErrorEvent event) {
        progress.dismiss();
    }


    @Override
    public void onStop() {
        if (eventBus != null) {
            eventBus.unregister(this);
        }
        super.onStop();
    }


    @Override
    public void onPause() {
        if (eventBus != null) {
            eventBus.unregister(this);
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        if (eventBus != null) {
            eventBus.register(this);
        }
        super.onResume();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {
            case R.id.complaints_type_spinner:
                complaintsType = parent.getItemAtPosition(position).toString();
                break;

            default:
                break;

        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}