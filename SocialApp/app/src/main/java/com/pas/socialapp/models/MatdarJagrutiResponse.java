package com.pas.socialapp.models;

import java.util.ArrayList;

public class MatdarJagrutiResponse extends GenericRequest {

    public String status;
    public String service;
    public String message;
    public ArrayList<MatdarJagruti> mjdata = new ArrayList<MatdarJagruti>();

    /**
     * @return The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return The service
     */
    public String getService() {
        return service;
    }

    /**
     * @param service The service
     */
    public void setService(String service) {
        this.service = service;
    }

    /**
     * @return The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<MatdarJagruti> getMjdata() {
        return mjdata;
    }

    public void setMjdata(ArrayList<MatdarJagruti> mjdata) {
        this.mjdata = mjdata;
    }

    public class MatdarJagruti {

        private String mjid;
        private String mjcategoryname;
        private String dataurl;
        private String pdffile;
        private String candidateid;
        private String created;
        private String updated;
        private String mjstatus;
        private String mjdataid;
        private String mjurl;

        public String getMjid() {
            return mjid;
        }

        public void setMjid(String mjid) {
            this.mjid = mjid;
        }

        public String getMjcategoryname() {
            return mjcategoryname;
        }

        public void setMjcategoryname(String mjcategoryname) {
            this.mjcategoryname = mjcategoryname;
        }

        public String getDataurl() {
            return dataurl;
        }

        public void setDataurl(String dataurl) {
            this.dataurl = dataurl;
        }

        public String getPdffile() {
            return pdffile;
        }

        public void setPdffile(String pdffile) {
            this.pdffile = pdffile;
        }

        public String getCandidateid() {
            return candidateid;
        }

        public void setCandidateid(String candidateid) {
            this.candidateid = candidateid;
        }

        public String getUpdated() {
            return updated;
        }

        public void setUpdated(String updated) {
            this.updated = updated;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getMjstatus() {
            return mjstatus;
        }

        public void setMjstatus(String mjstatus) {
            this.mjstatus = mjstatus;
        }

        public String getMjdataid() {
            return mjdataid;
        }

        public void setMjdataid(String mjdataid) {
            this.mjdataid = mjdataid;
        }

        public String getMjurl() {
            return mjurl;
        }

        public void setMjurl(String mjurl) {
            this.mjurl = mjurl;
        }
    }


}