package com.pas.socialapp.models;

import java.util.ArrayList;

/**
 * Created by Umesh  on 24/02/16.
 */
public class ServiceCategoryResponse extends GenericRequest {

    public String status;
    public String service;
    public String message;
    public ArrayList<Servicetype> servicetype = new ArrayList<Servicetype>();

    /**
     * @return The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return The service
     */
    public String getService() {
        return service;
    }

    /**
     * @param service The service
     */
    public void setService(String service) {
        this.service = service;
    }

    /**
     * @return The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return The servicetype
     */
    public ArrayList<Servicetype> getServicetype() {
        return servicetype;
    }

    /**
     * @param servicetype The servicetype
     */
    public void setServicetype(ArrayList<Servicetype> servicetype) {
        this.servicetype = servicetype;
    }


    public class Servicetype {

        private String servicecategoryid;
        private String category;
        private String servicename;
        private String serviceheading;
        private String servicedescription;
        private String servicecontactperson;
        private String serviceaddress;
        private String servicedesignation;
        private String serviceurl;
        private String servicephone;
        private String serviceimage;
        private String requireddocs;
        private String candidateid;
        private String created;
        private String updated;
        private String servicestatus;

        /**
         * @return The servicecategoryid
         */
        public String getServicecategoryid() {
            return servicecategoryid;
        }

        /**
         * @param servicecategoryid The servicecategoryid
         */
        public void setServicecategoryid(String servicecategoryid) {
            this.servicecategoryid = servicecategoryid;
        }

        /**
         * @return The category
         */
        public String getCategory() {
            return category;
        }

        /**
         * @param category The category
         */
        public void setCategory(String category) {
            this.category = category;
        }

        /**
         * @return The servicename
         */
        public String getServicename() {
            return servicename;
        }

        /**
         * @param servicename The servicename
         */
        public void setServicename(String servicename) {
            this.servicename = servicename;
        }

        /**
         * @return The serviceheading
         */
        public String getServiceheading() {
            return serviceheading;
        }

        /**
         * @param serviceheading The serviceheading
         */
        public void setServiceheading(String serviceheading) {
            this.serviceheading = serviceheading;
        }

        /**
         * @return The servicedescription
         */
        public String getServicedescription() {
            return servicedescription;
        }

        /**
         * @param servicedescription The servicedescription
         */
        public void setServicedescription(String servicedescription) {
            this.servicedescription = servicedescription;
        }

        /**
         * @return The servicecontactperson
         */
        public String getServicecontactperson() {
            return servicecontactperson;
        }

        /**
         * @param servicecontactperson The servicecontactperson
         */
        public void setServicecontactperson(String servicecontactperson) {
            this.servicecontactperson = servicecontactperson;
        }

        /**
         * @return The serviceaddress
         */
        public String getServiceaddress() {
            return serviceaddress;
        }

        /**
         * @param serviceaddress The serviceaddress
         */
        public void setServiceaddress(String serviceaddress) {
            this.serviceaddress = serviceaddress;
        }

        /**
         * @return The servicedesignation
         */
        public String getServicedesignation() {
            return servicedesignation;
        }

        /**
         * @param servicedesignation The servicedesignation
         */
        public void setServicedesignation(String servicedesignation) {
            this.servicedesignation = servicedesignation;
        }

        /**
         * @return The serviceurl
         */
        public String getServiceurl() {
            return serviceurl;
        }

        /**
         * @param serviceurl The serviceurl
         */
        public void setServiceurl(String serviceurl) {
            this.serviceurl = serviceurl;
        }

        /**
         * @return The servicephone
         */
        public String getServicephone() {
            return servicephone;
        }

        /**
         * @param servicephone The servicephone
         */
        public void setServicephone(String servicephone) {
            this.servicephone = servicephone;
        }

        /**
         * @return The serviceimage
         */
        public String getServiceimage() {
            return serviceimage;
        }

        /**
         * @param serviceimage The serviceimage
         */
        public void setServiceimage(String serviceimage) {
            this.serviceimage = serviceimage;
        }

        /**
         * @return The requireddocs
         */
        public String getRequireddocs() {
            return requireddocs;
        }

        /**
         * @param requireddocs The requireddocs
         */
        public void setRequireddocs(String requireddocs) {
            this.requireddocs = requireddocs;
        }

        /**
         * @return The candidateid
         */
        public String getCandidateid() {
            return candidateid;
        }

        /**
         * @param candidateid The candidateid
         */
        public void setCandidateid(String candidateid) {
            this.candidateid = candidateid;
        }

        /**
         * @return The created
         */
        public String getCreated() {
            return created;
        }

        /**
         * @param created The created
         */
        public void setCreated(String created) {
            this.created = created;
        }

        /**
         * @return The updated
         */
        public String getUpdated() {
            return updated;
        }

        /**
         * @param updated The updated
         */
        public void setUpdated(String updated) {
            this.updated = updated;
        }

        /**
         * @return The servicestatus
         */
        public String getServicestatus() {
            return servicestatus;
        }

        /**
         * @param servicestatus The servicestatus
         */
        public void setServicestatus(String servicestatus) {
            this.servicestatus = servicestatus;
        }

    }
}