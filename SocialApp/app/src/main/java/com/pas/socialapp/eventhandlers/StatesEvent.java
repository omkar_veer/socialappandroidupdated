package com.pas.socialapp.eventhandlers;

import com.pas.socialapp.models.StateResponse;

/**
 * Created by Umesh on 24/02/16.
 */
public class StatesEvent {

    public StateResponse stateResponse;

    public StatesEvent(StateResponse stateresponse) {
        this.stateResponse = stateresponse;
    }

}
