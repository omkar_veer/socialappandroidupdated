package com.pas.socialapp.models;

import java.util.ArrayList;

/**
 * Created by Umesh on 08/03/16.
 */
public class JahirnamaResponse extends GenericResponse {


    public String status;
    public String service;
    public String message;
    public ArrayList<Jahirnama> jahirnama = new ArrayList<Jahirnama>();

    /**
     * @return The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return The service
     */
    public String getService() {
        return service;
    }

    /**
     * @param service The service
     */
    public void setService(String service) {
        this.service = service;
    }

    /**
     * @return The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return The jahirnama
     */
    public ArrayList<Jahirnama> getJahirnama() {
        return jahirnama;
    }

    /**
     * @param jahirnama The jahirnama
     */
    public void setJahirnama(ArrayList<Jahirnama> jahirnama) {
        this.jahirnama = jahirnama;
    }


    public class Jahirnama {

        private String CID;
        private String type;
        private String description;
        private String image;

        /**
         * @return The CID
         */
        public String getCID() {
            return CID;
        }

        /**
         * @param CID The CID
         */
        public void setCID(String CID) {
            this.CID = CID;
        }

        /**
         * @return The type
         */
        public String getType() {
            return type;
        }

        /**
         * @param type The type
         */
        public void setType(String type) {
            this.type = type;
        }

        /**
         * @return The description
         */
        public String getDescription() {
            return description;
        }

        /**
         * @param description The description
         */
        public void setDescription(String description) {
            this.description = description;
        }

        /**
         * @return The image
         */
        public String getImage() {
            return image;
        }

        /**
         * @param image The image
         */
        public void setImage(String image) {
            this.image = image;
        }

    }
}