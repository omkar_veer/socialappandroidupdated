package com.pas.socialapp.views.adapters;


import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Paint;
import android.net.Uri;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.UnderlineSpan;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.pas.socialapp.R;
import com.pas.socialapp.models.HelpingHandCategoryResponse;
import com.pas.socialapp.utils.Utility;
import com.pas.socialapp.views.custom.CustomTextView;

import java.util.ArrayList;

public class HelpingHandSubCategoryListAdapter extends RecyclerView
        .Adapter<HelpingHandSubCategoryListAdapter
        .DataObjectHolder> {
    private static final String LOG_TAG = HelpingHandSubCategoryListAdapter.class.getSimpleName();
    private ArrayList<HelpingHandCategoryResponse.Hhdatum> mDataset;
    private Context context;

    private OpenURLListener openURLListener;

    public HelpingHandSubCategoryListAdapter(ArrayList<HelpingHandCategoryResponse.Hhdatum> myDataset, Context cont) {
        mDataset = myDataset;
        this.context = cont;
    }


    @Override
    public HelpingHandSubCategoryListAdapter.DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.hh_sub_category_list_items, parent, false);

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(HelpingHandSubCategoryListAdapter.DataObjectHolder holder, final int position) {

        String data = "";

        if (!TextUtils.isEmpty(mDataset.get(position).getHhcategoryname())) {
            //holder.hhCategoryName.setText(mDataset.get(position).getHhcategoryname());
            data += context.getString(R.string.lable_helping_hand) +  " : \n"+ mDataset.get(position).getHhurl() + "\n";
            SpannableString content = new SpannableString(mDataset.get(position).getHhcategoryname());
            content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
            holder.hhCategoryName.setText(content);
            holder.hhCategoryName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String browserURL= mDataset.get(position).getHhurl();
                    if(!browserURL.contains("http")){
                        browserURL= "http://"+browserURL;
                    }
                    Uri uri = Uri.parse(browserURL); // missing 'http://' will cause crashed
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    context.startActivity(intent);
                }
            });
            holder.shareFab.setTag(data);
            //holder.shareFab.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(context,R.color.colorPrimary)));
            holder.shareFab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String shareData = (String)view.getTag();
                    if (!TextUtils.isEmpty(shareData))
                        Utility.shareData(context, shareData);
                    else
                        Utility.displayToast(context, context.getString(R.string.msg_service_call_error));
                }
            });
        }

       /* if (!TextUtils.isEmpty(mDataset.get(position).getHhurl())) {
            holder.hhCategoryURL.setText(mDataset.get(position).getHhurl());
            Linkify.addLinks(holder.hhCategoryURL, Linkify.WEB_URLS);
        } else {
          //  holder.hhUrlLL.setVisibility(View.GONE);
        }*/
    }


    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class DataObjectHolder extends RecyclerView.ViewHolder {
        private CustomTextView hhCategoryName;
        private CustomTextView hhCategoryURL;
        private LinearLayout hhTitleLL;
        private LinearLayout hhUrlLL;
        ImageView shareFab;

        public DataObjectHolder(View itemView) {
            super(itemView);
            hhCategoryName = (CustomTextView) itemView.findViewById(R.id.hhCategoryValue);
            hhCategoryURL = (CustomTextView) itemView.findViewById(R.id.hhUrl);
            shareFab = (ImageView) itemView.findViewById(R.id.hhShareFab);
          //  hhTitleLL = (LinearLayout) itemView.findViewById(R.id.hhTitleLL);
          //  hhUrlLL = (LinearLayout) itemView.findViewById(R.id.hhUrlLL);
        }
    }

    public interface OpenURLListener {
        public void openURLinWEbView(String url);
    }

    public void openURLServiceListener(OpenURLListener openURLListener) {
        this.openURLListener = openURLListener;
    }
}