package com.pas.socialapp.models;

import java.util.ArrayList;

/**
 * Created by Umesh on 08/03/16.
 */
public class PartyProfileResponse extends GenericResponse {

    public String status;
    public String service;
    public String message;
    public ArrayList<Partyprofile> partyprofile = new ArrayList<Partyprofile>();

    /**
     * @return The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return The service
     */
    public String getService() {
        return service;
    }

    /**
     * @param service The service
     */
    public void setService(String service) {
        this.service = service;
    }

    /**
     * @return The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return The partyprofile
     */
    public ArrayList<Partyprofile> getPartyprofile() {
        return partyprofile;
    }

    /**
     * @param partyprofile The partyprofile
     */
    public void setPartyprofile(ArrayList<Partyprofile> partyprofile) {
        this.partyprofile = partyprofile;
    }


    public class Partyprofile {

        private String partyname;
        private String partyimage;
        private String partydetails;
        private String partystatus;
        private String otherpartyimage;

        public String getOtherpartyimage() {
            return otherpartyimage;
        }

        public void setOtherpartyimage(String otherpartyimage) {
            this.otherpartyimage = otherpartyimage;
        }

        /**
         * @return The partyname
         */
        public String getPartyname() {
            return partyname;
        }

        /**
         * @param partyname The partyname
         */
        public void setPartyname(String partyname) {
            this.partyname = partyname;
        }

        /**
         * @return The partyimage
         */
        public String getPartyimage() {
            return partyimage;
        }

        /**
         * @param partyimage The partyimage
         */
        public void setPartyimage(String partyimage) {
            this.partyimage = partyimage;
        }

        /**
         * @return The partydetails
         */
        public String getPartydetails() {
            return partydetails;
        }

        /**
         * @param partydetails The partydetails
         */
        public void setPartydetails(String partydetails) {
            this.partydetails = partydetails;
        }

        /**
         * @return The partystatus
         */
        public String getPartystatus() {
            return partystatus;
        }

        /**
         * @param partystatus The partystatus
         */
        public void setPartystatus(String partystatus) {
            this.partystatus = partystatus;
        }
    }
}
