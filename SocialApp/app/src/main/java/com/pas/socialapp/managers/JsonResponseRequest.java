package com.pas.socialapp.managers;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.pas.socialapp.models.OTPVerifyResponse;
import com.pas.socialapp.models.RegistrationResponse;
import com.pas.socialapp.models.ResendOTPVerifyResponse;

import java.io.UnsupportedEncodingException;

public class JsonResponseRequest<T, S> extends Request<T> {

    public static final String API_TAG = "Social_App_API";
    public static final int INITIAL_TIMEOUT_MS = 10000;
    private static final String PROTOCOL_CHARSET = "utf-8";
    private static final String PROTOCOL_CONTENT_TYPE = String.format("application/json; charset=%s", PROTOCOL_CHARSET);
    private final S mRequest;
    private Class<T> mResponseClass;
    private Listener<T> mListener;
    private Gson mGson;


    public JsonResponseRequest(String url, Class<T> responseClass, S request,
                               Listener<T> listener, ErrorListener errorListener) {
        super(Method.POST, url, errorListener);
        setRetryPolicy(getDefaultRetryPolicy());
        mGson = new Gson();
        mRequest = request;
        mResponseClass = responseClass;
        mListener = listener;
    }

    public JsonResponseRequest(String url, Class<T> responseClass,
                               Listener<T> listener, ErrorListener errorListener) {
        super(Method.GET, url, errorListener);
        setRetryPolicy(getDefaultRetryPolicy());
        mGson = new Gson();
        mResponseClass = responseClass;
        mListener = listener;
        mRequest = null;
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse networkResponse) {
        try {
            String jsonString = new String(networkResponse.data, HttpHeaderParser.parseCharset(networkResponse.headers));
            // Log.d(API_TAG, "API Response - " + jsonString);
            T response;

            if (mResponseClass.getName().equalsIgnoreCase(RegistrationResponse.class.getName())) {
                // jsonString = "{ myPacts : " + jsonString + "}";

                int startIndex = jsonString.indexOf("{");
                jsonString = jsonString.substring(startIndex, jsonString.length());

            }
            if (mResponseClass.getName().equalsIgnoreCase(OTPVerifyResponse.class.getName())) {
                // jsonString = "{ myPacts : " + jsonString + "}";

                int startIndex = jsonString.indexOf("{");
                jsonString = jsonString.substring(startIndex, jsonString.length());

            }

            if (mResponseClass.getName().equalsIgnoreCase(ResendOTPVerifyResponse.class.getName())) {
                 jsonString = jsonString + "{\"status\":\"OK\",\"service\":\"Get Candidate Details by Tab Identifier\",\"message\":\"OTP Resend Successfully\"}";

                int startIndex = jsonString.indexOf("{");
                jsonString = jsonString.substring(startIndex, jsonString.length());

            }

            response = mGson.fromJson(jsonString, mResponseClass);
            return Response.success(response,
                    HttpHeaderParser.parseCacheHeaders(networkResponse));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return Response.error(new ParseError(e));
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
            return Response.error(new ParseError(e));
        }
    }

    @Override
    protected void deliverResponse(T response) {
        mListener.onResponse(response);
    }

    @Override
    public String getBodyContentType() {
        return PROTOCOL_CONTENT_TYPE;
    }

    @Override
    public byte[] getBody() {
        try {
            if (mRequest != null) {
                String request = mGson.toJson(mRequest);
                // Log.d(API_TAG, "API Request - " + getOriginUrl() + request);
                return request.getBytes(PROTOCOL_CHARSET);
            } else {
                return super.getBody();
            }
            //return mRequest == null ? null : mGson.toJson(mRequest).getBytes(PROTOCOL_CHARSET);
        } catch (UnsupportedEncodingException uee) {
            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s",
                    mRequest, PROTOCOL_CHARSET);
            return null;
        } catch (AuthFailureError authFailureError) {
            authFailureError.printStackTrace();
            return null;
        }
    }

    private DefaultRetryPolicy getDefaultRetryPolicy() {
        return new DefaultRetryPolicy(INITIAL_TIMEOUT_MS, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

    }
}
