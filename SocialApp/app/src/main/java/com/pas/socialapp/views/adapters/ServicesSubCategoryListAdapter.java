package com.pas.socialapp.views.adapters;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.Toast;

import com.pas.socialapp.R;
import com.pas.socialapp.models.ServiceCategoryResponse;
import com.pas.socialapp.views.activities.MainActivity;
import com.pas.socialapp.views.custom.CustomTextView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ServicesSubCategoryListAdapter extends RecyclerView
        .Adapter<ServicesSubCategoryListAdapter
        .DataObjectHolder> {
    private static final String LOG_TAG = ServicesSubCategoryListAdapter.class.getSimpleName();
    private ArrayList<ServiceCategoryResponse.Servicetype> mDataset;
    private Context context;

    private OpenURLListener openURLListener;

    public ServicesSubCategoryListAdapter(ArrayList<ServiceCategoryResponse.Servicetype> myDataset, Context cont) {
        mDataset = myDataset;
        this.context = cont;
    }


    @Override
    public ServicesSubCategoryListAdapter.DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.service_sub_category_list_items, parent, false);

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(ServicesSubCategoryListAdapter.DataObjectHolder holder, final int position) {

        if (!TextUtils.isEmpty(mDataset.get(position).getServicename())) {
            Picasso.with(context).load(mDataset.get(position).getServiceimage()).error(R.drawable.app_icon).placeholder(R.drawable.app_icon).into(holder.serviceIcon);
            //holder.requestTitle.setText(mDataset.get(position));
            if (!mDataset.get(position).getCategory().equalsIgnoreCase("Medical Assistance Services")) {
                if (mDataset.get(position).getServicestatus().equalsIgnoreCase("1")) {

                    holder.serviceRowTL.setTag(position);

                    holder.service.setText(mDataset.get(position).getServicename());
                    if (!TextUtils.isEmpty(mDataset.get(position).getServicephone())) {
                        holder.serviceurlLL.setVisibility(View.GONE);
                        holder.contactnumber.setText(mDataset.get(position).getServicephone());
                        holder.contactperson.setText(mDataset.get(position).getServicecontactperson());
                        holder.designation.setText(mDataset.get(position).getServicedesignation());
                        holder.address.setText(mDataset.get(position).getServiceaddress());
                        Linkify.addLinks(holder.contactnumber, Linkify.PHONE_NUMBERS);


                    } else if(!TextUtils.isEmpty(mDataset.get(position).getServiceurl())){
                        holder.contactnumberLL.setVisibility(View.GONE);
                        holder.contactpersonLL.setVisibility(View.GONE);
                        holder.designationLL.setVisibility(View.GONE);
                        holder.addressLL.setVisibility(View.GONE);
                        holder.serviceurlLL.setVisibility(View.GONE);
                        holder.serviceurl.setText(mDataset.get(position).getServiceurl());
                        Linkify.addLinks(holder.serviceurl, Linkify.WEB_URLS);
                    }
                    else {
                        holder.contactnumberLL.setVisibility(View.GONE);
                        holder.contactpersonLL.setVisibility(View.GONE);
                        holder.designationLL.setVisibility(View.GONE);
                        holder.addressLL.setVisibility(View.GONE);
                        holder.serviceurlLL.setVisibility(View.VISIBLE);
                        holder.serviceurl.setText(mDataset.get(position).getServiceurl());
                        Linkify.addLinks(holder.serviceurl, Linkify.WEB_URLS);
                    }
                } else {
                    holder.serviceurlLL.setVisibility(View.GONE);
                    holder.contactnumberLL.setVisibility(View.GONE);
                    holder.contactpersonLL.setVisibility(View.GONE);
                    holder.designationLL.setVisibility(View.GONE);
                    holder.addressLL.setVisibility(View.GONE);
                    holder.service.setVisibility(View.GONE);
                    holder.bannerBottom.setVisibility(View.GONE);
                    holder.serviceTitleLL.setVisibility(View.GONE);
                }

                holder.serviceRowTL.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        detailDialogView((int) v.getTag());
                    }
                });

            } else {
                holder.serviceurlLL.setVisibility(View.GONE);
                holder.contactnumberLL.setVisibility(View.GONE);
                holder.contactpersonLL.setVisibility(View.GONE);
                holder.designationLL.setVisibility(View.GONE);
                holder.addressLL.setVisibility(View.GONE);

                if (mDataset.get(position).getServicestatus().equalsIgnoreCase("1")) {
                    if (!TextUtils.isEmpty(mDataset.get(position).getServicename())) {
                        holder.service.setPadding(0, 10, 0, 10);
                        holder.service.setTextSize(18);
                        holder.service.setText(mDataset.get(position).getServicename());
                        holder.serviceRowTL.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (mDataset.get(position).getServicename().toLowerCase().contains("blood"))
                                    AlertDialogView();
                                else
                                    Toast.makeText(context, context.getString(R.string.no_data_available), Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                } else {
                    holder.service.setVisibility(View.GONE);
                    holder.bannerBottom.setVisibility(View.GONE);
                    holder.serviceTitleLL.setVisibility(View.GONE);
                }
            }

        }
    }

    private void detailDialogView(int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle((mDataset.get(position).getServicename()));
        builder.setIcon(R.drawable.app_icon);
        View view = ((MainActivity) context).getLayoutInflater().inflate(R.layout.dialog_service_details, null);

        CustomTextView contactperson = (CustomTextView) view.findViewById(R.id.contactperson_value);
        CustomTextView designation = (CustomTextView) view.findViewById(R.id.designation_value);
        CustomTextView contactnumber = (CustomTextView) view.findViewById(R.id.contactnumber_value);
        CustomTextView address = (CustomTextView) view.findViewById(R.id.address_value);
        CustomTextView serviceurl = (CustomTextView) view.findViewById(R.id.serviceurl_value);
        TableRow designationLL = (TableRow) view.findViewById(R.id.designationLL);
        TableRow addressLL = (TableRow) view.findViewById(R.id.addressLL);
        TableRow serviceurlLL = (TableRow) view.findViewById(R.id.serviceurlLL);
        TableRow contactnumberLL = (TableRow) view.findViewById(R.id.contactnumberLL);
        TableRow contactpersonLL = (TableRow) view.findViewById(R.id.contactpersonLL);

        if (!TextUtils.isEmpty(mDataset.get(position).getServicephone())) {
            serviceurlLL.setVisibility(View.GONE);
            contactnumber.setText(mDataset.get(position).getServicephone());
            contactperson.setText(mDataset.get(position).getServicecontactperson());
            designation.setText(mDataset.get(position).getServicedesignation());
            address.setText(mDataset.get(position).getServiceaddress());
            Linkify.addLinks(contactnumber, Linkify.PHONE_NUMBERS);
        } else {
            contactnumberLL.setVisibility(View.GONE);
            contactpersonLL.setVisibility(View.GONE);
            designationLL.setVisibility(View.GONE);
            addressLL.setVisibility(View.GONE);
            serviceurlLL.setVisibility(View.VISIBLE);
            serviceurl.setText(mDataset.get(position).getServiceurl());
            Linkify.addLinks(serviceurl, Linkify.WEB_URLS);
        }

        builder.setView(view);
        builder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }


    private void AlertDialogView() {
        final CharSequence[] items = context.getResources().getStringArray(R.array.bloodgrp_array);

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Send Alert to Nagari Suvidha for Blood Group requirment -");
        builder.setIcon(R.drawable.app_icon);
        builder.setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                //Toast.makeText(context, "Selected blood group " + items[item], Toast.LENGTH_SHORT).show();
            }
        });

        builder.setPositiveButton("Send",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Toast.makeText(context, "Send", Toast.LENGTH_SHORT).show();
                    }
                });
        builder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }


    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class DataObjectHolder extends RecyclerView.ViewHolder {
        private CustomTextView service;
        private CustomTextView contactperson;
        private CustomTextView designation;
        private CustomTextView contactnumber;
        private CustomTextView address;
        private CustomTextView serviceurl;
        private ImageView serviceIcon;
//        private ImageView actioncallImageView;
//        private ImageView actionWebImageView;

        private TableRow contactpersonLL;
        private TableRow designationLL;
        private TableRow contactnumberLL;
        private TableRow addressLL;
        private TableRow serviceurlLL;
        private View bannerBottom;
        private TableRow serviceTitleLL;
        private CustomTextView serviceTitle;
        private View serviceRowTL;

        public DataObjectHolder(View itemView) {
            super(itemView);
            serviceIcon = (ImageView) itemView.findViewById(R.id.serviceIcon);
            serviceTitle = (CustomTextView) itemView.findViewById(R.id.serviceTitle);
            service = (CustomTextView) itemView.findViewById(R.id.service_value);
            contactperson = (CustomTextView) itemView.findViewById(R.id.contactperson_value);
            designation = (CustomTextView) itemView.findViewById(R.id.designation_value);
            contactnumber = (CustomTextView) itemView.findViewById(R.id.contactnumber_value);
            address = (CustomTextView) itemView.findViewById(R.id.address_value);
            serviceurl = (CustomTextView) itemView.findViewById(R.id.serviceurl_value);
            designationLL = (TableRow) itemView.findViewById(R.id.designationLL);
            addressLL = (TableRow) itemView.findViewById(R.id.addressLL);
            serviceurlLL = (TableRow) itemView.findViewById(R.id.serviceurlLL);
            contactnumberLL = (TableRow) itemView.findViewById(R.id.contactnumberLL);
            serviceTitleLL = (TableRow) itemView.findViewById(R.id.serviceTitleLL);
            contactpersonLL = (TableRow) itemView.findViewById(R.id.contactpersonLL);
            serviceRowTL = itemView.findViewById(R.id.serviceRowTL);
            bannerBottom = itemView.findViewById(R.id.footerView);
        }
    }

    public interface OpenURLListener {
        public void openURLinWEbView(String url);
    }

    public void openURLServiceListener(OpenURLListener openURLListener) {
        this.openURLListener = openURLListener;
    }
}