package com.pas.socialapp.views.adapters;


import android.content.Context;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.pas.socialapp.R;
import com.pas.socialapp.models.ProfileResponse;
import com.pas.socialapp.views.custom.CustomTextView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ProfileAdapter extends RecyclerView
        .Adapter<ProfileAdapter
        .DataObjectHolder> {
    private static final String LOG_TAG = ProfileAdapter.class.getSimpleName();
    private ArrayList<ProfileResponse.Profile> mDataset;
    private Context context;

    public ProfileAdapter(ArrayList<ProfileResponse.Profile> myDataset, Context cont) {
        mDataset = myDataset;
        this.context = cont;
    }


    @Override
    public ProfileAdapter.DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.profile_list_item, parent, false);

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(ProfileAdapter.DataObjectHolder holder, int position) {
        mDataset.get(position);
        if (!TextUtils.isEmpty(mDataset.get(position).getFirstname())) {
            holder.firstname.setText(mDataset.get(position).getFirstname() + " " +
                    mDataset.get(position).getMiddlename() + " " +
                    mDataset.get(position).getLastname());
        } else {
            holder.firstname.setText("NA");
        }
        if (!TextUtils.isEmpty(mDataset.get(position).getCandidatedescription())) {
            holder.candidateBriefDesc.setText(mDataset.get(position).getCandidatedescription());
        } else {
            holder.candidateBriefDesc.setText("NA");
        }

        if (!TextUtils.isEmpty(mDataset.get(position).getEmail())) {
            holder.email.setText(mDataset.get(position).getEmail());
            holder.email.setPaintFlags( holder.email.getPaintFlags() |   Paint.UNDERLINE_TEXT_FLAG);
        } else {
            holder.email.setText("NA");
        }
        if (!TextUtils.isEmpty(mDataset.get(position).getPhone())) {
            holder.phone.setText(":  " + mDataset.get(position).getPhone());
            Linkify.addLinks(holder.phone, Linkify.PHONE_NUMBERS);
        }
        if (!TextUtils.isEmpty(mDataset.get(position).getLandline())) {
            holder.landline.setText(":  " + mDataset.get(position).getLandline());
        } else {
            holder.landline.setText(":  NA");
        }

        if (!TextUtils.isEmpty(mDataset.get(position).getGender())) {
            holder.gender.setText(":  " + mDataset.get(position).getGender());
        } else {
            holder.gender.setText(":  NA");
        }
        if (!TextUtils.isEmpty(mDataset.get(position).getDob())) {
            holder.dob.setText(":  " + mDataset.get(position).getDob());
        } else {
            holder.dob.setText(":  NA");
        }

        if (!TextUtils.isEmpty(mDataset.get(position).getJahirnamastatus())) {
            holder.jahirnamastatus.setText(":  " + mDataset.get(position).getJahirnamastatus());
        } else {
            holder.jahirnamastatus.setText(":  NA");
        }

        if (!TextUtils.isEmpty(mDataset.get(position).getWorkstatus())) {
            holder.workstatus.setText(":  " + mDataset.get(position).getWorkstatus());
        }

        if (!TextUtils.isEmpty(mDataset.get(position).getLane1())) {
            if (!TextUtils.isEmpty(mDataset.get(position).getLane2())) {
                holder.lane1.setText(mDataset.get(position).getLane1() + ", " +
                        mDataset.get(position).getLane2());
            } else {
                holder.lane1.setText(":  " + mDataset.get(position).getLane1());
            }
        } else {
            holder.lane1.setText("NA");
        }
        if (!TextUtils.isEmpty(mDataset.get(position).getWard())) {
            holder.ward.setText(":  " + mDataset.get(position).getWard());
        } else {
            holder.ward.setText(":  NA");
        }
        if (!TextUtils.isEmpty(mDataset.get(position).getArea())) {
            holder.area.setText(":  " + mDataset.get(position).getArea());
        } else {
            holder.area.setText(":  NA");
        }
        if (!TextUtils.isEmpty(mDataset.get(position).getTaluka())) {
            holder.taluka.setText(":  " + mDataset.get(position).getTaluka());
        } else {
            holder.taluka.setText(":  NA");
        }
        if (!TextUtils.isEmpty(mDataset.get(position).getDistrict())) {
            holder.district.setText(":  " + mDataset.get(position).getDistrict());
        } else {
            holder.district.setText(":  NA");
        }
        if (!TextUtils.isEmpty(mDataset.get(position).getState())) {
            holder.state.setText(":  " + mDataset.get(position).getState());
        } else {
            holder.state.setText(":  NA");
        }
        if (!TextUtils.isEmpty(mDataset.get(position).getPincode())) {
            holder.pincode.setText(":  " + mDataset.get(position).getPincode());
        } else {
            holder.pincode.setText(":  NA");
        }
       /* if (!TextUtils.isEmpty(mDataset.get(position).getAssembly())) {
            holder.assembly.setText(":  " + mDataset.get(position).getAssembly());
        } else {
            holder.assembly.setText(":  NA");
        }
        if (!TextUtils.isEmpty(mDataset.get(position).getLoksabha())) {
            holder.loksabha.setText(":  " + mDataset.get(position).getLoksabha());
        } else {
            holder.loksabha.setText(":  NA");
        }
        if (!TextUtils.isEmpty(mDataset.get(position).getVidhansabha())) {
            holder.vidhansabha.setText(":  " + mDataset.get(position).getVidhansabha());
        } else {
            holder.vidhansabha.setText(":  NA");
        }*/
        if (!TextUtils.isEmpty(mDataset.get(position).getProfilestatus())) {
            holder.profilestatus.setText(":  " + mDataset.get(position).getProfilestatus());
        } else {
            holder.profilestatus.setText(":  NA");
        }
        if (!TextUtils.isEmpty(mDataset.get(position).getPartyname())) {
            holder.partyname.setText(mDataset.get(position).getPartyname());
        } else {
            holder.partyname.setText("NA");
        }
        if (!TextUtils.isEmpty(mDataset.get(position).getPartyid())) {
            holder.partyid.setText(":  " + mDataset.get(position).getPartyid());
        } else {
            holder.partyid.setText(":  NA");
        }
        if (!TextUtils.isEmpty(mDataset.get(position).getPartystatus())) {
            holder.partystatus.setText(":  " + mDataset.get(position).getPartystatus());
        } else {
            holder.partystatus.setText(":  NA");
        }

        if (mDataset.get(position).getImage() != null && !mDataset.get(position).getImage().isEmpty()) {
            Picasso.with(context).load(mDataset.get(position).getImage())
                    .error(R.drawable.banner00)
                    .placeholder(R.drawable.banner00)
                    .fit().centerInside()
                    .into(holder.profileImage);
        } else {
            holder.profileImage.setVisibility(View.GONE);
        }
        if(mDataset.get(position).getPartyname()!=null && mDataset.get(position).getPartyname().equalsIgnoreCase("Others")){
            if (mDataset.get(position).getOtherpartyimage() != null && !mDataset.get(position).getOtherpartyimage().isEmpty()) {
                Picasso.with(context).load(mDataset.get(position).getOtherpartyimage())
                        .error(R.drawable.banner00)
                        .placeholder(R.drawable.banner00)
                        .into(holder.partyImage);
            } else {
                holder.partyImage.setVisibility(View.GONE);
            }
        }else{
            if (mDataset.get(position).getPartyimage() != null && !mDataset.get(position).getPartyimage().isEmpty()) {
                Picasso.with(context).load(mDataset.get(position).getPartyimage())
                        .error(R.drawable.banner00)
                        .placeholder(R.drawable.banner00)
                        .into(holder.partyImage);
            } else {
                holder.partyImage.setVisibility(View.GONE);
            }
        }

    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public static class DataObjectHolder extends RecyclerView.ViewHolder {


        private TextView firstname;
        private TextView email;
        private TextView phone;
        private TextView landline;
        private TextView gender;
        private TextView dob;
        //   private TextView image;
        private TextView jahirnamastatus;
        private TextView workstatus;


        private TextView lane1;
        private TextView ward;
        private TextView area;
        private TextView taluka;
        private TextView district;
        private TextView state;
        private TextView pincode;
        private TextView assembly;
        private TextView loksabha;
        private TextView vidhansabha;
        private TextView profilestatus;
        private TextView partyname;
        private TextView partyid;
        private TextView partystatus;
        private TextView candidateBriefDesc;


        private ImageView profileImage;

        private ImageView partyImage;


        public DataObjectHolder(View itemView) {
            super(itemView);

            firstname = (CustomTextView) itemView.findViewById(R.id.firstname);
            email = (CustomTextView) itemView.findViewById(R.id.email);
            phone = (CustomTextView) itemView.findViewById(R.id.phone);
            landline = (CustomTextView) itemView.findViewById(R.id.landline);
            gender = (CustomTextView) itemView.findViewById(R.id.gender);
            dob = (CustomTextView) itemView.findViewById(R.id.dob);
            jahirnamastatus = (CustomTextView) itemView.findViewById(R.id.jahirnamastatus);
            workstatus = (CustomTextView) itemView.findViewById(R.id.workstatus);
            candidateBriefDesc = (CustomTextView) itemView.findViewById(R.id.candidateBriefDesc);


            lane1 = (CustomTextView) itemView.findViewById(R.id.lane1);
            ward = (CustomTextView) itemView.findViewById(R.id.ward);
            area = (CustomTextView) itemView.findViewById(R.id.area);
            taluka = (CustomTextView) itemView.findViewById(R.id.taluka);
            district = (CustomTextView) itemView.findViewById(R.id.district);
            state = (CustomTextView) itemView.findViewById(R.id.state);
            pincode = (CustomTextView) itemView.findViewById(R.id.pincode);
           /* assembly = (CustomTextView) itemView.findViewById(R.id.assembly);
            loksabha = (CustomTextView) itemView.findViewById(R.id.loksabha);
            vidhansabha = (CustomTextView) itemView.findViewById(R.id.vidhansabha);*/
            profilestatus = (CustomTextView) itemView.findViewById(R.id.profilestatus);
            partyname = (CustomTextView) itemView.findViewById(R.id.partyname);
            partyid = (CustomTextView) itemView.findViewById(R.id.partyid);
            partystatus = (CustomTextView) itemView.findViewById(R.id.partystatus);
            profileImage = (ImageView) itemView.findViewById(R.id.profileImageView);
            partyImage = (ImageView) itemView.findViewById(R.id.partyImage);

        }
    }
}