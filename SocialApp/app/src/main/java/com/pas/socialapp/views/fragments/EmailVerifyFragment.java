package com.pas.socialapp.views.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pas.socialapp.R;
import com.pas.socialapp.views.activities.OnboardingActivity;

// Ganesh : Temporary not using this screen
public class EmailVerifyFragment extends Fragment implements View.OnClickListener {

    private View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_email_verification, container, false);

        rootView.findViewById(R.id.email_verify_resend_button).setOnClickListener(this);
        rootView.findViewById(R.id.email_verify_signin_button).setOnClickListener(this);



        return rootView;
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.email_verify_resend_button:
                break;

            case R.id.email_verify_signin_button:
                ((OnboardingActivity) getActivity()).clearBackstack();
                ((OnboardingActivity) getActivity()).moveToScreen(new LoginFragment());
                break;

            default:
                break;

        }
    }

}