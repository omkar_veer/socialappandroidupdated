package com.pas.socialapp.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Umesh on 24/02/16.
 */
public class StateResponse extends GenericRequest {

    public String status;
    public String service;
    public String message;
    public ArrayList<State> states = new ArrayList<State>();

    /**
     * @return The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return The service
     */
    public String getService() {
        return service;
    }

    /**
     * @param service The service
     */
    public void setService(String service) {
        this.service = service;
    }

    /**
     * @return The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return The states
     */
    public ArrayList<State> getStates() {
        return states;
    }

    /**
     * @param states The states
     */
    public void setStates(ArrayList<State> states) {
        this.states = states;
    }


    public class State {

        public String state;

        /**
         * @return The state
         */
        public String getState() {
            return state;
        }

        /**
         * @param state The state
         */
        public void setState(String state) {
            this.state = state;
        }

    }

}
