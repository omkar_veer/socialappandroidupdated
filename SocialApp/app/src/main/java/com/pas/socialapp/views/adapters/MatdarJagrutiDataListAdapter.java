package com.pas.socialapp.views.adapters;


import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.net.Uri;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.UnderlineSpan;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pas.socialapp.R;
import com.pas.socialapp.models.HelpingHandCategoryResponse;
import com.pas.socialapp.models.MatdarJagrutiResponse;
import com.pas.socialapp.utils.Utility;
import com.pas.socialapp.views.custom.CustomTextView;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MatdarJagrutiDataListAdapter extends RecyclerView
        .Adapter<MatdarJagrutiDataListAdapter
        .DataObjectHolder> {
    private static final String LOG_TAG = MatdarJagrutiDataListAdapter.class.getSimpleName();
    private ArrayList<MatdarJagrutiResponse.MatdarJagruti> mDataset;
    private Context context;

    private OpenURLListener openURLListener;

    public MatdarJagrutiDataListAdapter(ArrayList<MatdarJagrutiResponse.MatdarJagruti> myDataset, Context cont) {
        mDataset = myDataset;
        this.context = cont;
    }


    @Override
    public MatdarJagrutiDataListAdapter.DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.matdar_jagruti_data_list_items, parent, false);

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(MatdarJagrutiDataListAdapter.DataObjectHolder holder, final int position) {

        String data = "";

        if (!TextUtils.isEmpty(mDataset.get(position).getMjcategoryname())) {
           // holder.mjCategoryName.setText(mDataset.get(position).getMjcategoryname());
           // addLink(holder.mjCategoryName, "^Android", mDataset.get(position).getMjurl());
            data += context.getString(R.string.lable_matdar_jagruti) +  " : \n"+ mDataset.get(position).getMjurl() + "\n";
            SpannableString content = new SpannableString(mDataset.get(position).getMjcategoryname());
            content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
            holder.mjCategoryName.setText(content);
            holder.mjCategoryName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String browserURL= mDataset.get(position).getMjurl();
                    if(!browserURL.contains("http")){
                        browserURL= "http://"+browserURL;
                    }
                    Uri uri = Uri.parse(browserURL); // missing 'http://' will cause crashed
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    context.startActivity(intent);
                }
            });
            holder.shareFab.setTag(data);
        //    holder.shareFab.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(context,R.color.colorPrimary)));
            holder.shareFab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String shareData = (String)view.getTag();
                    if (!TextUtils.isEmpty(shareData))
                        Utility.shareData(context, shareData);
                    else
                        Utility.displayToast(context, context.getString(R.string.msg_service_call_error));
                }
            });
        }

      /*  if (!TextUtils.isEmpty(mDataset.get(position).getMjurl())) {
           // holder.mjCategoryURL.setText(mDataset.get(position).getMjurl());
          //  Linkify.addLinks(holder.mjCategoryURL, Linkify.WEB_URLS);
        }*/

    }

   /* public static void addLink(TextView textView, String patternToMatch,
                               final String link) {
        Linkify.TransformFilter filter = new Linkify.TransformFilter() {
            @Override public String transformUrl(Matcher match, String url) {
                return link;
            }
        };
        Linkify.addLinks(textView, Pattern.compile(patternToMatch), null, null,
                filter);
    }*/


    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class DataObjectHolder extends RecyclerView.ViewHolder {
        private CustomTextView mjCategoryName;
        private CustomTextView mjCategoryURL;
        ImageView shareFab;

        public DataObjectHolder(View itemView) {
            super(itemView);
            mjCategoryName = (CustomTextView) itemView.findViewById(R.id.mjCategoryValue);
            mjCategoryURL = (CustomTextView) itemView.findViewById(R.id.mjUrl);
            shareFab = (ImageView) itemView.findViewById(R.id.mjShareFab);

        }
    }

    public interface OpenURLListener {
        public void openURLinWEbView(String url);
    }

    public void openURLServiceListener(OpenURLListener openURLListener) {
        this.openURLListener = openURLListener;
    }
}