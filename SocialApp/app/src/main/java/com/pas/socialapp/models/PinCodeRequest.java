package com.pas.socialapp.models;

/**
 * Created by Umesh on 24/02/16.
 */
public class PinCodeRequest extends GenericRequest {

    public PinCodeRequest(String taluka) {
        this.taluka = taluka;
    }

    public String taluka;
}
