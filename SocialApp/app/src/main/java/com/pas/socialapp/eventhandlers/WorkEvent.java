package com.pas.socialapp.eventhandlers;

import com.pas.socialapp.models.WorkResponse;

/**
 * Created by ganesh on 5/1/16.
 */
public class WorkEvent {

    public WorkResponse workResponse;

    public WorkEvent(WorkResponse workResponse) {
        this.workResponse = workResponse;
    }

}
