package com.pas.socialapp.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import android.content.Context;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.pas.socialapp.Constants;


public class CommonUtils
{

	/**
	 * @param ctx
	 */
	public static void setApplicationLocale(Context ctx)
	{
		// TODO

	}

	/**
	 * @param s
	 * @param c
	 * @return String
	 */
	public static String removeChar(String s, char c)
	{
		StringBuffer r = new StringBuffer(s.length());
		r.setLength(s.length());
		int current = 0;
		for (int i = 0; i < s.length(); i++)
		{
			char cur = s.charAt(i);
			if (cur != c)
				r.setCharAt(current++, cur);
		}
		return r.toString().trim();
	}

	// This function is used to get the info about the device type.
	// Is it Phone, 7 inch Tab, 10 inch Tab ? ?
	public static String getDeviceType(Context context)
	{

		String strDeviceType = null;

		if ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == 0x00000003)
		{
			strDeviceType = Constants.TABLET_7_INCH;
		}
		else if ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == 0x00000004)
		{
			strDeviceType = Constants.TABLET_10_INCH;
		}
		else if ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == 0x00000002)
		{
			strDeviceType = Constants.PHONE;
		}
		else
			strDeviceType = null;


		return strDeviceType;

	}

	/**
	 * This method is used to check the network availability on device
	 * 
	 * @param ctx
	 * @return boolean true if network available otherwise false
	 */
	public static boolean isNetworkConnAvailable(Context ctx)
	{
		ConnectivityManager conMgr = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);

		boolean returnVal = true;

		if (conMgr != null)
		{
			NetworkInfo networkInfo = conMgr.getActiveNetworkInfo();
			if (networkInfo != null)
			{
				if (!networkInfo.isConnected())
					returnVal = false;
				if (!networkInfo.isAvailable())
					returnVal = false;
			}
			else
			{
				returnVal = false;
			}
		}
		else
			returnVal = true;

		return returnVal;
	}

	public static String md5(final String s)
	{
		try
		{
			// Create MD5 Hash
			MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
			digest.update(s.getBytes());
			byte messageDigest[] = digest.digest();

			// Create Hex String
			StringBuffer hexString = new StringBuffer();
			for (int i = 0; i < messageDigest.length; i++)
				hexString.append(Integer.toHexString(0xFF & messageDigest[i]));
			return hexString.toString();

		}
		catch (NoSuchAlgorithmException e)
		{
			e.printStackTrace();
		}
		return "";
	}

	public static String populateDatafromStaticFile(final int filename, final Context context)
	{

		final InputStream is = context.getResources().openRawResource(filename);
		final BufferedReader br = new BufferedReader(new InputStreamReader(is));
		final StringBuilder sb = new StringBuilder();
		String readLine = null;
		try
		{
			while ((readLine = br.readLine()) != null)
			{
				sb.append(readLine);
			}
		}
		catch (IOException e)
		{
			//
		}
		return sb.toString();
	}

	public static String getJsonStringFromObject(Object obj)
	{
		/*final Gson gson = new Gson();
		final String json = gson.toJson(obj);
		return json;*/
		
		return null; //TODO
	}

	public static boolean isPointInCircle(double lat1, double lng1, double lat2, double lng2, double radious)
	{
		final double distance = distFrom(lat1, lng1, lat2, lng2);
		return (distance <= radious);
	}

	public static boolean isInRectangle(double centerX, double centerY, double radius, double x, double y)
	{
		return x >= centerX - radius && x <= centerX + radius && y >= centerY - radius && y <= centerY + radius;
	}

	public static double distFrom(double lat1, double lng1, double lat2, double lng2)
	{
		double earthRadius = 6371; // kilometers
		double dLat = Math.toRadians(lat2 - lat1);
		double dLng = Math.toRadians(lng2 - lng1);
		double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) * Math.sin(dLng / 2)
				* Math.sin(dLng / 2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		double dist = (earthRadius * c);

		return dist;
	}

    public static String parseFileToString(Context context, int filename)
    {
        try
        {
            InputStream stream = context.getResources().openRawResource(filename);
            // InputStream stream =
            // context.getResources().openRawResource(R.raw.lockeduser);
            int size = stream.available();

            byte[] bytes = new byte[size];
            stream.read(bytes);
            stream.close();

            return new String(bytes);

        }
        catch (IOException e)
        {
            VULogger.d("IOException: " + e.getMessage());
        }
        return null;
    }
}
