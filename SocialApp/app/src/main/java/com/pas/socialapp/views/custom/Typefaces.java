package com.pas.socialapp.views.custom;

import android.content.Context;
import android.graphics.Typeface;

import java.util.Hashtable;

/**
 * Created by Umesh on 8/4/2015.
 */


/**
 * The Class Typefaces.
 */
public class Typefaces {

    /**
     * The Constant cache.
     */
    private static Hashtable<String, Typeface> cache = new Hashtable<String, Typeface>();

    /**
     * Gets the.
     *
     * @param context   the context
     * @param assetPath the asset path
     * @return the typeface
     */
    public static Typeface get(Context context, String assetPath) {
        synchronized (cache) {
            if (!cache.containsKey(assetPath)) {
                try {
                    Typeface typeface = Typeface.createFromAsset(context.getAssets(), assetPath);
                    cache.put(assetPath, typeface);
                } catch (Exception e) {
                    return null;
                }
            }
            return cache.get(assetPath);
        }
    }
}