package com.pas.socialapp.models;

import java.util.ArrayList;

/**
 * Created by Umesh  on 24/02/16.
 */
public class ReligionResponse extends GenericRequest {


    public String status;
    public String service;
    public String message;
    public ArrayList<Religion> religion = new ArrayList<Religion>();

    /**
     * @return The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return The service
     */
    public String getService() {
        return service;
    }

    /**
     * @param service The service
     */
    public void setService(String service) {
        this.service = service;
    }

    /**
     * @return The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return The religion
     */
    public ArrayList<Religion> getReligion() {
        return religion;
    }

    /**
     * @param religion The religion
     */
    public void setReligion(ArrayList<Religion> religion) {
        this.religion = religion;
    }

    public class Religion {

        private String religionid;
        private String religion;

        /**
         * @return The religionid
         */
        public String getReligionid() {
            return religionid;
        }

        /**
         * @param religionid The religionid
         */
        public void setReligionid(String religionid) {
            this.religionid = religionid;
        }

        /**
         * @return The religion
         */
        public String getReligion() {
            return religion;
        }

        /**
         * @param religion The religion
         */
        public void setReligion(String religion) {
            this.religion = religion;
        }

    }
}