package com.pas.socialapp.eventhandlers;

/**
 * Created by ganesh on 5/1/16.
 */
public class GenericErrorEvent {

    public String errorMessage;

    public GenericErrorEvent(String errorMessage) {
        this.errorMessage = errorMessage;
    }

}
