package com.pas.socialapp.views.custom;

import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

import com.pas.socialapp.R;

import java.util.HashMap;
import java.util.Map;

public class CustomEditText extends EditText {


    private static Map<String, Typeface> mTypefaces;

    public CustomEditText(final Context context) {
        this(context, null);
    }

    public CustomEditText(final Context context, final AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CustomEditText(final Context context, final AttributeSet attrs, final int defStyle) {
        super(context, attrs, defStyle);
        if (mTypefaces == null) {
            mTypefaces = new HashMap<String, Typeface>();
        }

        // prevent exception in Android Studio / ADT interface builder
        if (this.isInEditMode()) {
            return;
        }

        final TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.CustomTextView);

        if (array != null) {
            final String typefaceAssetPath = array.getString(
                    R.styleable.CustomTextView_customTypeface);

            if (typefaceAssetPath != null) {
                Typeface typeface = null;

                if (mTypefaces.containsKey(typefaceAssetPath)) {
                    typeface = mTypefaces.get(typefaceAssetPath);
                } else {
                    AssetManager assets = context.getAssets();
                    typeface = Typeface.createFromAsset(assets, typefaceAssetPath);
                    mTypefaces.put(typefaceAssetPath, typeface);
                }

                setTypeface(typeface);
            }
            array.recycle();
        }
    }

}
