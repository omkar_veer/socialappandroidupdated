package com.pas.socialapp.models;

import java.util.ArrayList;

/**
 * Created by Umesh  on 24/02/16.
 */
public class DistrictResponse extends GenericRequest {

    public String status;
    public String service;
    public String message;
    public ArrayList<District> districts = new ArrayList<District>();

    /**
     * @return The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return The service
     */
    public String getService() {
        return service;
    }

    /**
     * @param service The service
     */
    public void setService(String service) {
        this.service = service;
    }

    /**
     * @return The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return The districts
     */
    public ArrayList<District> getDistricts() {
        return districts;
    }

    /**
     * @param districts The districts
     */
    public void setDistricts(ArrayList<District> districts) {
        this.districts = districts;
    }


    public class District {

        private String district;

        /**
         * @return The district
         */
        public String getDistrict() {
            return district;
        }

        /**
         * @param district The district
         */
        public void setDistrict(String district) {
            this.district = district;
        }

    }
}
