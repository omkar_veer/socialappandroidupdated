package com.pas.socialapp.models;

/**
 * Created by ganesh on 5/1/16.
 */
public class SocialNetworkTabRequest extends GenericRequest {


    public String CID;
    public String action;

    public SocialNetworkTabRequest(String CID, String action) {
        this.CID = CID;
        this.action = action;
    }


}
