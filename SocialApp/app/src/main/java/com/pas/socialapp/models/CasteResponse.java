package com.pas.socialapp.models;

        import java.util.ArrayList;

/**
 * Created by Umesh  on 24/02/16.
 */
public class CasteResponse extends GenericRequest {

    public String status;
    public String service;
    public String message;
    public ArrayList<Caste> caste = new ArrayList<Caste>();

    /**
     * @return The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return The service
     */
    public String getService() {
        return service;
    }

    /**
     * @param service The service
     */
    public void setService(String service) {
        this.service = service;
    }

    /**
     * @return The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return The caste
     */
    public ArrayList<Caste> getCaste() {
        return caste;
    }

    /**
     * @param caste The caste
     */
    public void setCaste(ArrayList<Caste> caste) {
        this.caste = caste;
    }


    public class Caste {

        private String casteid;
        private String castename;

        /**
         * @return The casteid
         */
        public String getCasteid() {
            return casteid;
        }

        /**
         * @param casteid The casteid
         */
        public void setCasteid(String casteid) {
            this.casteid = casteid;
        }

        /**
         * @return The castename
         */
        public String getCastename() {
            return castename;
        }

        /**
         * @param castename The castename
         */
        public void setCastename(String castename) {
            this.castename = castename;
        }

    }
}