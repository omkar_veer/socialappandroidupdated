package com.pas.socialapp.eventhandlers;

import com.pas.socialapp.models.HelpingHandCategoryResponse;
import com.pas.socialapp.models.ServiceCategoryResponse;

/**
 * Created by Umesh on 24/02/16.
 */
public class HelpingHandEvent {

    public HelpingHandCategoryResponse helpingHandCategoryResponse;

    public HelpingHandEvent(HelpingHandCategoryResponse helpingHandCategoryResponse) {
        this.helpingHandCategoryResponse = helpingHandCategoryResponse;
    }

}
