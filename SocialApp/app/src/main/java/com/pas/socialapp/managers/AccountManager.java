package com.pas.socialapp.managers;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.pas.socialapp.Constants;
import com.pas.socialapp.R;
import com.pas.socialapp.eventhandlers.CasteEvent;
import com.pas.socialapp.eventhandlers.ComplaintsTypeEvent;
import com.pas.socialapp.eventhandlers.DistrictEvent;
import com.pas.socialapp.eventhandlers.ForgotPasswordEvent;
import com.pas.socialapp.eventhandlers.GCMRegistrationEvent;
import com.pas.socialapp.eventhandlers.GenericErrorEvent;
import com.pas.socialapp.eventhandlers.HelpingHandEvent;
import com.pas.socialapp.eventhandlers.JahirnamaEvent;
import com.pas.socialapp.eventhandlers.LoginEvent;
import com.pas.socialapp.eventhandlers.MatdarJagrutiEvent;
import com.pas.socialapp.eventhandlers.OTPverifyEvent;
import com.pas.socialapp.eventhandlers.PartyProfileEvent;
import com.pas.socialapp.eventhandlers.PinCodeEvent;
import com.pas.socialapp.eventhandlers.ProfileEvent;
import com.pas.socialapp.eventhandlers.RegEvent;
import com.pas.socialapp.eventhandlers.ReligionEvent;
import com.pas.socialapp.eventhandlers.ResendOTPverifyEvent;
import com.pas.socialapp.eventhandlers.ServiceCategoryEvent;
import com.pas.socialapp.eventhandlers.SocialNetworkTabEvent;
import com.pas.socialapp.eventhandlers.StatesEvent;
import com.pas.socialapp.eventhandlers.SubmitComplaintsEvent;
import com.pas.socialapp.eventhandlers.TabVisibilityEvent;
import com.pas.socialapp.eventhandlers.TalukaEvent;
import com.pas.socialapp.eventhandlers.WorkEvent;
import com.pas.socialapp.models.CandidateRequest;
import com.pas.socialapp.models.CasteResponse;
import com.pas.socialapp.models.ComplaintsTypeResponse;
import com.pas.socialapp.models.DistrictRequest;
import com.pas.socialapp.models.DistrictResponse;
import com.pas.socialapp.models.ForgotPasswordRequest;
import com.pas.socialapp.models.ForgotPasswordResponse;
import com.pas.socialapp.models.GCMRegistrationRequest;
import com.pas.socialapp.models.GCMRegistrationResponse;
import com.pas.socialapp.models.HHSubCatRequest;
import com.pas.socialapp.models.HelpingHandCategoryResponse;
import com.pas.socialapp.models.JahirnamaRequest;
import com.pas.socialapp.models.JahirnamaResponse;
import com.pas.socialapp.models.LoginRequest;
import com.pas.socialapp.models.LoginResponse;
import com.pas.socialapp.models.MatdarJagrutiRequest;
import com.pas.socialapp.models.MatdarJagrutiResponse;
import com.pas.socialapp.models.OTPVerifyRequest;
import com.pas.socialapp.models.OTPVerifyResponse;
import com.pas.socialapp.models.PartyProfileResponse;
import com.pas.socialapp.models.PinCodeRequest;
import com.pas.socialapp.models.PinCodeResponse;
import com.pas.socialapp.models.ProfileResponse;
import com.pas.socialapp.models.RegistrationRequest;
import com.pas.socialapp.models.RegistrationResponse;
import com.pas.socialapp.models.ReligionResponse;
import com.pas.socialapp.models.ResendOTPVerifyResponse;
import com.pas.socialapp.models.ServiceCategoryResponse;
import com.pas.socialapp.models.ServiceRequest;
import com.pas.socialapp.models.SocialNetworkTabRequest;
import com.pas.socialapp.models.SocialNetworkTabResponse;
import com.pas.socialapp.models.StateResponse;
import com.pas.socialapp.models.SubmitComplaintRequest;
import com.pas.socialapp.models.SubmitComplaintsResponse;
import com.pas.socialapp.models.TabVisibilityRequest;
import com.pas.socialapp.models.TabVisibilityResponse;
import com.pas.socialapp.models.TalukaRequest;
import com.pas.socialapp.models.TalukaResponse;
import com.pas.socialapp.models.WorkRequest;
import com.pas.socialapp.models.WorkResponse;

import java.util.Date;

import de.greenrobot.event.EventBus;


public class AccountManager {

    private Context context;

    public AccountManager(Context context) {
        this.context = context;
    }


    public void loginTask(String username, String password) {

        LoginRequest requestBody = createLoginRequest(username, password);
        final Request<LoginResponse> request = new JsonResponseRequest<>(Constants.LOGIN_URL,
                LoginResponse.class, requestBody,
                new Response.Listener<LoginResponse>() {
                    @Override
                    public void onResponse(LoginResponse response) {
                        EventBus.getDefault().post(new LoginEvent(response));
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError volleyError) {
                volleyError.printStackTrace();
                EventBus.getDefault().post(new GenericErrorEvent(volleyError.getMessage()));
            }
        });
        VolleyManager.getInstance().addToRequestQueue(request);
    }

    private LoginRequest createLoginRequest(String username, String password) {
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.email = username;
        loginRequest.password = password;
        loginRequest.date = new Date().toString();
        return loginRequest;
    }


    private MatdarJagrutiRequest createMatdarJagrutiRequest(String cid) {
        MatdarJagrutiRequest jagrutiRequest = new MatdarJagrutiRequest(cid);
        jagrutiRequest.CID = cid;
        return jagrutiRequest;
    }

    private TabVisibilityRequest createTabVisibilityRequest(String cid) {
        TabVisibilityRequest visibilityRequest = new TabVisibilityRequest(cid);
        visibilityRequest.CID = cid;
        return visibilityRequest;
    }


    private SocialNetworkTabRequest createSocialNetworkTabRequest(String cid,String action) {
        SocialNetworkTabRequest networkTabRequest = new SocialNetworkTabRequest(cid,action);
        networkTabRequest.CID = cid;
        networkTabRequest.action=action;
        return networkTabRequest;
    }

    private ForgotPasswordRequest createForgotPasswordRequest(String phone, String userrole) {
        ForgotPasswordRequest forgotPasswordRequest = new ForgotPasswordRequest(phone,userrole);
        forgotPasswordRequest.phone = phone;
        forgotPasswordRequest.userrole=userrole;
        return forgotPasswordRequest;
    }



    public void userRegistrationTask(String username, String middlename, String lastname, String email, String password, String phone, String landline, String dob, String gender, String bloodgroup, String education, String profession, String isChairman, String flatno, String house, String wing, String society, String survey, String lane1, String lane2, String area, String state, String district, String taluka, String pincode, String casteStr, String mothername, String isVoter, String anniversaryDate, String alternatePhone, String secondaryPhone, String religionStr) {
        RegistrationRequest requestBody = createRegistrationRequest(username, middlename, lastname, email, password, phone, landline, dob, gender, bloodgroup, education, profession, isChairman, flatno, house, wing, society, survey, lane1, lane2, area, state, district, taluka, pincode, casteStr, mothername, isVoter, anniversaryDate, alternatePhone, secondaryPhone, religionStr);

        Log.v("------>>>>", "" + requestBody.toString());
        final Request<RegistrationResponse> request = new JsonResponseRequest<>(Constants.REGISTRATION_URL,
                RegistrationResponse.class, requestBody,
                new Response.Listener<RegistrationResponse>() {

                    @Override
                    public void onResponse(RegistrationResponse response) {
                        EventBus.getDefault().post(new RegEvent(response));
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError volleyError) {
                volleyError.printStackTrace();
                EventBus.getDefault().post(new GenericErrorEvent(volleyError.getMessage()));
            }
        });
        VolleyManager.getInstance().addToRequestQueue(request);
    }

    private RegistrationRequest createRegistrationRequest(String username, String middlename, String lastname, String email, String password, String phone, String landline, String dob, String gender, String bloodgroup, String education, String profession, String isChairman, String flatno, String house, String wing, String society, String survey, String lane1, String lane2, String area, String state, String district, String taluka, String pincode, String casteStr, String mothername, String isVoter, String anniversaryDate, String alternatePhone, String secondaryPhone, String religionStr) {
        RegistrationRequest regRequest = new RegistrationRequest();
        regRequest.firstname = username;
        regRequest.middlename = middlename;
        regRequest.lastname = lastname;

        regRequest.password = password;
        regRequest.email = email;
        regRequest.phone = phone;
        regRequest.landline = landline;
        regRequest.dob = dob;
        regRequest.gender = gender;
        regRequest.bloodgroup = bloodgroup;
        regRequest.education = education;
        regRequest.profession = profession;
        regRequest.registeredFrom = Constants.REQUEST_FROM;
        regRequest.ischairman = isChairman;

        regRequest.flathouseno = flatno;
        regRequest.housename = house;
        regRequest.wing = wing;
        regRequest.society = society;
        regRequest.surveyno = survey;
        regRequest.lane1 = lane1;
        regRequest.lane2 = lane2;
        regRequest.area = area;

        regRequest.state = state;
        regRequest.district = district;
        regRequest.taluka = taluka;
        regRequest.pincode = pincode;
        regRequest.caste = casteStr;
        regRequest.CID = context.getResources().getString(R.string.candidate_id);
        regRequest.mothername = mothername;
        regRequest.role = Constants.ROLE;
        regRequest.isVoter = isVoter;
        regRequest.anniversary = anniversaryDate;
        regRequest.alternatephone = alternatePhone;
        regRequest.secondaryphone = secondaryPhone;
        regRequest.religion = religionStr;
        regRequest.date = new Date().toString();
        return regRequest;
    }

    public void verifyOTPTask(String mobileNo, String otp, String CTID) {
        OTPVerifyRequest requestBody = createOTPRequest(mobileNo, otp, CTID);
        final Request<OTPVerifyResponse> request = new JsonResponseRequest<>(Constants.VERIFY_OTP_URL,
                OTPVerifyResponse.class, requestBody,
                new Response.Listener<OTPVerifyResponse>() {

                    @Override
                    public void onResponse(OTPVerifyResponse response) {
                        EventBus.getDefault().post(new OTPverifyEvent(response, false));
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError volleyError) {
                volleyError.printStackTrace();
                EventBus.getDefault().post(new GenericErrorEvent(volleyError.getMessage()));
            }
        });
        VolleyManager.getInstance().addToRequestQueue(request);
    }


    public void resendOTPTask(String mobileNo, String CTID) {
        OTPVerifyRequest requestBody = createOTPRequest(mobileNo, "", CTID);
        final Request<OTPVerifyResponse> request = new JsonResponseRequest<>(Constants.RESEND_OTP_URL,
                OTPVerifyResponse.class, requestBody,
                new Response.Listener<OTPVerifyResponse>() {

                    @Override
                    public void onResponse(OTPVerifyResponse response) {
                        EventBus.getDefault().post(new OTPverifyEvent(response, true));
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError volleyError) {
                volleyError.printStackTrace();
                EventBus.getDefault().post(new GenericErrorEvent(volleyError.getMessage()));
            }
        });
        VolleyManager.getInstance().addToRequestQueue(request);
    }


    public void resendOTP(String mobileNo, String CTID) {
        OTPVerifyRequest requestBody = createOTPRequest(mobileNo, "", CTID);
        final Request<ResendOTPVerifyResponse> request = new JsonResponseRequest<>(Constants.RESEND_OTP_URL,
                ResendOTPVerifyResponse.class, requestBody,
                new Response.Listener<ResendOTPVerifyResponse>() {

                    @Override
                    public void onResponse(ResendOTPVerifyResponse response) {
                        EventBus.getDefault().post(new ResendOTPverifyEvent(response, true));
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError volleyError) {
                volleyError.printStackTrace();
                EventBus.getDefault().post(new GenericErrorEvent(volleyError.getMessage()));
            }
        });
        VolleyManager.getInstance().addToRequestQueue(request);
    }


    private OTPVerifyRequest createOTPRequest(String mobileNo, String otp, String CTID) {
        OTPVerifyRequest otpRequest = new OTPVerifyRequest();
        otpRequest.phone = mobileNo;
        otpRequest.otp = otp;
        otpRequest.CTID = CTID;
        otpRequest.date = new Date().toString();
        return otpRequest;
    }

    public void getAllStates() {

        final Request<StateResponse> request = new JsonResponseRequest<>(Constants.STATES_URL,
                StateResponse.class, "",
                new Response.Listener<StateResponse>() {
                    @Override
                    public void onResponse(StateResponse response) {
                        EventBus.getDefault().post(new StatesEvent(response));
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError volleyError) {
                volleyError.printStackTrace();
                EventBus.getDefault().post(new GenericErrorEvent(volleyError.getMessage()));
            }
        });
        VolleyManager.getInstance().addToRequestQueue(request);
    }

    public void getComplaintsTypeFromServer() {
        final Request<ComplaintsTypeResponse> request = new JsonResponseRequest<>(Constants.GET_COMPLAINTS_MASTER_DATA_URL,
                ComplaintsTypeResponse.class, "",
                new Response.Listener<ComplaintsTypeResponse>() {
                    @Override
                    public void onResponse(ComplaintsTypeResponse response) {
                        EventBus.getDefault().post(new ComplaintsTypeEvent(response));
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError volleyError) {
                volleyError.printStackTrace();
                EventBus.getDefault().post(new GenericErrorEvent(volleyError.getMessage()));
            }
        });
        VolleyManager.getInstance().addToRequestQueue(request);
    }


    public void submitComplaint(String complaintcategory, String complaintdescription, String complainttitle,
                                String complaintimage1, String complaintimage2, String complaintimage3, String CTID, String CID) {
        SubmitComplaintRequest requestBody = createSubmitComplaintRequest( complaintcategory,  complaintdescription,  complainttitle,
                 complaintimage1,  complaintimage2,  complaintimage3,  CTID,  CID);
        final Request<SubmitComplaintsResponse> request = new JsonResponseRequest<>(Constants.ADD_COMPLAINT_URL,
                SubmitComplaintsResponse.class, requestBody,
                new Response.Listener<SubmitComplaintsResponse>() {

                    @Override
                    public void onResponse(SubmitComplaintsResponse response) {
                        EventBus.getDefault().post(new SubmitComplaintsEvent(response));
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError volleyError) {
                volleyError.printStackTrace();
                EventBus.getDefault().post(new GenericErrorEvent(volleyError.getMessage()));
            }
        });
        VolleyManager.getInstance().addToRequestQueue(request);
    }

    private SubmitComplaintRequest createSubmitComplaintRequest(String complaintcategory, String complaintdescription, String complainttitle,
                                                                String complaintimage1, String complaintimage2, String complaintimage3, String CTID, String CID) {
        SubmitComplaintRequest submitComplaintRequest = new SubmitComplaintRequest();
        submitComplaintRequest.complaintcategory = complaintcategory;
        submitComplaintRequest.complaintdescription = complaintdescription;
        submitComplaintRequest.complainttitle = complainttitle;
        submitComplaintRequest.complaintimage1 = complaintimage1;
        submitComplaintRequest.complaintimage2 = complaintimage2;
        submitComplaintRequest.complaintimage3 = complaintimage3;
        submitComplaintRequest.CTID = CTID;
        submitComplaintRequest.CID = CID;
        submitComplaintRequest.date = new Date().toString();
        return submitComplaintRequest;
    }


    public void getCaste() {

        final Request<CasteResponse> request = new JsonResponseRequest<>(Constants.CASTE_URL,
                CasteResponse.class, "",
                new Response.Listener<CasteResponse>() {
                    @Override
                    public void onResponse(CasteResponse response) {
                        EventBus.getDefault().post(new CasteEvent(response));
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError volleyError) {
                volleyError.printStackTrace();
                EventBus.getDefault().post(new GenericErrorEvent(volleyError.getMessage()));
            }
        });
        VolleyManager.getInstance().addToRequestQueue(request);
    }

    public void getReligion() {

        final Request<ReligionResponse> request = new JsonResponseRequest<>(Constants.RELIGION_URL,
                ReligionResponse.class, "",
                new Response.Listener<ReligionResponse>() {
                    @Override
                    public void onResponse(ReligionResponse response) {
                        EventBus.getDefault().post(new ReligionEvent(response));
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError volleyError) {
                volleyError.printStackTrace();
                EventBus.getDefault().post(new GenericErrorEvent(volleyError.getMessage()));
            }
        });
        VolleyManager.getInstance().addToRequestQueue(request);
    }

    public void getAllDistricts(String state) {
        DistrictRequest requestBody = new DistrictRequest(state);
        final Request<DistrictResponse> request = new JsonResponseRequest<>(Constants.DISTRICT_URL,
                DistrictResponse.class, requestBody,
                new Response.Listener<DistrictResponse>() {
                    @Override
                    public void onResponse(DistrictResponse response) {
                        EventBus.getDefault().post(new DistrictEvent(response));
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError volleyError) {
                volleyError.printStackTrace();
                EventBus.getDefault().post(new GenericErrorEvent(volleyError.getMessage()));
            }
        });
        VolleyManager.getInstance().addToRequestQueue(request);
    }

    public void getAllTalukas(String district) {
        TalukaRequest requestBody = new TalukaRequest(district);

        final Request<TalukaResponse> request = new JsonResponseRequest<>(Constants.TALUKA_URL,
                TalukaResponse.class, requestBody,
                new Response.Listener<TalukaResponse>() {
                    @Override
                    public void onResponse(TalukaResponse response) {
                        EventBus.getDefault().post(new TalukaEvent(response));
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError volleyError) {
                volleyError.printStackTrace();
                EventBus.getDefault().post(new GenericErrorEvent(volleyError.getMessage()));
            }
        });
        VolleyManager.getInstance().addToRequestQueue(request);
    }

    public void getAllPinCodes(String taluka) {
        PinCodeRequest requestBody = new PinCodeRequest(taluka);

        final Request<PinCodeResponse> request = new JsonResponseRequest<>(Constants.PINCODE_URL,
                PinCodeResponse.class, requestBody,
                new Response.Listener<PinCodeResponse>() {
                    @Override
                    public void onResponse(PinCodeResponse response) {
                        EventBus.getDefault().post(new PinCodeEvent(response));
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError volleyError) {
                volleyError.printStackTrace();
                EventBus.getDefault().post(new GenericErrorEvent(volleyError.getMessage()));
            }
        });
        VolleyManager.getInstance().addToRequestQueue(request);
    }


    public void getProfileDetails(String tabName) {

        CandidateRequest requestBody = new CandidateRequest(String.valueOf(context.getResources().getString(R.string.candidate_id)), tabName);
        final Request<ProfileResponse> request = new JsonResponseRequest<>(Constants.CANDIDATE_DATA,
                ProfileResponse.class, requestBody,
                new Response.Listener<ProfileResponse>() {
                    @Override
                    public void onResponse(ProfileResponse response) {
                        EventBus.getDefault().post(new ProfileEvent(response));
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError volleyError) {
                volleyError.printStackTrace();
                EventBus.getDefault().post(new GenericErrorEvent(volleyError.getMessage()));
            }
        });
        VolleyManager.getInstance().addToRequestQueue(request);
    }



    public void getTabVisibilityData() {
        TabVisibilityRequest requestBody = new TabVisibilityRequest(String.valueOf(context.getResources().getString(R.string.candidate_id)));
        final Request<TabVisibilityResponse> request = new JsonResponseRequest<>(Constants.GET_TAB_VISIBILITY_DATA_URL,
                TabVisibilityResponse.class, requestBody,
                new Response.Listener<TabVisibilityResponse>() {
                    @Override
                    public void onResponse(TabVisibilityResponse response) {
                        EventBus.getDefault().post(new TabVisibilityEvent(response));
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError volleyError) {
                volleyError.printStackTrace();
                EventBus.getDefault().post(new GenericErrorEvent(volleyError.getMessage()));
            }
        });
        VolleyManager.getInstance().addToRequestQueue(request);
    }

    public void getPartyProfileDetails(String tabName) {

        CandidateRequest requestBody = new CandidateRequest(String.valueOf(context.getResources().getString(R.string.candidate_id)), tabName);
        final Request<PartyProfileResponse> request = new JsonResponseRequest<>(Constants.CANDIDATE_DATA,
                PartyProfileResponse.class, requestBody,
                new Response.Listener<PartyProfileResponse>() {
                    @Override
                    public void onResponse(PartyProfileResponse response) {
                        EventBus.getDefault().post(new PartyProfileEvent(response));
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError volleyError) {
                volleyError.printStackTrace();
                EventBus.getDefault().post(new GenericErrorEvent(volleyError.getMessage()));
            }
        });
        VolleyManager.getInstance().addToRequestQueue(request);
    }

    public void getWorkDetails(String tabName) {

        WorkRequest requestBody = new WorkRequest(String.valueOf(context.getResources().getString(R.string.candidate_id)), tabName);
        Log.d("Json string is ",requestBody.toString());
        final Request<WorkResponse> request = new JsonResponseRequest<>(Constants.WORK_JAHIRNAMA_DATA,
                WorkResponse.class, requestBody,
                new Response.Listener<WorkResponse>() {
                    @Override
                    public void onResponse(WorkResponse response) {
                        EventBus.getDefault().post(new WorkEvent(response));
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError volleyError) {
                volleyError.printStackTrace();
                EventBus.getDefault().post(new GenericErrorEvent(volleyError.getMessage()));
            }
        });
        VolleyManager.getInstance().addToRequestQueue(request);
    }

    public void getJahirnamaDetails(String tabName) {
        JahirnamaRequest requestBody = new JahirnamaRequest(String.valueOf(context.getResources().getString(R.string.candidate_id)), tabName);
        final Request<JahirnamaResponse> request = new JsonResponseRequest<>(Constants.WORK_JAHIRNAMA_DATA,
                JahirnamaResponse.class, requestBody,
                new Response.Listener<JahirnamaResponse>() {
                    @Override
                    public void onResponse(JahirnamaResponse response) {
                        EventBus.getDefault().post(new JahirnamaEvent(response));
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError volleyError) {
                volleyError.printStackTrace();
                EventBus.getDefault().post(new GenericErrorEvent(volleyError.getMessage()));
            }
        });
        VolleyManager.getInstance().addToRequestQueue(request);
    }


    public void getServicesByCategory(String serviceType) {

        ServiceRequest requestBody = new ServiceRequest(String.valueOf(context.getResources().getString(R.string.candidate_id)), serviceType);

        final Request<ServiceCategoryResponse> request = new JsonResponseRequest<>(Constants.GET_SERVICE_CATEGORYS,
                ServiceCategoryResponse.class, requestBody,
                new Response.Listener<ServiceCategoryResponse>() {
                    @Override
                    public void onResponse(ServiceCategoryResponse response) {
                        EventBus.getDefault().post(new ServiceCategoryEvent(response));
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError volleyError) {
                volleyError.printStackTrace();
                EventBus.getDefault().post(new GenericErrorEvent(volleyError.getMessage()));
            }
        });
        VolleyManager.getInstance().addToRequestQueue(request);
    }


    public void getMatdarJagrutiDataFromServer() {

        MatdarJagrutiRequest jagrutiRequest = createMatdarJagrutiRequest(String.valueOf(context.getResources().getString(R.string.candidate_id)));

        final Request<MatdarJagrutiResponse> request = new JsonResponseRequest<>(Constants.GET_MATDAR_JAGRUTI_DATA_URL,
                MatdarJagrutiResponse.class, jagrutiRequest,
                new Response.Listener<MatdarJagrutiResponse>() {
                    @Override
                    public void onResponse(MatdarJagrutiResponse response) {
                        EventBus.getDefault().post(new MatdarJagrutiEvent(response));
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError volleyError) {
                volleyError.printStackTrace();
                EventBus.getDefault().post(new GenericErrorEvent(volleyError.getMessage()));
            }
        });
        VolleyManager.getInstance().addToRequestQueue(request);
    }


    public void getSocialNetworkDataFromServer(String cid,String action) {

        SocialNetworkTabRequest networkTabRequest = createSocialNetworkTabRequest(cid, action);

        final Request<SocialNetworkTabResponse> request = new JsonResponseRequest<>(Constants.GET_SOCIAL_NETWORK_DATA_URL,
                SocialNetworkTabResponse.class, networkTabRequest,
                new Response.Listener<SocialNetworkTabResponse>() {
                    @Override
                    public void onResponse(SocialNetworkTabResponse response) {
                        EventBus.getDefault().post(new SocialNetworkTabEvent(response));
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError volleyError) {
                volleyError.printStackTrace();
                EventBus.getDefault().post(new GenericErrorEvent(volleyError.getMessage()));
            }
        });
        VolleyManager.getInstance().addToRequestQueue(request);
    }

    public void forgotPassword(String phone,String userrole) {

        ForgotPasswordRequest forgotPasswordRequest = createForgotPasswordRequest(phone, userrole);

        final Request<ForgotPasswordResponse> request = new JsonResponseRequest<>(Constants.FORGOT_PASSWORD_URL,
                ForgotPasswordResponse.class, forgotPasswordRequest,
                new Response.Listener<ForgotPasswordResponse>() {
                    @Override
                    public void onResponse(ForgotPasswordResponse response) {
                        EventBus.getDefault().post(new ForgotPasswordEvent(response));
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError volleyError) {
                volleyError.printStackTrace();
                EventBus.getDefault().post(new GenericErrorEvent(volleyError.getMessage()));
            }
        });
        VolleyManager.getInstance().addToRequestQueue(request);
    }


    public void getHelpingHnadByCategory(String serviceType) {

        HHSubCatRequest requestBody = new HHSubCatRequest(String.valueOf(context.getResources().getString(R.string.candidate_id)), serviceType);

        final Request<HelpingHandCategoryResponse> request = new JsonResponseRequest<>(Constants.GET_HELPING_HAND_SERVICE_CATEGORYS,
                HelpingHandCategoryResponse.class, requestBody,
                new Response.Listener<HelpingHandCategoryResponse>() {
                    @Override
                    public void onResponse(HelpingHandCategoryResponse response) {
                        EventBus.getDefault().post(new HelpingHandEvent(response));
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError volleyError) {
                volleyError.printStackTrace();
                EventBus.getDefault().post(new GenericErrorEvent(volleyError.getMessage()));
            }
        });
        VolleyManager.getInstance().addToRequestQueue(request);
    }

    public void sendGCMRegIdToServer(String mobileNO,String deviceToken,String deviceId) {

        GCMRegistrationRequest requestBody = new GCMRegistrationRequest( mobileNO, deviceToken, deviceId);

        final Request<GCMRegistrationResponse> request = new JsonResponseRequest<>(Constants.SEND_GCM_REG_ID_TO_SERVER,
                GCMRegistrationResponse.class, requestBody,
                new Response.Listener<GCMRegistrationResponse>() {
                    @Override
                    public void onResponse(GCMRegistrationResponse response) {
                        EventBus.getDefault().post(new GCMRegistrationEvent(response));
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError volleyError) {
                volleyError.printStackTrace();
                EventBus.getDefault().post(new GenericErrorEvent(volleyError.getMessage()));
            }
        });
        VolleyManager.getInstance().addToRequestQueue(request);
    }

}
