package com.pas.socialapp.models;

import com.pas.socialapp.Constants;

/**
 * Created by ganesh on 5/1/16.
 */
public class HHSubCatRequest extends GenericRequest {


    public String CID;
    public String hhmaincategory;
    public String requestfrom;

    public HHSubCatRequest(String CID, String hhmaincategory) {
        this.CID = CID;
        this.hhmaincategory = hhmaincategory;
        this.requestfrom = Constants.REQUEST_FROM;
    }


}
