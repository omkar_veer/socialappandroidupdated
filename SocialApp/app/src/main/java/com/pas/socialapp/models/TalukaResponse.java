package com.pas.socialapp.models;

import java.util.ArrayList;

/**
 * Created by Umesh  on 24/02/16.
 */
public class TalukaResponse extends GenericRequest {

    public String status;
    public String service;
    public String message;
    public ArrayList<Taluka> talukas = new ArrayList<Taluka>();

    /**
     * @return The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return The service
     */
    public String getService() {
        return service;
    }

    /**
     * @param service The service
     */
    public void setService(String service) {
        this.service = service;
    }

    /**
     * @return The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return The talukas
     */
    public ArrayList<Taluka> getTalukas() {
        return talukas;
    }

    /**
     * @param talukas The talukas
     */
    public void setTalukas(ArrayList<Taluka> talukas) {
        this.talukas = talukas;
    }


    public class Taluka {

        private String taluka;

        /**
         * @return The taluka
         */
        public String getTaluka() {
            return taluka;
        }

        /**
         * @param taluka The taluka
         */
        public void setTaluka(String taluka) {
            this.taluka = taluka;
        }
    }
}
