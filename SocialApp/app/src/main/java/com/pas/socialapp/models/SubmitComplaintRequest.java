package com.pas.socialapp.models;

public class SubmitComplaintRequest extends GenericRequest {

    public String complaintcategory;
    public String complainttitle;
    public String complaintdescription;
    public String complaintimage1;
    public String complaintimage2;
    public String complaintimage3;
    public String CTID;
    public String CID;

}
