package com.pas.socialapp.models;

import java.util.ArrayList;

/**
 * Created by ganesh on 5/1/16.
 */
public class ProfileResponse extends GenericResponse {


    public String status;
    public String service;
    public String message;
    public ArrayList<Profile> profile = new ArrayList<Profile>();

    /**
     * @return The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return The service
     */
    public String getService() {
        return service;
    }

    /**
     * @param service The service
     */
    public void setService(String service) {
        this.service = service;
    }

    /**
     * @return The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return The profile
     */
    public ArrayList<Profile> getProfile() {
        return profile;
    }

    /**
     * @param profile The profile
     */
    public void setProfile(ArrayList<Profile> profile) {
        this.profile = profile;
    }

    public class Profile {

        private String CID;
        private String firstname;
        private String middlename;
        private String lastname;
        private String email;
        private String phone;
        private String landline;
        private String gender;
        private String dob;
        private String jahirnamastatus;
        private String workstatus;
        private String image;
        private String lane1;
        private String lane2;
        private String ward;
        private String area;
        private String taluka;
        private String district;
        private String state;
        private String pincode;
        private String assembly;
        private String loksabha;
        private String vidhansabha;
        private String profilestatus;
        private String partyname;
        private String partyid;
        private String partystatus;
        private String partyimage;
        private String candidatedescription;
        private String otherpartyimage;

        public String getCandidatedescription() {
            return candidatedescription;
        }

        public void setCandidatedescription(String candidatedescription) {
            this.candidatedescription = candidatedescription;
        }

        public String getOtherpartyimage() {
            return otherpartyimage;
        }

        public void setOtherpartyimage(String otherpartyimage) {
            this.otherpartyimage = otherpartyimage;
        }

        /**
         * @return The CID
         */
        public String getCID() {
            return CID;
        }

        /**
         * @param CID The CID
         */
        public void setCID(String CID) {
            this.CID = CID;
        }

        /**
         * @return The firstname
         */
        public String getFirstname() {
            return firstname;
        }

        /**
         * @param firstname The firstname
         */
        public void setFirstname(String firstname) {
            this.firstname = firstname;
        }

        /**
         * @return The middlename
         */
        public String getMiddlename() {
            return middlename;
        }

        /**
         * @param middlename The middlename
         */
        public void setMiddlename(String middlename) {
            this.middlename = middlename;
        }

        /**
         * @return The lastname
         */
        public String getLastname() {
            return lastname;
        }

        /**
         * @param lastname The lastname
         */
        public void setLastname(String lastname) {
            this.lastname = lastname;
        }

        /**
         * @return The email
         */
        public String getEmail() {
            return email;
        }

        /**
         * @param email The email
         */
        public void setEmail(String email) {
            this.email = email;
        }

        /**
         * @return The phone
         */
        public String getPhone() {
            return phone;
        }

        /**
         * @param phone The phone
         */
        public void setPhone(String phone) {
            this.phone = phone;
        }

        /**
         * @return The landline
         */
        public String getLandline() {
            return landline;
        }

        /**
         * @param landline The landline
         */
        public void setLandline(String landline) {
            this.landline = landline;
        }

        /**
         * @return The gender
         */
        public String getGender() {
            return gender;
        }

        /**
         * @param gender The gender
         */
        public void setGender(String gender) {
            this.gender = gender;
        }

        /**
         * @return The dob
         */
        public String getDob() {
            return dob;
        }

        /**
         * @param dob The dob
         */
        public void setDob(String dob) {
            this.dob = dob;
        }

        /**
         * @return The image
         */
        public String getImage() {
            return image;
        }

        /**
         * @param image The image
         */
        public void setImage(String image) {
            this.image = image;
        }

        /**
         * @return The lane1
         */
        public String getLane1() {
            return lane1;
        }

        /**
         * @param lane1 The lane1
         */
        public void setLane1(String lane1) {
            this.lane1 = lane1;
        }

        /**
         * @return The lane2
         */
        public String getLane2() {
            return lane2;
        }

        /**
         * @param lane2 The lane2
         */
        public void setLane2(String lane2) {
            this.lane2 = lane2;
        }

        /**
         * @return The state
         */
        public String getState() {
            return state;
        }

        /**
         * @param state The state
         */
        public void setState(String state) {
            this.state = state;
        }

        /**
         * @return The district
         */
        public String getDistrict() {
            return district;
        }

        /**
         * @param district The district
         */
        public void setDistrict(String district) {
            this.district = district;
        }

        /**
         * @return The taluka
         */
        public String getTaluka() {
            return taluka;
        }

        /**
         * @param taluka The taluka
         */
        public void setTaluka(String taluka) {
            this.taluka = taluka;
        }

        /**
         * @return The pincode
         */
        public String getPincode() {
            return pincode;
        }

        /**
         * @param pincode The pincode
         */
        public void setPincode(String pincode) {
            this.pincode = pincode;
        }

        /**
         * @return The area
         */
        public String getArea() {
            return area;
        }

        /**
         * @param area The area
         */
        public void setArea(String area) {
            this.area = area;
        }

        /**
         * @return The assembly
         */
        public String getAssembly() {
            return assembly;
        }

        /**
         * @param assembly The assembly
         */
        public void setAssembly(String assembly) {
            this.assembly = assembly;
        }

        /**
         * @return The loksabha
         */
        public String getLoksabha() {
            return loksabha;
        }

        /**
         * @param loksabha The loksabha
         */
        public void setLoksabha(String loksabha) {
            this.loksabha = loksabha;
        }

        /**
         * @return The vidhansabha
         */
        public String getVidhansabha() {
            return vidhansabha;
        }

        /**
         * @param vidhansabha The vidhansabha
         */
        public void setVidhansabha(String vidhansabha) {
            this.vidhansabha = vidhansabha;
        }

        /**
         * @return The ward
         */
        public String getWard() {
            return ward;
        }

        /**
         * @param ward The ward
         */
        public void setWard(String ward) {
            this.ward = ward;
        }

        /**
         * @return The profilestatus
         */
        public String getProfilestatus() {
            return profilestatus;
        }

        /**
         * @param profilestatus The profilestatus
         */
        public void setProfilestatus(String profilestatus) {
            this.profilestatus = profilestatus;
        }

        /**
         * @return The partyid
         */
        public String getPartyid() {
            return partyid;
        }

        /**
         * @param partyid The partyid
         */
        public void setPartyid(String partyid) {
            this.partyid = partyid;
        }

        /**
         * @return The partystatus
         */
        public String getPartystatus() {
            return partystatus;
        }

        /**
         * @param partystatus The partystatus
         */
        public void setPartystatus(String partystatus) {
            this.partystatus = partystatus;
        }

        /**
         * @return The partyname
         */
        public String getPartyname() {
            return partyname;
        }

        /**
         * @param partyname The partyname
         */
        public void setPartyname(String partyname) {
            this.partyname = partyname;
        }

        /**
         * @return The partyimage
         */
        public String getPartyimage() {
            return partyimage;
        }

        /**
         * @param partyimage The partyimage
         */
        public void setPartyimage(String partyimage) {
            this.partyimage = partyimage;
        }

        /**
         * @return The jahirnamastatus
         */
        public String getJahirnamastatus() {
            return jahirnamastatus;
        }

        /**
         * @param jahirnamastatus The jahirnamastatus
         */
        public void setJahirnamastatus(String jahirnamastatus) {
            this.jahirnamastatus = jahirnamastatus;
        }

        /**
         * @return The workstatus
         */
        public String getWorkstatus() {
            return workstatus;
        }

        /**
         * @param workstatus The workstatus
         */
        public void setWorkstatus(String workstatus) {
            this.workstatus = workstatus;
        }

    }
}
