package com.pas.socialapp.models;

/**
 * Created by ganesh on 5/1/16.
 */
public class RegistrationRequest extends GenericRequest {


    @Override
    public String toString() {
        return "RegistrationRequest{" +
                "firstname='" + firstname + '\'' +
                ", middlename='" + middlename + '\'' +
                ", lastname='" + lastname + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", phone='" + phone + '\'' +
                ", landline='" + landline + '\'' +
                ", dob='" + dob + '\'' +
                ", gender='" + gender + '\'' +
                ", bloodgroup='" + bloodgroup + '\'' +
                ", education='" + education + '\'' +
                ", profession='" + profession + '\'' +
                ", registeredFrom='" + registeredFrom + '\'' +
                ", ischairman='" + ischairman + '\'' +
                ", flathouseno='" + flathouseno + '\'' +
                ", housename='" + housename + '\'' +
                ", wing='" + wing + '\'' +
                ", society='" + society + '\'' +
                ", surveyno='" + surveyno + '\'' +
                ", lane1='" + lane1 + '\'' +
                ", lane2='" + lane2 + '\'' +
                ", area='" + area + '\'' +
                ", pincode='" + pincode + '\'' +
                ", taluka='" + taluka + '\'' +
                ", district='" + district + '\'' +
                ", state='" + state + '\'' +
                ", caste='" + caste + '\'' +
                ", CID='" + CID + '\'' +
                ", mothername='" + mothername + '\'' +
                ", anniversary='" + anniversary + '\'' +
                ", isVoter='" + isVoter + '\'' +
                ", role='" + role + '\'' +
                ", alternatephone='" + alternatephone + '\'' +
                ", secondaryphone='" + secondaryphone + '\'' +
                ", religion='" + religion + '\'' +
                '}';
    }

    public String firstname;
    public String middlename;
    public String lastname;
    public String email;
    public String password;
    public String phone;
    public String landline;
    public String dob;
    public String gender;
    public String bloodgroup;
    public String education;
    public String profession;
    public String registeredFrom;
    public String ischairman;

    public String flathouseno;
    public String housename;
    public String wing;
    public String society;
    public String surveyno;
    public String lane1;
    public String lane2;
    public String area;

    public String pincode;
    public String taluka;
    public String district;
    public String state;

    public String caste;
    public String CID;

    public String mothername;
    public String anniversary;
    public String isVoter;
    public String role;

    public String alternatephone;
    public String secondaryphone;
    public String religion;



}
