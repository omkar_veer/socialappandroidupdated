package com.pas.socialapp.managers;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

public class PrefManager {

    private static String USER_PREFS_NAME = "userPref";

    private static String TAB_PREFS_NAME = "tabPref";

    public static void setUserDetails(Context context, String username, String password, String CTID,String phone) {
        SharedPreferences.Editor editor = context.getSharedPreferences(USER_PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putString("UserName", username);
        editor.putString("Password", password);
        editor.putString("CTID", CTID);
        editor.putString("Phone", phone);
        editor.apply();
    }

    public static String getUsername(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(USER_PREFS_NAME, Context.MODE_PRIVATE);
        return prefs.getString("UserName", "");
    }

    public static String getCTID(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(USER_PREFS_NAME, Context.MODE_PRIVATE);
        return prefs.getString("CTID", "");
    }

    public static String getUserPhone(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(USER_PREFS_NAME, Context.MODE_PRIVATE);
        return prefs.getString("Phone", "");
    }


    public static boolean isUserLoggedIn(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(USER_PREFS_NAME, Context.MODE_PRIVATE);
        String username = prefs.getString("UserName", null);
        if (TextUtils.isEmpty(username))
            return false;
        return true;
    }

    public static void setGCMRegId(Context context, String gcmRegId) {
        SharedPreferences.Editor editor = context.getSharedPreferences(USER_PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putString("gcmRegId", gcmRegId);
        editor.apply();
    }

    public static String getGCMRegId(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(USER_PREFS_NAME, Context.MODE_PRIVATE);
        return prefs.getString("gcmRegId", "");
    }



    /* set String key values */
    public static void setStringToPref(Context context, String key, String value) {
        SharedPreferences.Editor editor = context.getSharedPreferences(USER_PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static String getStringFromPref(Context context, String key) {
        SharedPreferences prefs = context.getSharedPreferences(USER_PREFS_NAME, Context.MODE_PRIVATE);
        return prefs.getString(key, "");

    }

    /* set bool key values */
    public static void setBooleanToPref(Context context, String key, boolean value) {
        SharedPreferences.Editor editor = context.getSharedPreferences(USER_PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public static boolean getBooleanFromPref(Context context, String key) {
        SharedPreferences prefs = context.getSharedPreferences(USER_PREFS_NAME, Context.MODE_PRIVATE);
        return prefs.getBoolean(key, false);
    }

    public static void setTabPref(Context context, String key, boolean value) {
        SharedPreferences.Editor editor = context.getSharedPreferences(TAB_PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public static boolean getTabPref(Context context, String key) {
        SharedPreferences prefs = context.getSharedPreferences(TAB_PREFS_NAME, Context.MODE_PRIVATE);
        return prefs.getBoolean(key, false);
    }


}
