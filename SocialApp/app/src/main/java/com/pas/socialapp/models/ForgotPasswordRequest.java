package com.pas.socialapp.models;

public class ForgotPasswordRequest extends GenericRequest {


    public String phone;
    public String userrole;

    public ForgotPasswordRequest(String phone, String userrole) {
        this.phone = phone;
        this.userrole = userrole;
    }


}
